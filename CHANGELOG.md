# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added

- ...

## [2.1.1] - 2025-02-26

### Added

- New changelog format


## [2.1.0] - 2025-02-26

### Added

- bitbucket pipelines improved
- session auth improved

### Fixed

- saved request usage with tests


## [2.0.0] - 2024-05-31

### Changed

- project and namespace renamed from `hondzyk` to `xklmicroapps`


## [1.0.7] - 2024-03-13

### Added

- auth middlewares extends `SlimApiEssentials\App\Di\ContainerActions::getServiceFromIdNameByClass`
- added some helper methods and isArgsAssoc fix


## [1.0.6] - 2024-03-12

### Added

- auth middlewares extends `SlimApiEssentials\App\Di\ContainerActions::getServiceFromIdNameByClass`
- do not autowire optional constructor classes now


## [1.0.5] - 2023-12-20

### Added

- auth middlewares extends `SlimApiEssentials\App\Middlewares\Auth\AbstractAuthMiddleware` with prepared methods
- and auth service is getting during processing request, not in constructor like before


## [1.0.4] - 2023-12-20

### Added

- `SlimApiEssentials\App\Services\Auth::check` is throwing `AuthOauth2Exception` now instead of 500 on bad token format


## [1.0.3] - 2023-12-20

### Added

- and url is configurable by auth type (Authorization header type like `Bearer`, `Basic`, `ApiKey` or whatever)

### Fixed

- fix `SlimApiEssentials\App\Services\Auth\Remote::check` method by httpclient request added method


## [1.0.2] - 2023-12-19

### Added

- invalid_client error set for `SlimApiEssentials\App\Exceptions\AuthOauth2Exception` with missing or bad header


## [1.0.1] - 2023-12-19

### Added

- `SlimApiEssentials\App\Middlewares\ContentCheckMiddleware`
- checks json and for oauth2 route checks x-www-form-urlencoded content type


## [1.0.0] - 2023-12-19

### Added

- new usable `Auth` services accessible via middlewares for protected routes
- `SlimApiEssentials\App\Middlewares\Auth\ApikeyMiddleware` - api key auth
- `SlimApiEssentials\App\Middlewares\Auth\BasicMiddleware` - http basic auth
- `SlimApiEssentials\App\Middlewares\Auth\Oauth2Middleware` - oauth2 auth with use of JWT (JSON Web Token)
- `SlimApiEssentials\App\Middlewares\Auth\RemoteMiddleware` - use of remote services (works just somehow like proxy by resending auth headers)
- and all other relevant `Auth` services and modifications required


## [0.8.3] - 2023-10-12

### Added

- simplified `SlimApiEssentials\App\Di\Container` and `SlimApiEssentials\App\Di\ContainerActions` parameters auto wiring
- it is possible to use named parameters in Container services getting now


## [0.8.2] - 2023-03-09

### Changed

- php8.1 dependency upgraded to php8.2 with all libraries and requirements


## [0.7.2] - 2022-11-29

### Added

- `SlimApiEssentials\App\Services\AuthDummy` accept empty token value as dummy auth.
- Just "Bearer" string is required to pass


## [0.7.1] - 2022-11-28

### Added

- `SlimApiEssentials\App\Services\AuthDummy` added class for possible use.


## [0.7.0] - 2022-11-28

### Added

- `SlimApiEssentials\App\Di\Services::getSlimApiEssentialsAppServicesAuth()` return class name can be defined in configuration now.
- It is basic auth service.
- And must extend original `SlimApiEssentials\App\Services\Auth`
- Useful e.g. for testing


## [0.6.0] - 2022-09-06

### Added

- `SlimApiEssentials\App\Services\Database::getSqlOrderBy()` allow use order by param as is, without quotes.
- only sql keywords are quoted, otherwise the orderby param is inserted in sql without changes
- so the parent process must control the security of passed param


## [0.5.0] - 2022-06-08

### Added

- `SlimApiEssentials\App\Services\Database::bindParam` fixed bool param type.
- `$value` is passed by refference as in `PDOStatement::bindParam`
- and php `bool` data type is retyped to `'1' (true)` and `'0' (false)` to work with postgres `boolean` data type


## [0.4.1] - 2022-04-27

### Added

- new method `SlimApiEssentials\App\Di\Container::set` to explicitly set service (useful when mocking services in functional tests)


## [0.3.1] - 2022-04-26

### Fixed

- fix `SlimApiEssentials\App\Entities\AbstractMessage` properties retyping (check allowed types for the `settype` function)


## [0.2.1] - 2022-03-23

### Fixed

- fix `SlimApiEssentials\App\Middlewares\AuthMiddleware` injection via constructor


## [0.2.0] - 2022-03-23

### Removed

- class `App\Di\Middlewares` removed. Dependency injection into the auth removed too.
- AuthMiddleware is not included in setRoutes as a parameter anymore


## [0.1.0] - 2022-03-23

### Added

- new `App\Di\Middlewares` to get auth middleware with just injected dependency (without constructor use)


## [0.0.8] - 2022-03-23

### Changed

- `SlimApiEssentials\App\Services\ResponseHelper::getWithLocation()`
- is now returning `urldecoded` url string


## [0.0.7] - 2022-03-23

### Changed

- `SlimApiEssentials\App\Middlewares` classes changed privat properties and methods to protected
- (to be able to extend these Middlewares)


## [0.0.6] - 2022-03-22

### Added

- new general class `SlimApiEssentials\App\Services\Auth` usable for authenticating request

### Changed

- changes in container - methods for getting services are now based on the whole namespaced class name
- (e.g. to get `Some\Namespaced\Service` someone should create `getSomeNamespacedService` method instead of `getService` before)


## [0.0.5] - 2022-03-22

### Added

- new general use classes included with tests:
- `SlimApiEssentials\App\Entities\Message`, `SlimApiEssentials\App\Entities\MessageIterator`


## [0.0.4] - 2022-03-22

### Added

- `SlimApiEssentials\App\Di\Container::setConfigParameters(array $params)`
- set `$params` as is without any type changing. Proper type has to be defined in config file


## [0.0.3] - 2022-03-22

### Added

- .gitattributes to ignore dev/tests parts of library in production


## [0.0.2] - 2022-03-22

### Changed

- tests namespacing updates
- composer requirements updates


## [0.0.1] - 2022-02-28

### Added

- Initial commit with tags management


[unreleased]: https://bitbucket.org/xklmicroapps/slim_api_essentials/branches/compare/HEAD%0D2.1.1
[2.1.1]: https://bitbucket.org/xklmicroapps/slim_api_essentials/branches/compare/2.1.1%0D2.1.0
[2.1.0]: https://bitbucket.org/xklmicroapps/slim_api_essentials/branches/compare/2.1.0%0D2.0.0
[2.0.0]: https://bitbucket.org/xklmicroapps/slim_api_essentials/branches/compare/2.0.0%0D1.0.7
[1.0.7]: https://bitbucket.org/xklmicroapps/slim_api_essentials/branches/compare/1.0.7%0D1.0.6
[1.0.6]: https://bitbucket.org/xklmicroapps/slim_api_essentials/branches/compare/1.0.6%0D1.0.5
[1.0.5]: https://bitbucket.org/xklmicroapps/slim_api_essentials/branches/compare/1.0.5%0D1.0.4
[1.0.4]: https://bitbucket.org/xklmicroapps/slim_api_essentials/branches/compare/1.0.4%0D1.0.3
[1.0.3]: https://bitbucket.org/xklmicroapps/slim_api_essentials/branches/compare/1.0.3%0D1.0.2
[1.0.2]: https://bitbucket.org/xklmicroapps/slim_api_essentials/branches/compare/1.0.2%0D1.0.1
[1.0.1]: https://bitbucket.org/xklmicroapps/slim_api_essentials/branches/compare/1.0.1%0D1.0.0
[1.0.0]: https://bitbucket.org/xklmicroapps/slim_api_essentials/branches/compare/1.0.0%0D0.8.3
[0.8.3]: https://bitbucket.org/xklmicroapps/slim_api_essentials/branches/compare/0.8.3%0D0.8.2
[0.8.2]: https://bitbucket.org/xklmicroapps/slim_api_essentials/branches/compare/0.8.2%0D0.7.2
[0.7.2]: https://bitbucket.org/xklmicroapps/slim_api_essentials/branches/compare/0.7.2%0D0.7.1
[0.7.1]: https://bitbucket.org/xklmicroapps/slim_api_essentials/branches/compare/0.7.1%0D0.7.0
[0.7.0]: https://bitbucket.org/xklmicroapps/slim_api_essentials/branches/compare/0.7.0%0D0.6.0
[0.6.0]: https://bitbucket.org/xklmicroapps/slim_api_essentials/branches/compare/0.6.0%0D0.5.0
[0.5.0]: https://bitbucket.org/xklmicroapps/slim_api_essentials/branches/compare/0.5.0%0D0.4.1
[0.4.1]: https://bitbucket.org/xklmicroapps/slim_api_essentials/branches/compare/0.4.1%0D0.3.1
[0.3.1]: https://bitbucket.org/xklmicroapps/slim_api_essentials/branches/compare/0.3.1%0D0.2.1
[0.2.1]: https://bitbucket.org/xklmicroapps/slim_api_essentials/branches/compare/0.2.1%0D0.2.0
[0.2.0]: https://bitbucket.org/xklmicroapps/slim_api_essentials/branches/compare/0.2.0%0D0.1.0
[0.1.0]: https://bitbucket.org/xklmicroapps/slim_api_essentials/branches/compare/0.1.0%0D0.0.8
[0.0.8]: https://bitbucket.org/xklmicroapps/slim_api_essentials/branches/compare/0.0.8%0D0.0.7
[0.0.7]: https://bitbucket.org/xklmicroapps/slim_api_essentials/branches/compare/0.0.7%0D0.0.6
[0.0.6]: https://bitbucket.org/xklmicroapps/slim_api_essentials/branches/compare/0.0.6%0D0.0.5
[0.0.5]: https://bitbucket.org/xklmicroapps/slim_api_essentials/branches/compare/0.0.5%0D0.0.4
[0.0.4]: https://bitbucket.org/xklmicroapps/slim_api_essentials/branches/compare/0.0.4%0D0.0.3
[0.0.3]: https://bitbucket.org/xklmicroapps/slim_api_essentials/branches/compare/0.0.3%0D0.0.2
[0.0.2]: https://bitbucket.org/xklmicroapps/slim_api_essentials/branches/compare/0.0.2%0D0.0.1
[0.0.1]: https://bitbucket.org/xklmicroapps/slim_api_essentials/branches/compare/0.0.1%0D9e4f4a418736d457a01fae21221d73cb2bef11da


# Types of changes

- `Added` for new features.
- `Changed` for changes in existing functionality.
- `Deprecated` for soon-to-be removed features.
- `Removed` for now removed features.
- `Fixed` for any bug fixes.
- `Security` in case of vulnerabilities.
