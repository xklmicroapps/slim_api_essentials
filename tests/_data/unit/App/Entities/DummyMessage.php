<?php

declare(strict_types=1);

namespace SlimApiEssentials\Tests\Data\App\Entities;

use SlimApiEssentials\App\Entities\AbstractMessage;

class DummyMessage extends AbstractMessage
{
    protected string $var1;

    protected array $var2;

    protected int $var3;
}
