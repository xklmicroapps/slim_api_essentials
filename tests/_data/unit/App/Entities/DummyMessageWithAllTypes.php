<?php

declare(strict_types=1);

namespace SlimApiEssentials\Tests\Data\App\Entities;

use SlimApiEssentials\App\Entities\AbstractMessage;
use StdClass;
use Traversable;

class DummyMessageWithAllTypes extends AbstractMessage
{
    protected StdClass $stdclass;

    protected AnyClass $anyclass;

    protected Traversable $traversable;

    protected self $self;

    protected parent $parent;

    protected array $array;

    protected bool $bool;

    protected float $float;

    protected int $int;

    protected string $string;

    protected iterable $iterable;

    protected object $object;

    protected mixed $mixed;


}
