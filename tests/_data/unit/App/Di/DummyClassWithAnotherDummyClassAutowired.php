<?php

declare(strict_types=1);

namespace SlimApiEssentials\Tests\Data\App\Di;

use DateTime;

class DummyClassWithAnotherDummyClassAutowired
{
    /**
     * As of PHP 8.0.0, declaring mandatory arguments after optional arguments is deprecated.
     * ...
     * @see https://www.php.net/manual/en/functions.arguments.php#functions.arguments.default
     * ...
     * "Incorrect usage of default function arguments"
     * "Correct usage of default function arguments"
     * ...
     */
    public function __construct(
        private string $arg1,
        private DummyClassToAutowire $arg2,
        private ?string $arg3,
        private $arg4 = null,
        private string $arg5 = 'arg5DefaultValue',
        private ?DateTime $arg6 = null
    ) {
    }

    public function __get($name)
    {
        return $this->$name;
    }
}
