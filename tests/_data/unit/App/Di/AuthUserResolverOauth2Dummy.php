<?php

declare(strict_types=1);

namespace SlimApiEssentials\Tests\Data\App\Di;

use SlimApiEssentials\App\Services\Auth\Oauth2UserResolverInterface;

class AuthUserResolverOauth2Dummy implements Oauth2UserResolverInterface
{
    public function getClientNameByClient(mixed $client): string
    {
        return '';
    }

    public function getClientIdByClient(mixed $client): string
    {
        return '';
    }

    public function getClientByClientId(string $id): mixed
    {
        return [];
    }

    public function isClientSecretOk(mixed $client, string $secret): bool
    {
        return false;
    }
}
