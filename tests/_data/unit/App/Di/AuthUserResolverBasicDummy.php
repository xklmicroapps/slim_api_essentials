<?php

declare(strict_types=1);

namespace SlimApiEssentials\Tests\Data\App\Di;

use SlimApiEssentials\App\Services\Auth\BasicUserResolverInterface;

class AuthUserResolverBasicDummy implements BasicUserResolverInterface
{
    public function getUserByUsername(mixed $id): mixed
    {
        return [];
    }

    public function isUserPassOk(mixed $user, string $password): bool
    {
        return false;
    }
}
