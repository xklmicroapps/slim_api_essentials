<?php

declare(strict_types=1);

namespace SlimApiEssentials\Tests\Data\App\Di;

use DateTime;

class DummyClassWithConstructorArgs
{
    public function __construct(private DateTime $arg1, private DateTime $arg2)
    {
    }
}
