<?php

declare(strict_types=1);

namespace SlimApiEssentials\Tests\Data\App\Di;

use SlimApiEssentials\App\Services\Auth\ApikeyUserResolverInterface;

class AuthUserResolverApikeyDummy implements ApikeyUserResolverInterface
{
    public function getUserByApikey(mixed $apikey): mixed
    {
        return [];
    }

    public function isApikeyExpired(string $apikey): bool
    {
        return false;
    }
}
