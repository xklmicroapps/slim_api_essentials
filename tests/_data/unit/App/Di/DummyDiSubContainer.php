<?php

declare(strict_types=1);

namespace SlimApiEssentials\Tests\Data\App\Di;

use SlimApiEssentials\App\Di\AbstractServices;
use StdClass;

class DummyDiSubContainer extends AbstractServices
{
    public function getSomeService($arg1, $arg2): StdClass
    {
        $class = new StdClass();
        $class->arg1 = $arg1;
        $class->arg2 = $arg2;
        return $class;
    }
}
