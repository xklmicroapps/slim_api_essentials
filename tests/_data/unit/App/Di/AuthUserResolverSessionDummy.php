<?php

declare(strict_types=1);

namespace SlimApiEssentials\Tests\Data\App\Di;

use SlimApiEssentials\App\Services\Auth\SessionUserResolverInterface;
use SlimApiEssentials\App\Services\Auth\SessionFlashMessagesInterface;
use Psr\Http\Message\RequestInterface;

class AuthUserResolverSessionDummy implements SessionUserResolverInterface
{
    public function getUserByUsername(mixed $id): mixed
    {
        return [];
    }

    public function isUserPassOk(mixed $user, string $password): bool
    {
        return false;
    }

    public function getUserSession(RequestInterface $request): mixed
    {
        return [];
    }

    public function getFlashMessages(): ?SessionFlashMessagesInterface
    {
        return null;
    }

    public function setUserSession(mixed $user): void
    {
    }

    public function unsetUserSession(RequestInterface $request): void
    {
    }
}
