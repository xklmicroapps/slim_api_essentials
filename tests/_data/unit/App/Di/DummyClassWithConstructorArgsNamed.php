<?php

declare(strict_types=1);

namespace SlimApiEssentials\Tests\Data\App\Di;

use DateTime;

class DummyClassWithConstructorArgsNamed
{
    public function __construct(
        private mixed $arg1 = 'arg1DefalutValue',
        private mixed $arg2 = new DateTime('1980-01-01')
    ) {
    }

    public function __get($name)
    {
        return $this->$name;
    }
}
