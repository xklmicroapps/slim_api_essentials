<?php

declare(strict_types=1);

namespace SlimApiEssentials\Tests\Data\App\Services;

use SlimApiEssentials\App\Entities\AbstractMessage;

class DummyPayloadMessage extends AbstractMessage
{
    protected string $var1;

    protected array $var2;

    protected int $var3;
}
