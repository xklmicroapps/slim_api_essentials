<?php

declare(strict_types=1);

namespace SlimApiEssentials\Tests\Data\App\Bootstrap;

use SlimApiEssentials\App\Di\AbstractServices;
use Slim\App;
use SlimApiEssentials\App\Middlewares\AuthMiddleware;

class DummySubContainer extends AbstractServices
{
}
