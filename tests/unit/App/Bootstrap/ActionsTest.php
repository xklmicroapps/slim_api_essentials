<?php

declare(strict_types=1);

namespace SlimApiEssentials\Tests\App\Bootstrap;

use Codeception\Test\Unit;
use SlimApiEssentials\App\Bootstrap\Actions;
use SlimApiEssentials\App\Bootstrap\Functions;
use SlimApiEssentials\App\Bootstrap\ErrorHandler;
use SlimApiEssentials\App\Di\Container;
use SlimApiEssentials\App\Di\ContainerActions;
use SlimApiEssentials\App\Di\Services;
use SlimApiEssentials\App\Di\AbstractServices;
use Slim\App;
use Slim\Psr7\Factory\ResponseFactory;
use Slim\MiddlewareDispatcher;
use SlimApiEssentials\App\Middlewares\ContentCheckMiddleware;
use Slim\Middleware\RoutingMiddleware;
use SlimApiEssentials\App\Middlewares\JsonContentParsingMiddleware;
use Slim\Middleware\ErrorMiddleware;
use InvalidArgumentException;
use SlimApiEssentials\Tests\Data\App\Bootstrap\DummySubContainer;

class ActionsTest extends Unit
{
    /**
     * @var \Tests\UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    public function testSetGetAppMiddlewares()
    {
        $responseFactory = $this->make(ResponseFactory::class);
        $container = new Container(new ContainerActions());

        /**
         * keep order of adding middlewares !
         */
        $middlewaresToCheck = [
            ContentCheckMiddleware::class => 1,
            RoutingMiddleware::class => 1,
            JsonContentParsingMiddleware::class => 1,
            ErrorMiddleware::class => 1
        ];
        $middlewaresChecked = [];

        $middlewareDispatcher = $this->make(
            MiddlewareDispatcher::class,
            [
                'add' => function ($middleware) use ($middlewaresToCheck, &$middlewaresChecked) {

                    $middleWareClass = get_class($middleware);
                    $this->tester->assertArrayHasKey(
                        $middleWareClass,
                        $middlewaresToCheck
                    );
                    $middlewaresChecked[$middleWareClass] = 1;

                    return $this->make(MiddlewareDispatcher::class);
                }
            ]
        );

        $app = $this->construct(
            App::class,
            [
                'responseFactory' => $responseFactory,
                'middlewareDispatcher' => $middlewareDispatcher
            ],
            [
                'getContainer' => $container
            ]
        );
        $container->setApp($app);

        $actions = new Actions(new Functions(), new ErrorHandler());

        $actions->setAppMiddlewares($app);

        $this->tester->assertSame(
            $middlewaresToCheck,
            $middlewaresChecked
        );
    }

    public function testGetContainer()
    {
        require_once codecept_data_dir('unit/App/Bootstrap/DummySubContainer.php');

        $actions = $this->make(Actions::class);

        $this->tester->expectThrowable(
            InvalidArgumentException::class,
            function () use ($actions) {
                $actions->getContainer(
                    [
                        'someconfigParam' => 'configValue',
                        'otherConfigParam' => 5,
                        'subContainers' => DummySubContainer::class
                    ]
                );
            }
        );

        $container1 = $actions->getContainer(
            [
                'someconfigParam' => 'configValue',
                'otherConfigParam' => 5,
                'subContainers' => [DummySubContainer::class]
            ]
        );

        $container2 = $actions->getContainer(
            [
                'completelyDirrenentParam' => 'somevaluedifferent'
            ]
        );

        $this->tester->assertInstanceOf(Container::class, $container1);
        $this->tester->assertInstanceOf(Container::class, $container2);
        $this->tester->assertCount(2, $container1->getSubContainers());
        $this->tester->assertCount(2, $container2->getSubContainers());
        $this->tester->assertInstanceOf(DummySubContainer::class, $container1->getSubContainers()[0]);
        $this->tester->assertInstanceOf(DummySubContainer::class, $container2->getSubContainers()[0]);
        $this->tester->assertInstanceOf(Services::class, $container1->getSubContainers()[1]);
        $this->tester->assertInstanceOf(Services::class, $container2->getSubContainers()[1]);
        // the same object instance for another function call
        $this->tester->assertSame(
            spl_object_hash($container1),
            spl_object_hash($container2)
        );

        $this->tester->assertSame(
            [
                'someconfigParam' => 'configValue',
                'otherConfigParam' => 5,
                'subContainers' => ['SlimApiEssentials\Tests\Data\App\Bootstrap\DummySubContainer']
            ],
            $container1->getConfigParameters()
        );

        $this->tester->assertSame(
            [
                'someconfigParam' => 'configValue',
                'otherConfigParam' => 5,
                'subContainers' => ['SlimApiEssentials\Tests\Data\App\Bootstrap\DummySubContainer']
            ],
            $container2->getConfigParameters()
        );
    }
}
