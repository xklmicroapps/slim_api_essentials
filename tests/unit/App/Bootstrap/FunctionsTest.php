<?php

declare(strict_types=1);

namespace SlimApiEssentials\Tests\App\Bootstrap;

use Codeception\Test\Unit;
use Exception;
use LogicException;
use Throwable;
use SlimApiEssentials\App\Exceptions\AuthOauth2Exception;
use SlimApiEssentials\App\Bootstrap\Functions;

class FunctionsTest extends Unit
{
    /**
     * @var \Tests\UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    /**
     * @dataProvider getErrorFromExceptionHtmlData
     */
    public function testGetErrorFromExceptionHtml(
        array $expecteds,
        Throwable $exception,
        bool $displayErrorDetails
    ) {
        $functions = $this->make(Functions::class);

        $this->tester->assertTrue(method_exists($functions, 'getErrorFromExceptionHtml'));

        $response = $functions->getErrorFromExceptionHtml(
            $exception,
            $displayErrorDetails
        );

        foreach ($expecteds as $expected) {
            $this->tester->assertStringContainsString($expected, $response);
        }
    }

    /**
     * @dataProvider getErrorFromExceptionJsonData
     */
    public function testGetErrorFromExceptionJson(
        array $expecteds,
        Throwable $exception,
        bool $displayErrorDetails
    ) {
        $functions = $this->make(Functions::class);

        $this->tester->assertTrue(method_exists($functions, 'getErrorFromExceptionJson'));

        $response = $functions->getErrorFromExceptionJson(
            $exception,
            $displayErrorDetails
        );

        foreach ($expecteds as $expected) {
            $this->tester->assertStringContainsString($expected, $response);
        }
    }

    /**
     * @dataProvider getExceptionPayloadData
     */
    public function testGetExceptionPayload(
        array $expectedPayloadKeyValuePairs,
        Throwable $exception,
        bool $displayErrorDetails
    ) {
        $functions = $this->make(Functions::class);

        $this->tester->assertTrue(method_exists($functions, 'getExceptionPayload'));

        $payload = $functions->getExceptionPayload(
            $exception,
            $displayErrorDetails
        );

        $this->tester->assertIsArray($payload);
        $this->tester->assertArrayHasKey(
            'error',
            $payload
        );
        if ($expectedPayloadKeyValuePairs['error'] !== '__anyvalue__') {
            $this->tester->assertSame(
                $expectedPayloadKeyValuePairs['error'],
                $payload['error']
            );
        }

        $this->tester->assertArrayHasKey(
            'error_description',
            $payload
        );
        if ($expectedPayloadKeyValuePairs['error_description'] !== '__anyvalue__') {
            $this->tester->assertSame(
                $expectedPayloadKeyValuePairs['error_description'],
                $payload['error_description']
            );
        }

        if (array_key_exists('debug', $expectedPayloadKeyValuePairs)) {
            $this->tester->assertArrayHasKey(
                'debug',
                $payload
            );
            foreach ($expectedPayloadKeyValuePairs['debug'] as $debugKey => $debugValue) {
                $this->tester->assertArrayHasKey(
                    $debugKey,
                    $payload['debug']
                );
                if ($debugValue !== '__anyvalue__') {
                    $this->tester->assertSame(
                        $debugValue,
                        $payload['debug'][$debugKey]
                    );
                }
            }
        }
    }

    /**
     * @dataProvider getExceptionHttpStatusData
     */
    public function testGetExceptionHttpStatus(?int $exceptionCode, int $expectedCode)
    {
        $functions = $this->make(Functions::class);

        $this->tester->assertTrue(method_exists($functions, 'getExceptionHttpStatus'));

        $this->tester->assertSame(
            $expectedCode,
            $functions->getExceptionHttpStatus(
                $exceptionCode === null ? new Exception('') : new Exception('', $exceptionCode)
            )
        );
    }

    public function getErrorFromExceptionHtmlData(): array
    {
        return [
            'AuthOauth2ExceptionNoDebug' => [
                [
                    '<title>Error #401</title>'
                ],
                new AuthOauth2Exception(),
                false
            ],
            'ExceptionWithPreviousNoDebug' => [
                [
                    '<title>Error #0</title>'
                ],
                new Exception('', 0, new Exception('previous', 100)),
                false
            ],
            'ExceptionWithPreviousWithDebug' => [
                [
                    '<title>Error #0</title>'
                ],
                new Exception('error desc', 0, new Exception('prev error', 100)),
                true
            ],
            'LogicExceptionWithDebug' => [
                [
                    '<title>Error #99</title>'
                ],
                new LogicException('error desc logic', 99),
                true
            ]
        ];
    }

    public function getErrorFromExceptionJsonData(): array
    {
        return [
            'AuthOauth2ExceptionNoDebug' => [
                [
                    '{"error":"invalid_request","error_description":"401: Unauthorized!"}'
                ],
                new AuthOauth2Exception(),
                false
            ],
            'ExceptionWithPreviousNoDebug' => [
                [
                    '{"error":0,"error_description":""}'
                ],
                new Exception('', 0, new Exception('previous', 100)),
                false
            ],
            'ExceptionWithPreviousWithDebug' => [
                [
                    '{"error":0,"error_description":"error desc","debug":{"class":"Exception","file":"',
                    '"previous"'
                ],
                new Exception('error desc', 0, new Exception('prev error', 100)),
                true
            ],
            'LogicExceptionWithDebug' => [
                [
                    '{"error":99,"error_description":"error desc logic","debug":{"class":"LogicException","file":'
                ],
                new LogicException('error desc logic', 99),
                true
            ]
        ];
    }

    public function getExceptionPayloadData(): array
    {
        return [
            'AuthOauth2ExceptionNoDebug' => [
                [
                    'error' => 'invalid_request',
                    'error_description' => '401: Unauthorized!'
                ],
                new AuthOauth2Exception(),
                false
            ],
            'ExceptionWithPreviousNoDebug' => [
                [
                    'error' => 0,
                    'error_description' => ''
                ],
                new Exception('', 0, new Exception('previous', 100)),
                false
            ],
            'ExceptionWithPreviousWithDebug' => [
                [
                    'error' => 0,
                    'error_description' => 'error desc',
                    'debug' => [
                        'class' => 'Exception',
                        'file' => __FILE__,
                        'line' => '__anyvalue__',
                        'trace' => '__anyvalue__',
                        'previous' => '__anyvalue__'
                    ],
                ],
                new Exception('error desc', 0, new Exception('prev error', 100)),
                true
            ],
            'LogicExceptionWithDebug' => [
                [
                    'error' => 99,
                    'error_description' => 'error desc logic',
                    'debug' => [
                        'class' => 'LogicException',
                        'file' => __FILE__,
                        'line' => '__anyvalue__',
                        'trace' => '__anyvalue__'
                    ],
                ],
                new LogicException('error desc logic', 99),
                true
            ]
        ];
    }

    public function getExceptionHttpStatusData(): array
    {
        return [
            'null -> 500' => [null, 500],
            '0 -> 500' => [0, 500],
            '1 -> 500' => [1, 500],
            '99 -> 500' => [99, 500],
            '100 -> 100' => [100, 100],
            '200 -> 200' => [200, 200],
            '301 -> 301' => [301, 301],
            '302 -> 302' => [302, 302],
            '303 -> 303' => [303, 303],
            '400 -> 400' => [400, 400],
            '401 -> 401' => [401, 401],
            '402 -> 402' => [402, 402],
            '403 -> 403' => [403, 403],
            '404 -> 404' => [404, 404],
            '500 -> 500' => [500, 500],
            '501 -> 501' => [501, 501],
            '502 -> 502' => [502, 502],
            '503 -> 503' => [503, 503],
            '504 -> 504' => [504, 504],
            '505 -> 505' => [505, 505],
            '599 -> 599' => [599, 599],
            '600 -> 500' => [600, 500],
            '801 -> 500' => [801, 500]
        ];
    }
}
