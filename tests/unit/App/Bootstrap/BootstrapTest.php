<?php

declare(strict_types=1);

namespace SlimApiEssentials\Tests\App\Bootstrap;

use Codeception\Test\Unit;
use SlimApiEssentials\App\Bootstrap\Bootstrap;
use SlimApiEssentials\App\Bootstrap\Actions;
use SlimApiEssentials\App\Bootstrap\Functions;
use SlimApiEssentials\App\Bootstrap\ErrorHandler;
use SlimApiEssentials\App\Di\Container;
use SlimApiEssentials\App\Di\ContainerActions;
use SlimApiEssentials\Tests\Data\App\Bootstrap\DummySubContainer;

class BootstrapTest extends Unit
{
    /**
     * @var \Tests\UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
        /**
         * !!! Bootstrap::getApp() is setting custom error handler !!!
         *
         * so restore it back here
         *     if restore not working, there can be an error in getApp method
         *     because Boostrap seterror handlers must be processed only once
         */
        restore_error_handler();
        restore_exception_handler();
    }

    public function testGetApp()
    {
        require_once codecept_data_dir('unit/App/Bootstrap/DummySubContainer.php');

        $functions = $this->make(Functions::class);
        $errorHandler = $this->make(ErrorHandler::class);

        $actions = $this->construct(
            Actions::class,
            [
                'functions' => $functions,
                'errorHandler' => $errorHandler
            ]
        );

        $bootstrap = new Bootstrap(
            [
                'someparam' => 1,
                'some' => 'any',
                'subContainerClass' => [DummySubContainer::class]
            ],
            $actions
        );

        $app1 = $bootstrap->getApp();
        $app2 = $bootstrap->getApp();

        $this->tester->assertSame(
            $app1,
            $app2
        );

        /**
         * seems all is ok if not error is thrown in getApp
         */
    }
}
