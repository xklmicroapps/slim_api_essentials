<?php

declare(strict_types=1);

namespace SlimApiEssentials\Tests\App\Bootstrap;

use Codeception\Test\Unit;
use SlimApiEssentials\App\Bootstrap\SlimDefaultErrorHandler;
use SlimApiEssentials\App\Bootstrap\Functions;
use Slim\App;
use Slim\Psr7\Request as SlimRequest;
use Exception;
use RuntimeException;
use SlimApiEssentials\App\Exceptions\AuthSessionException;
use SlimApiEssentials\App\Exceptions\AuthRemoteException;
use SlimApiEssentials\App\Exceptions\AuthApikeyException;
use SlimApiEssentials\App\Exceptions\AuthBasicException;
use SlimApiEssentials\App\Exceptions\AuthBearerException;
use SlimApiEssentials\App\Exceptions\AuthOauth2Exception;
use SlimApiEssentials\App\Di\Container;
use SlimApiEssentials\App\Di\ContainerActions;
use Slim\Psr7\Factory\ResponseFactory;
use Psr\Http\Message\ResponseInterface;
use Slim\Psr7\Uri;
use Slim\Routing\RouteCollector;
use Slim\Routing\RouteParser;

class SlimDefaultErrorHandlerTest extends Unit
{
    /**
     * @var \Tests\UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    public function testInvokeAuthSessionExceptionHtml()
    {
        $container = new Container(new ContainerActions());
        $container->setConfigParameters(
            [
                'contentCheck' => 'json',
                'authSession' => [
                    'routeNameGet' => 'get/login'
                ]
            ]
        );

        $responseFactory = $this->make(
            ResponseFactory::class
        );

        $routeParser = $this->make(
            RouteParser::class,
            [
                'fullUrlFor' => function (Uri $uri, $routeName) {
                    $this->tester->assertSame(
                        'get/login',
                        $routeName
                    );

                    return 'dummy-returned-url';
                }
            ]
        );

        $routeCollector = $this->make(
            RouteCollector::class,
            [
                'getRouteParser' => $routeParser
            ]
        );

        $app = $this->make(
            App::class,
            [
                'getContainer' => $container,
                'getResponseFactory' => $responseFactory,
                'getRouteCollector' => $routeCollector
            ]
        );
        $container->setApp($app);

        $functions = $this->make(Functions::class);

        $request = $this->make(
            SlimRequest::class,
            [
                'getUri' => new Uri('https://', 'host.name'),
                'getHeaderLine' => 'text/html'
            ]
        );

        $handler = $this->construct(
            SlimDefaultErrorHandler::class,
            [
                'app' => $app,
                'functions' => $functions
            ]
        );

        $response = $handler(
            $request,
            new AuthSessionException(),
            false,
            false,
            false
        );

        $this->tester->assertInstanceOf(
            ResponseInterface::class,
            $response
        );

        $this->tester->assertSame(
            401,
            $response->getStatusCode()
        );

        $this->tester->assertSame(
            'Session realm="Xklmicroapps"',
            $response->getHeaderLine('WWW-Authenticate')
        );
        $this->tester->assertSame(
            'dummy-returned-url',
            $response->getHeaderLine('Xklmicroapps-Authenticate-Location')
        );
        $this->tester->assertSame(
            '',
            $response->getHeaderLine('Location')
        );

        $this->tester->assertSame(
            'text/html',
            $response->getHeaderLine('content-type')
        );

        $expectedhtml = <<<EXPECTEDHTML

                    </html>
                        <head>
                            <title>Error #401</title>
                        </head>
                        <body>
</script>"> <script> (function() {    alert('Server Error... #401');    console.error('{"message":"{\\n    \"error\": 401,\\n    \"error_description\": \"Unauthorized!\"\\n}"}'); })(); </script>

            <div
                class="h30-debug-wrap"
                style="
                    position: fixed;
                    top: 0;
                    left: 0;
                    z-index: 9222333;
                    background: #ffffff;
                    "
            >

            <button
                type="button"
                class="btn btn-sm btn-danger"
                onclick="
                    let display = 'none';
                    document.querySelectorAll('.h30-debug-wrap-sub').forEach((item, key) => {
                        if (!key && item.style.display === 'none') {
                            display = 'block';
                        }
                        item.style.display = display;
                    });
                "
            >
                !!!
            </button>

            <div
                class="h30-debug-wrap-sub"
                style="
                    width: 100vw;
                    height: 100vh;
                    padding: 133px;
                    color: #2e2e2e;
                    font-weight: bold;
                    font-size: 2.2em;
                    overflow: auto;
                    "
                "
            >
<pre>
{
    "error": 401,
    "error_description": "Unauthorized!"
}
</pre>
</div>
</div>

            <script type="text/javascript">
                document.querySelectorAll('.h30-debug-wrap').forEach((item) => {
                    let el = document.querySelector('body');
                    el.append(item);
                });
            </script>

                            </body>
                        </html>
EXPECTEDHTML;

        $this->tester->assertSame(
            /**
             * keep the spaces and indentation like this
             */
            $expectedhtml,
            $response->getBody()->__toString()
        );
    }

    public function testInvokeAuthSessionExceptionJson()
    {
        $container = new Container(new ContainerActions());
        $container->setConfigParameters(
            [
                'authSession' => [
                    'routeNameGet' => 'get/login'
                ]
            ]
        );

        $responseFactory = $this->make(
            ResponseFactory::class
        );

        $routeParser = $this->make(
            RouteParser::class,
            [
                'fullUrlFor' => function (Uri $uri, $routeName) {
                    $this->tester->assertSame(
                        'get/login',
                        $routeName
                    );

                    return 'dummy-returned-url';
                }
            ]
        );

        $routeCollector = $this->make(
            RouteCollector::class,
            [
                'getRouteParser' => $routeParser
            ]
        );

        $app = $this->make(
            App::class,
            [
                'getContainer' => $container,
                'getResponseFactory' => $responseFactory,
                'getRouteCollector' => $routeCollector
            ]
        );
        $container->setApp($app);

        $functions = $this->make(Functions::class);

        $request = $this->make(
            SlimRequest::class,
            [
                'getUri' => new Uri('https://', 'host.name'),
                'getHeaderLine' => 'application/json'
            ]
        );

        $handler = $this->construct(
            SlimDefaultErrorHandler::class,
            [
                'app' => $app,
                'functions' => $functions
            ]
        );

        $response = $handler(
            $request,
            new AuthSessionException(),
            false,
            false,
            false
        );

        $this->tester->assertInstanceOf(
            ResponseInterface::class,
            $response
        );

        $this->tester->assertSame(
            401,
            $response->getStatusCode()
        );

        $this->tester->assertSame(
            'Session realm="Xklmicroapps"',
            $response->getHeaderLine('WWW-Authenticate')
        );
        $this->tester->assertSame(
            'dummy-returned-url',
            $response->getHeaderLine('Xklmicroapps-Authenticate-Location')
        );

        $this->tester->assertSame(
            'application/json',
            $response->getHeaderLine('content-type')
        );


        $this->tester->assertSame(
            '{"error":401,"error_description":"Unauthorized!"}',
            $response->getBody()->__toString()
        );
    }

    public function testInvokeAuthRemoteException()
    {
        $container = new Container(new ContainerActions());

        $responseFactory = $this->make(
            ResponseFactory::class
        );

        $app = $this->make(
            App::class,
            [
                'getContainer' => $container,
                'getResponseFactory' => $responseFactory
            ]
        );
        $container->setApp($app);

        $functions = $this->make(Functions::class);

        $request = $this->make(
            SlimRequest::class,
            [
                'getHeaderLine' => 'application/json'
            ]
        );

        $handler = $this->construct(
            SlimDefaultErrorHandler::class,
            [
                'app' => $app,
                'functions' => $functions
            ]
        );

        $response = $handler(
            $request,
            (new AuthRemoteException())->setWwwAuthenticate('www authtenticate dummy'),
            false,
            false,
            false
        );

        $this->tester->assertInstanceOf(
            ResponseInterface::class,
            $response
        );

        $this->tester->assertSame(
            401,
            $response->getStatusCode()
        );

        $this->tester->assertSame(
            'www authtenticate dummy',
            $response->getHeaderLine('WWW-Authenticate')
        );

        $this->tester->assertSame(
            'application/json',
            $response->getHeaderLine('content-type')
        );

        $this->tester->assertSame(
            '{"error":401,"error_description":"Unauthorized!"}',
            $response->getBody()->__toString()
        );
    }

    public function testInvokeAuthApikeyException()
    {
        $container = new Container(new ContainerActions());

        $responseFactory = $this->make(
            ResponseFactory::class
        );

        $app = $this->make(
            App::class,
            [
                'getContainer' => $container,
                'getResponseFactory' => $responseFactory
            ]
        );
        $container->setApp($app);

        $functions = $this->make(Functions::class);

        $request = $this->make(
            SlimRequest::class,
            [
                'getHeaderLine' => 'application/json'
            ]
        );

        $handler = $this->construct(
            SlimDefaultErrorHandler::class,
            [
                'app' => $app,
                'functions' => $functions
            ]
        );

        $response = $handler(
            $request,
            new AuthApikeyException(),
            false,
            false,
            false
        );

        $this->tester->assertInstanceOf(
            ResponseInterface::class,
            $response
        );

        $this->tester->assertSame(
            401,
            $response->getStatusCode()
        );

        $this->tester->assertSame(
            'ApiKey realm="Xklmicroapps"',
            $response->getHeaderLine('WWW-Authenticate')
        );

        $this->tester->assertSame(
            'application/json',
            $response->getHeaderLine('content-type')
        );

        $this->tester->assertSame(
            '{"error":401,"error_description":"Unauthorized!"}',
            $response->getBody()->__toString()
        );
    }

    public function testInvokeAuthBasicException()
    {
        $container = new Container(new ContainerActions());

        $responseFactory = $this->make(
            ResponseFactory::class
        );

        $app = $this->make(
            App::class,
            [
                'getContainer' => $container,
                'getResponseFactory' => $responseFactory
            ]
        );
        $container->setApp($app);

        $functions = $this->make(Functions::class);

        $request = $this->make(
            SlimRequest::class,
            [
                'getHeaderLine' => 'application/json'
            ]
        );

        $handler = $this->construct(
            SlimDefaultErrorHandler::class,
            [
                'app' => $app,
                'functions' => $functions
            ]
        );

        $response = $handler(
            $request,
            new AuthBasicException(),
            false,
            false,
            false
        );

        $this->tester->assertInstanceOf(
            ResponseInterface::class,
            $response
        );

        $this->tester->assertSame(
            401,
            $response->getStatusCode()
        );

        $this->tester->assertSame(
            'Basic realm="Xklmicroapps"',
            $response->getHeaderLine('WWW-Authenticate')
        );

        $this->tester->assertSame(
            'application/json',
            $response->getHeaderLine('content-type')
        );

        $this->tester->assertSame(
            '{"error":401,"error_description":"Unauthorized!"}',
            $response->getBody()->__toString()
        );
    }

    public function testInvokeAuthOauth2Exception()
    {
        $container = new Container(new ContainerActions());
        $container->setConfigParameters(
            [
                'authOauth2' => [
                    'routeName' => 'post/token'
                ]
            ]
        );

        $responseFactory = $this->make(
            ResponseFactory::class
        );

        $routeParser = $this->make(
            RouteParser::class,
            [
                'fullUrlFor' => function (Uri $uri, $routeName) {
                    $this->tester->assertSame(
                        'post/token',
                        $routeName
                    );

                    return 'dummy-returned-url';
                }
            ]
        );

        $routeCollector = $this->make(
            RouteCollector::class,
            [
                'getRouteParser' => $routeParser
            ]
        );

        $app = $this->make(
            App::class,
            [
                'getContainer' => $container,
                'getResponseFactory' => $responseFactory,
                'getRouteCollector' => $routeCollector
            ]
        );
        $container->setApp($app);

        $functions = $this->make(Functions::class);

        $request = $this->make(
            SlimRequest::class,
            [
                'getUri' => new Uri('https://', 'host.name'),
                'getHeaderLine' => 'application/json'
            ]
        );

        $handler = $this->construct(
            SlimDefaultErrorHandler::class,
            [
                'app' => $app,
                'functions' => $functions
            ]
        );

        $response = $handler(
            $request,
            new AuthOauth2Exception(),
            false,
            false,
            false
        );

        $this->tester->assertInstanceOf(
            ResponseInterface::class,
            $response
        );

        $this->tester->assertSame(
            401,
            $response->getStatusCode()
        );

        $this->tester->assertSame(
            'Bearer realm="Xklmicroapps"',
            $response->getHeaderLine('WWW-Authenticate')
        );
        $this->tester->assertSame(
            'dummy-returned-url',
            $response->getHeaderLine('Xklmicroapps-Authenticate-Location')
        );

        $this->tester->assertSame(
            'application/json',
            $response->getHeaderLine('content-type')
        );


        $this->tester->assertSame(
            '{"error":"invalid_request","error_description":"401: Unauthorized!"}',
            $response->getBody()->__toString()
        );
    }

    public function testInvokeAuthBearerException()
    {
        $container = new Container(new ContainerActions());
        $container->setConfigParameters(
            [
                'authOauth2' => [
                    'routeName' => 'post/token'
                ]
            ]
        );

        $responseFactory = $this->make(
            ResponseFactory::class
        );

        $routeParser = $this->make(
            RouteParser::class,
            [
                'fullUrlFor' => function (Uri $uri, $routeName) {
                    $this->tester->assertSame(
                        'post/token',
                        $routeName
                    );

                    return 'dummy-returned-url';
                }
            ]
        );

        $routeCollector = $this->make(
            RouteCollector::class,
            [
                'getRouteParser' => $routeParser
            ]
        );

        $app = $this->make(
            App::class,
            [
                'getContainer' => $container,
                'getResponseFactory' => $responseFactory,
                'getRouteCollector' => $routeCollector
            ]
        );
        $container->setApp($app);

        $functions = $this->make(Functions::class);

        $request = $this->make(
            SlimRequest::class,
            [
                'getUri' => new Uri('https://', 'host.name'),
                'getHeaderLine' => 'application/json'
            ]
        );

        $handler = $this->construct(
            SlimDefaultErrorHandler::class,
            [
                'app' => $app,
                'functions' => $functions
            ]
        );

        $response = $handler(
            $request,
            new AuthBearerException(),
            false,
            false,
            false
        );

        $this->tester->assertInstanceOf(
            ResponseInterface::class,
            $response
        );

        $this->tester->assertSame(
            401,
            $response->getStatusCode()
        );

        $this->tester->assertSame(
            'Bearer realm="Xklmicroapps"',
            $response->getHeaderLine('WWW-Authenticate')
        );

        $this->tester->assertSame(
            'dummy-returned-url',
            $response->getHeaderLine('Xklmicroapps-Authenticate-Location')
        );


        $this->tester->assertSame(
            'application/json',
            $response->getHeaderLine('content-type')
        );

        $this->tester->assertSame(
            '{"error":401,"error_description":"Unauthorized!"}',
            $response->getBody()->__toString()
        );
    }

    public function testInvokeException()
    {
        $container = new Container(new ContainerActions());

        $responseFactory = $this->make(
            ResponseFactory::class
        );

        $app = $this->make(
            App::class,
            [
                'getContainer' => $container,
                'getResponseFactory' => $responseFactory
            ]
        );
        $container->setApp($app);

        $functions = $this->make(Functions::class);

        $request = $this->make(
            SlimRequest::class,
            [
                'getHeaderLine' => 'application/json'
            ]
        );

        $handler = $this->construct(
            SlimDefaultErrorHandler::class,
            [
                'app' => $app,
                'functions' => $functions
            ]
        );

        $response = $handler(
            $request,
            new Exception('message', 99),
            false,
            false,
            false
        );

        $this->tester->assertInstanceOf(
            ResponseInterface::class,
            $response
        );

        $this->tester->assertSame(
            500,
            $response->getStatusCode()
        );

        $this->tester->assertSame(
            'application/json',
            $response->getHeaderLine('content-type')
        );


        $this->tester->assertSame(
            '{"error":99,"error_description":"message"}',
            $response->getBody()->__toString()
        );
    }
}
