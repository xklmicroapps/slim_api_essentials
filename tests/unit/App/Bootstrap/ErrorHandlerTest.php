<?php

declare(strict_types=1);

namespace SlimApiEssentials\Tests\App\Bootstrap;

use Codeception\Test\Unit;
use SlimApiEssentials\App\Bootstrap\ErrorHandler;
use SlimApiEssentials\App\Bootstrap\Functions;
use ErrorException;
use Throwable;
use Exception;

class ErrorHandlerTest extends Unit
{
    /**
     * @var \Tests\UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    public function testHandleExceptionIsdebug()
    {
        $handler = $this->make(ErrorHandler::class);

        $functions = $this->make(Functions::class);

        $exception = new Exception('message2', 123);

        $this->expectOutputRegex('#^{"error":123,"error_description":"message2","debug":.+$#');

        $handler->handleException(
            $exception,
            true,
            $functions
        );
    }

    public function testHandleExceptionNotdebug()
    {
        $handler = $this->make(ErrorHandler::class);

        $functions = $this->make(Functions::class);

        $exception = new Exception('message', 99);

        $this->expectOutputString('{"error":99,"error_description":"message"}');

        $handler->handleException(
            $exception,
            false,
            $functions
        );
    }

    public function testHandleError()
    {
        $handler = $this->make(ErrorHandler::class);

        try {
            /**
             * test exception with all needed values
             * (expectException is not requied for such case)
             */
            $handler->handleError(
                1,
                'message',
                'file',
                2
            );
            $this->tester->fail('expected exception ErrorException');
        } catch (ErrorException $e) {
            $this->tester->assertSame(1, $e->getCode());
            $this->tester->assertSame(1, $e->getSeverity());
            $this->tester->assertSame('message', $e->getMessage());
            $this->tester->assertSame('file', $e->getFile());
            $this->tester->assertSame(2, $e->getLine());
        } catch (Throwable $e) {
            $this->tester->fail('unexpected exception type: "' . get_class($e) . '"');
        }
    }
}
