<?php

declare(strict_types=1);

namespace SlimApiEssentials\Tests\App\Services\Helpers;

use SlimApiEssentials\App\Services\Helpers\Security as SecurityHelper;
use SlimApiEssentials\App\Services\Helpers\Common as CommonHelper;
use Codeception\Test\Unit;

class SecurityTest extends Unit
{
    /**
     * @var \Tests\UnitTester
     */
    protected $tester;

    protected $helperService;

    protected function _before()
    {
        $this->commonHelper = new CommonHelper();
        $this->helperService = new SecurityHelper(
            $this->commonHelper,
            'some random secret key string long enough AD447-##%$)&--15řšěů§§',
            'secret initial vector string for better encryption D55 ##%&@ds8áí":íěčřŘZŽZ'
        );
    }

    protected function _after()
    {
    }

    /**
     * @dataProvider getSignatureHmacData
     */
    public function testGetSignatureHmac($dataToSign, $expectedSignature)
    {
        $actualSignature = $this->helperService->getSignatureHmac(
            $dataToSign
        );

        $this->assertSame(
            $expectedSignature,
            $actualSignature
        );
    }

    /**
     * @dataProvider getEncryptDecryptData
     */
    public function testEncryptDecrypt($value)
    {
        // encrypt
        $valueEnc = $this->helperService->encrypt($value);
        // check string has changed
        $this->assertStringNotContainsStringIgnoringCase($value, $valueEnc);
        // check valid base64 encoded string
        // @see https://stackoverflow.com/questions/475074/regex-to-parse-or-validate-base64-data/475217#475217
        $this->assertRegExp(
            '#' . $this->commonHelper::BASE64_REGEXP . '#',
            $valueEnc,
            'Invalid base64 encoded string! Encrypted string must be base64 encoded string!'
        );
        // decrypt
        $valueDec = $this->helperService->decrypt($valueEnc);
        // check decoded enc.string is the same as before encoding
        $this->assertSame($value, $valueDec);
    }

    public function getEncryptDecryptData(): array
    {
        return [
            ['string_to_encrypt'],
            ['anotherstring'],
            ['next_string_not_too_short']
        ];
    }

    public function getSignatureHmacData(): array
    {
        return [
            [
                'data-to-get-signature-for',
                '9dc579fc4f89fd0a54882885fefcd6812742bcd9e7f3ee5f18593bdb3ff3258e'
            ],
            [
                'anotherstring',
                'b6a8d466015e4ac6fa2a2fd7e8a38f66137b77b6c95b7aaccc470e333cfd0307'
            ],
            [
                'next_string_not_too_short křžž=ě aééííš {"aaa": "8844ýýššé´=++++"}',
                '25a94511fa1cbbdeda3fb70297958dc71b39dbe646108dde68e2b630462d83e7'
            ]
        ];
    }
}
