<?php

declare(strict_types=1);

namespace SlimApiEssentials\Tests\App\Services\Helpers;

use SlimApiEssentials\App\Services\Helpers\File as FileHelper;
use SlimApiEssentials\App\Services\Helpers\Common as CommonHelper;
use Codeception\Test\Unit;

class FileTest extends Unit
{
    /**
     * @var \Tests\UnitTester
     */
    protected $tester;

    protected $helperService;

    protected function _before()
    {
        $this->commonHelper = new CommonHelper();
        $this->helperService = new FileHelper(
            $this->commonHelper
        );
    }

    protected function _after()
    {
    }

    public function testGetFilePathFromString()
    {
        /**
         * from the real path
         */
        $file = tmpfile();
        fwrite($file, "file path dummy contents");
        $filePathFromString = $this->helperService->getFilePathFromString(
            stream_get_meta_data($file)['uri'], // eg: /tmp/phpFx0513a,
        );
        $this->assertSame(
            'file path dummy contents',
            file_get_contents($filePathFromString)
        );
        fclose($file);

        /**
         * from base64 encoded string
         */
        $filePathFromString = $this->helperService->getFilePathFromString(
            base64_encode('dummy base64 content')
        );
        $this->assertSame(
            'dummy base64 content',
            file_get_contents($filePathFromString)
        );

        /**
         * from the http url
         */
        $filePathFromString = $this->helperService->getFilePathFromString(
            'http://example.org'
        );
        $this->assertStringContainsString(
            '<h1>Example Domain</h1>',
            file_get_contents($filePathFromString)
        );
    }

    /**
     * @dataProvider isStringFilepathData
     */
    public function testIsStringFilepath($value, $expected)
    {
        $this->assertSame(
            $expected,
            $this->helperService->isStringFilepath($value)
        );
    }

    public function isStringFilepathData(): array
    {
        return [
            ["\0", false],
            ['somedummy.nonexistent', false],
            [base64_encode('some encoded string'), false],
            [file_get_contents(__FILE__), false],
            [__DIR__, false],
            [__FILE__, true]
        ];
    }
}
