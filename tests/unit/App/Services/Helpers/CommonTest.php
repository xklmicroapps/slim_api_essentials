<?php

declare(strict_types=1);

namespace SlimApiEssentials\Tests\App\Services\Helpers;

use SlimApiEssentials\App\Services\Helpers\Common as CommonHelper;
use Codeception\Test\Unit;

class CommonTest extends Unit
{
    /**
     * @var \Tests\UnitTester
     */
    protected $tester;

    protected $helperService;

    protected function _before()
    {
        $this->helperService = new CommonHelper();
    }

    protected function _after()
    {
    }

    /**
     * @dataProvider getBase64EncodeDecodeDada
     */
    public function testBase64EncodeDecode($value)
    {
        $this->assertSame(
            base64_encode($value),
            $this->helperService->base64Encode($value),
            '`' . get_class($this->helperService) . '`::base64Encode gives different results
                from native `base64_encode` function'
        );

        $this->assertSame(
            $value,
            $this->helperService->base64Decode(
                $this->helperService->base64Encode($value)
            ),
            'base64Encode/base64Decode modified input string!'
        );
    }

    /**
     * @dataProvider getFormattedSizebytesData
     */
    public function testGetFormattedSizebytes($value, $expected)
    {
        $this->assertSame(
            $expected,
            $this->helperService->getFormattedSizebytes($value)
        );
    }

    public function getBase64EncodeDecodeDada(): array
    {
        return [
            ['some random string'],
            [base64_encode('some random string')],
            [''],
            [
                'Sed et nulla consectetur velit convallis faucibus et in libero. Aliquam at ornare ex. Pellentesque sed urna eget lacus pulvinar tempor vel finibus libero. Mauris iaculis nisi quis porttitor posuere. Interdum et malesuada fames ac ante ipsum primis in faucibus. Sed ullamcorper egestas finibus. Suspendisse nec lacus accumsan, rhoncus ante nec, facilisis eros. Mauris ex augue, scelerisque vitae blandit in, volutpat ullamcorper velit. Nam eros nibh, porttitor id lacinia semper, faucibus sed tellus. Integer vel rhoncus lacus. Nullam accumsan neque placerat orci molestie facilisis. Suspendisse potenti. Nunc non ullamcorper metus, ac dignissim nibh. Suspendisse mollis eros ut ante hendrerit sodales sed non arcu. In hac habitasse platea dictumst.'
            ],
            [
                base64_encode(
                    'Sed et nulla consectetur velit convallis faucibus et in libero. Aliquam at ornare ex. Pellentesque sed urna eget lacus pulvinar tempor vel finibus libero. Mauris iaculis nisi quis porttitor posuere. Interdum et malesuada fames ac ante ipsum primis in faucibus. Sed ullamcorper egestas finibus. Suspendisse nec lacus accumsan, rhoncus ante nec, facilisis eros. Mauris ex augue, scelerisque vitae blandit in, volutpat ullamcorper velit. Nam eros nibh, porttitor id lacinia semper, faucibus sed tellus. Integer vel rhoncus lacus. Nullam accumsan neque placerat orci molestie facilisis. Suspendisse potenti. Nunc non ullamcorper metus, ac dignissim nibh. Suspendisse mollis eros ut ante hendrerit sodales sed non arcu. In hac habitasse platea dictumst.'
                )
            ]
        ];
    }

    public function getFormattedSizebytesData(): array
    {
        return [
            [-10, '0 bytes'],
            [0, '0 bytes'],
            [1, '1 byte'],
            [10, '10 bytes'],
            [1024, '1.00 KB'],
            [1048576, '1.00 MB'],
            [1073741824, '1.00 GB'],
            [1073741824 * 1000, '1 000.00 GB']
        ];
    }
}
