<?php

declare(strict_types=1);

namespace SlimApiEssentials\Tests\App\Services;

use Codeception\Test\Unit;
use SlimApiEssentials\App\Services\Database;
use SlimApiEssentials\App\Services\Helpers\DatabaseKeywords;
use PDO;
use PDOStatement;
use Codeception\Stub\Expected;
use RuntimeException;

// use Slim\Psr7\Response as SlimResponse;
// use Slim\App as App;
// use Slim\Routing\RouteCollector;
// use Slim\Routing\RouteParser;
// use SlimApiEssentials\Tests\Data\App\Services\DummyPayloadMessage;

class DatabaseTest extends Unit
{
    /**
     * @var \Tests\UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    /**
     * @codingStandardsIgnoreStart
     */
    public function test__call()
    {
        /**
         * @codingStandardsIgnoreEnd
         */

        // create mocks
        $pdoStatementMock = $this->make(PDOStatement::class);
        $pdoMock = $this->make(
            PDO::class,
            [
                'beginTransaction' => Expected::exactly(1, true),
                'commit' => Expected::exactly(1, true),
                'lastInsertId' => Expected::exactly(1, 'last id returned'),
                'query' => Expected::exactly(1, new PDOStatement())
            ]
        );

        // create service
        $database = new Database($pdoMock, new DatabaseKeywords());

        $database->beginTransaction();
        $database->commit();
        $database->lastInsertId();
        $database->query('');
    }

    public function testGetSqlLimitOffset()
    {
        // create service mock
        $database = $this->getDatabaseMock();

        $result = $database->getSqlLimitOffset();
        $this->tester->assertSame(
            '',
            $result
        );

        $result = $database->getSqlLimitOffset('100', '2');
        $this->tester->assertSame(
            ' LIMIT 100 OFFSET 2',
            $result
        );

        $result = $database->getSqlLimitOffset('string', '66.45879');
        $this->tester->assertSame(
            ' LIMIT 0 OFFSET 66',
            $result
        );

        $result = $database->getSqlLimitOffset('99');
        $this->tester->assertSame(
            ' LIMIT 99',
            $result
        );

        $result = $database->getSqlLimitOffset(null, '66.544');
        $this->tester->assertSame(
            ' OFFSET 66',
            $result
        );
    }

    public function testGetOrderByArgs()
    {
        // create service mock
        $database = $this->getDatabaseMock();

        $result = $database->getOrderByArgs(
            [
                'somecol1',
                'somecol2',
                'etc...'
            ],
            'somecol1:desc,etc...,'
        );
        $this->tester->assertSame(
            [
                'somecol1' => 'desc',
                'etc...' => 'asc'
            ],
            $result
        );

        $result = $database->getOrderByArgs(
            [
                'somecol1',
                'somecol2',
                'etc...'
            ],
            'somecol1,etc...,somecol2:asc'
        );
        $this->tester->assertSame(
            [
                'somecol1' => 'asc',
                'etc...' => 'asc',
                'somecol2' => 'asc',
            ],
            $result
        );

        $result = $database->getOrderByArgs(
            [
                'somecol1',
                'somecol2',
                'etc...'
            ]
        );
        $this->tester->assertSame(
            [],
            $result
        );

        $this->tester->expectThrowable(
            RuntimeException::class,
            function () use ($database) {
                $database->getOrderByArgs(
                    [
                        'anotherallowedCol',
                        'anotherallowedCol2',
                    ],
                    'somecol1,etc...,somecol2:asc'
                );
            }
        );

        $this->tester->expectThrowable(
            RuntimeException::class,
            function () use ($database) {
                $database->getOrderByArgs(
                    [
                        'somecol1',
                        'somecol2',
                        'etc...'
                    ],
                    'somecol1,etc...,somecol2:NOTALLOWEDDIRECTION'
                );
            }
        );
    }

    public function testGetSqlOrderBy()
    {
        // create service mock
        $database = $this->getDatabaseMock();

        $result = $database->getSqlOrderBy(
            [
                'somecol1',
                'somecol2',
                'etc...',
                // sql keywords (will be quoted)
                'aggregate',
                'YEAR'
            ],
            'somecol1:desc,etc...,aggregate:desc,YEAR'
        );
        $this->tester->assertSame(
            ' ORDER BY somecol1 desc, etc... asc, "aggregate" desc, "YEAR" asc',
            $result
        );

        $result = $database->getSqlOrderBy(
            [
                'somecol1',
                'somecol2',
                'etc...',
                'lower(something->>\'likethis\')'
            ],
            'somecol1,etc...,somecol2:asc,lower(something->>\'likethis\'):desc'
        );
        $this->tester->assertSame(
            ' ORDER BY somecol1 asc, etc... asc, somecol2 asc, lower(something->>\'likethis\') desc',
            $result
        );

        $result = $database->getSqlOrderBy(
            [
                'somecol1',
                'somecol2',
                'etc...'
            ]
        );
        $this->tester->assertSame(
            '',
            $result
        );

        $this->tester->expectThrowable(
            RuntimeException::class,
            function () use ($database) {
                $database->getSqlOrderBy(
                    [
                        'anotherallowedCol',
                        'anotherallowedCol2',
                    ],
                    'somecol1,etc...,somecol2:asc'
                );
            }
        );
    }

    public function testGetSqlFilterCondition()
    {
        // create service mock
        $database = $this->getDatabaseMock();

        $result = $database->getSqlFilterCondition(
            [
                [
                    'someJson' => 'CONDITION eg GTE',
                    'and' => 'COLUMN IN',
                    '__join_string' => 'joinstring-eg-OR'
                ],
                [
                    'someJson' => 'SOMECOND2',
                    'and' => 'ANOTHER COND2',
                ]
            ],
            [
                'someJson' => 'value',
                'and' => [
                    'another',
                    'arrayvalue'
                ],
            ]
        );
        $this->tester->assertSame(
            '(CONDITION eg GTE :someJson joinstring-eg-OR COLUMN IN (:and0,:and1)) AND (SOMECOND2 :someJson AND ANOTHER COND2 (:and0,:and1))',
            $result
        );

        $result = $database->getSqlFilterCondition(
            [
                [
                    'someJson' => 'CONDITION eg GTE',
                    'and' => 'COLUMN IN'
                ],
                [
                    'someJson' => 'SOMECOND2',
                    'and' => 'ANOTHER COND2',
                ]
            ],
            [
                'someJson' => 'value',
                'and' => [
                    'another',
                    'arrayvalue'
                ],
            ],
            'XXX'
        );
        $this->tester->assertSame(
            '(CONDITION eg GTE :someJson AND COLUMN IN (:and0,:and1)) XXX (SOMECOND2 :someJson AND ANOTHER COND2 (:and0,:and1))',
            $result
        );
    }

    public function testGetFilterParamParsed()
    {
        // create service mock
        $database = $this->getDatabaseMock();

        $this->tester->assertSame(
            null,
            $database->getFilterParamParsed(null)
        );
        $this->tester->assertSame(
            [
                'someJson' => 'value',
                'and' => [
                    'another',
                    'arrayvalue'
                ],
            ],
            $database->getFilterParamParsed('{"someJson": "value", "and": ["another", "arrayvalue"]}')
        );

        $this->tester->expectThrowable(
            RuntimeException::class,
            function () use ($database) {
                $database->getFilterParamParsed('json_undecodable_string or whatewher');
            }
        );
    }

    public function testRun()
    {
        // create mocks
        $pdoStatementMock = $this->make(
            PDOStatement::class,
            [
                'bindParam' => Expected::exactly(
                    14,
                    function ($key, $value, $type) {
                        $this->assertRunBindParamKeyValueType(
                            $key,
                            $value,
                            $type
                        );
                        return true;
                    }
                ),
                'execute' => function ($params) {
                    $this->tester->assertSame(
                        [
                            0 => 1,
                            1 => 'someval',
                            '20' => 1,
                            '21' => 'strval',
                            '2strkey' => 88,
                            '2strkey2' => 'strval2',
                            'paramArray0' => 8,
                            'paramArray1' => 'strval',
                            'paramArraystrkey' => 88,
                            'paramArraystrkey2' => 'strval2',
                            'paramString' => 'paramStringValue',
                            'paramInt' => 599,
                            'paramBoolTrue' => '1',
                            'paramBoolFalse' => '0'
                        ],
                        $params
                    );
                    return true;
                }
            ]
        );
        $pdoMock = $this->make(
            PDO::class,
            [
                'prepare' => function ($sql) use ($pdoStatementMock) {
                    $this->tester->assertSame(
                        'SOME DUMMY SQL QUERY STRING',
                        $sql
                    );
                    return $pdoStatementMock;
                }
            ]
        );

        // create service
        $database = new Database($pdoMock, new DatabaseKeywords());

        $database->run(
            'SOME DUMMY SQL QUERY STRING',
            [
                1,
                'someval',
                [
                    1,
                    'strval',
                    'strkey' => 88,
                    'strkey2' => 'strval2'
                ],
                'paramArray' => [
                    8,
                    'strval',
                    'strkey' => 88,
                    'strkey2' => 'strval2'
                ],
                'paramString' => 'paramStringValue',
                'paramInt' => 599,
                'paramBoolTrue' => true,
                'paramBoolFalse' => false
            ]
        );
    }

    private function getDatabaseMock()
    {
        return $this->construct(
            Database::class,
            [
                $this->make(PDO::class),
                new DatabaseKeywords()
            ]
        );
    }

    /**
     * asserts mocked method params
     *
     * @param  [type] $key   [description]
     * @param  [type] $value [description]
     * @param  [type] $type  [description]
     * @return [type]        [description]
     */
    private function assertRunBindParamKeyValueType($key, $value, $type)
    {
        switch ($key) {
            case 0:
                $this->tester->assertSame(
                    1,
                    $value
                );
                $this->tester->assertSame(
                    PDO::PARAM_INT,
                    $type
                );
                break;
            case 1:
                $this->tester->assertSame(
                    'someval',
                    $value
                );
                $this->tester->assertSame(
                    PDO::PARAM_STR,
                    $type
                );
                break;
            case '20':
                $this->tester->assertSame(
                    1,
                    $value
                );
                $this->tester->assertSame(
                    PDO::PARAM_INT,
                    $type
                );
                break;
            case '21':
                $this->tester->assertSame(
                    'strval',
                    $value
                );
                $this->tester->assertSame(
                    PDO::PARAM_STR,
                    $type
                );
                break;
            case '2strkey':
                $this->tester->assertSame(
                    88,
                    $value
                );
                $this->tester->assertSame(
                    PDO::PARAM_INT,
                    $type
                );
                break;
            case '2strkey2':
                $this->tester->assertSame(
                    'strval2',
                    $value
                );
                $this->tester->assertSame(
                    PDO::PARAM_STR,
                    $type
                );
                break;
            case 'paramArray0':
                $this->tester->assertSame(
                    8,
                    $value
                );
                $this->tester->assertSame(
                    PDO::PARAM_INT,
                    $type
                );
                break;
            case 'paramArray1':
                $this->tester->assertSame(
                    'strval',
                    $value
                );
                $this->tester->assertSame(
                    PDO::PARAM_STR,
                    $type
                );
                break;
            case 'paramArraystrkey':
                $this->tester->assertSame(
                    88,
                    $value
                );
                $this->tester->assertSame(
                    PDO::PARAM_INT,
                    $type
                );
                break;
            case 'paramArraystrkey2':
                $this->tester->assertSame(
                    'strval2',
                    $value
                );
                $this->tester->assertSame(
                    PDO::PARAM_STR,
                    $type
                );
                break;
            case 'paramString':
                $this->tester->assertSame(
                    'paramStringValue',
                    $value
                );
                $this->tester->assertSame(
                    PDO::PARAM_STR,
                    $type
                );
                break;
            case 'paramInt':
                $this->tester->assertSame(
                    599,
                    $value
                );
                $this->tester->assertSame(
                    PDO::PARAM_INT,
                    $type
                );
                break;
            case 'paramBoolTrue':
                $this->tester->assertSame(
                    '1',
                    $value
                );
                $this->tester->assertSame(
                    PDO::PARAM_BOOL,
                    $type
                );
                break;
            case 'paramBoolFalse':
                $this->tester->assertSame(
                    '0',
                    $value
                );
                $this->tester->assertSame(
                    PDO::PARAM_BOOL,
                    $type
                );
                break;
            default:
                $this->tester->fail("Unexptected bindParam key: $key");
                break;
        }
    }
}
