<?php

declare(strict_types=1);

namespace SlimApiEssentials\Tests\App\Services;

use RuntimeException;
use Codeception\Test\Unit;
use SlimApiEssentials\App\Services\ResponseHelper;
use SlimApiEssentials\App\Services\RequestHelper;
use Slim\Psr7\Request as SlimRequest;
use Slim\Psr7\Response as SlimResponse;
use Slim\Psr7\Uri as SlimUri;
use Psr\Http\Message\UriInterface;
use Slim\App as App;
use Slim\Routing\RouteCollector;
use Slim\Routing\RouteParser;
use SlimApiEssentials\Tests\Data\App\Services\DummyPayloadMessage;

class ResponseHelperTest extends Unit
{
    /**
     * @var \Tests\UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    public function testGetHtml()
    {
        $payload = '<div>somehtml</div>';
        // create real response
        $response = new SlimResponse();

        // create mocked service
        $responseHelperService = $this->make(ResponseHelper::class);

        // test method
        $responseReturned = $responseHelperService->getHtml(
            $response,
            $payload,
            299
        );

        $this->tester->assertSame(
            [
                'Content-Type' => [
                    0 => 'text/html',
                ]
            ],
            $responseReturned->getHeaders()
        );

        $this->tester->assertIsString((string) $responseReturned->getBody());

        $this->tester->assertSame(
            '<div>somehtml</div>',
            (string) $responseReturned->getBody()
        );

        $this->tester->assertSame(
            299,
            $responseReturned->getStatusCode()
        );
    }

    public function testGetJsonFromText()
    {
        $payload = json_encode(
            [
                'var1' => 'val"lue1"',
                'var2' => ['vall\'ue21', 'čž+i=épok7řůŽ'],
                'var3' => 3,
            ]
        );
        // create real response
        $response = new SlimResponse();

        // create mocked service
        $responseHelperService = $this->make(ResponseHelper::class);

        // test method
        $responseReturned = $responseHelperService->getJsonFromText(
            $response,
            $payload,
            299
        );

        $this->tester->assertSame(
            [
                'Access-Control-Allow-Origin' => [
                    0 => '*',
                ],
                'Access-Control-Allow-Methods' => [
                    0 => 'GET, POST, DELETE, PUT, PATCH, OPTIONS',
                ],
                'Access-Control-Allow-Headers' => [
                    0 => 'Content-Type, Authorization, X-Requested-With',
                ],
                'Access-Control-Expose-Headers' => [
                    0 => 'Content-Type, Location',
                ],
                'Content-Type' => [
                    0 => 'application/json',
                ]
            ],
            $responseReturned->getHeaders()
        );

        $this->tester->assertJson((string) $responseReturned->getBody());
        $this->tester->assertJsonStringEqualsJsonString(
            '{"var1":"val\"lue1\"","var2":["vall\'ue21","čž+i=épok7řůŽ"],"var3":3}',
            (string) $responseReturned->getBody()
        );

        $this->tester->assertSame(
            299,
            $responseReturned->getStatusCode()
        );
    }

    public function testGetWithLocationFromRequest()
    {
        // mock responses
        $response = new SlimResponse();

        // mock routeParser
        $routeParser = $this->make(
            RouteParser::class,
            [
                'urlFor' => function ($routeName, $dataToPass, $queryParams) {
                    $this->tester->assertSame(
                        'dummyroutename',
                        $routeName
                    );
                    $this->tester->assertSame(
                        [
                            'datavar1' => 'some,array,values',
                            'datavar2' => 5,
                            'datavar3' => 'somevalue'
                        ],
                        $dataToPass
                    );
                    $this->tester->assertSame(
                        [
                            'queryparam1' => 1,
                            'queryparam2' => '/queryparam/like/path/mut/not/be/urlencoded'
                        ],
                        $queryParams
                    );

                    /**
                     * Slim\Routing\RouteParser::urlFor() uses relativeUrlFor()
                     *     which uses http_build_query() that returns encoded string
                     */
                    return 'dummy-returned-url?' . http_build_query($queryParams);
                }
            ]
        );

        // mock routeCollector
        $routeCollector = $this->make(
            RouteCollector::class,
            [
                'getRouteParser' => $routeParser
            ]
        );

        // mock app
        $app = $this->make(
            App::class,
            [
                'getRouteCollector' => $routeCollector
            ]
        );

        // create mocked service
        $responseHelperService = $this->construct(
            ResponseHelper::class,
            [
                'app' => $app,
                'requestHelper' => new RequestHelper()
            ]
        );

        $requestJson = $this->make(
            SlimRequest::class,
            [
                'getHeaderLine' => function () {
                    return 'application/json';
                }
            ]
        );
        $request = $this->make(
            SlimRequest::class,
            [
                'getHeaderLine' => function () {
                    return '';
                }
            ]
        );

        $fromRequest = $this->make(
            SlimRequest::class,
            [
                'getUri' => function () {
                    return new SlimUri('https', 'dummyhost');
                }
            ]
        );

        // test method
        $result = $responseHelperService->getWithLocationFromRequest(
            $request,
            $response,
            $fromRequest
        );

        $this->tester->assertSame(
            302,
            $result->getStatusCode()
        );

        $this->tester->assertSame(
            [
                'Location' => [
                    'https://dummyhost/'
                ]
            ],
            $result->getHeaders()
        );

        $resultJson = $responseHelperService->getWithLocationFromRequest(
            $requestJson,
            $response,
            $fromRequest
        );

        $this->tester->assertSame(
            200,
            $resultJson->getStatusCode()
        );

        $this->tester->assertSame(
            [
                'Access-Control-Allow-Origin' => [
                    '*'
                ],
                'Access-Control-Allow-Methods' => [
                    'GET, POST, DELETE, PUT, PATCH, OPTIONS'
                ],
                'Access-Control-Allow-Headers' => [
                    'Content-Type, Authorization, X-Requested-With'
                ],
                'Access-Control-Expose-Headers' => [
                    'Content-Type, Location'
                ],
                'Content-Type' => [
                    'application/json'
                ]
            ],
            $resultJson->getHeaders()
        );
        $this->tester->assertSame(
            '{"redirect":true,"location":"https://dummyhost/"}',
            $resultJson->getBody()->__toString()
        );
    }

    public function testGetWithCors()
    {
        // create real response
        $response = new SlimResponse();

        // create mocked service
        $responseHelperService = $this->make(ResponseHelper::class);

        // test method
        $responseReturned = $responseHelperService->getWithCors(
            $response,
            [
                'another-header' => 'another-header-value',
                'yet-another-header' => ['yet-another-header-valuearray', 'value2']
            ]
        );

        $this->tester->assertSame(
            [
                'Access-Control-Allow-Origin' => [
                    0 => '*',
                ],
                'Access-Control-Allow-Methods' => [
                    0 => 'GET, POST, DELETE, PUT, PATCH, OPTIONS',
                ],
                'Access-Control-Allow-Headers' => [
                    0 => 'Content-Type, Authorization, X-Requested-With',
                ],
                'another-header' => [
                    0 => 'another-header-value',
                ],
                'yet-another-header' => [
                    0 => 'yet-another-header-valuearray',
                    1 => 'value2',
                ]
            ],
            $responseReturned->getHeaders()
        );
    }

    public function testGetJson()
    {
        // create real payload message
        require_once codecept_data_dir('unit/App/Services/DummyPayloadMessage.php');
        $payload = new DummyPayloadMessage(
            [
                'var1' => 'val"lue1"',
                'var2' => ['vall\'ue21', 'čž+i=épok7řůŽ'],
                'var3' => 3,
            ]
        );
        // create real response
        $response = new SlimResponse();

        // create mocked service
        $responseHelperService = $this->make(ResponseHelper::class);

        // test method
        $responseReturned = $responseHelperService->getJson(
            $response,
            $payload,
            299
        );

        $this->tester->assertSame(
            [
                'Access-Control-Allow-Origin' => [
                    0 => '*',
                ],
                'Access-Control-Allow-Methods' => [
                    0 => 'GET, POST, DELETE, PUT, PATCH, OPTIONS',
                ],
                'Access-Control-Allow-Headers' => [
                    0 => 'Content-Type, Authorization, X-Requested-With',
                ],
                'Access-Control-Expose-Headers' => [
                    0 => 'Content-Type, Location',
                ],
                'Content-Type' => [
                    0 => 'application/json',
                ]
            ],
            $responseReturned->getHeaders()
        );

        $this->tester->assertJson((string) $responseReturned->getBody());
        $this->tester->assertJsonStringEqualsJsonString(
            '{"var1":"val\"lue1\"","var2":["vall\'ue21","čž+i=épok7řůŽ"],"var3":3}',
            (string) $responseReturned->getBody()
        );

        $this->tester->assertSame(
            299,
            $responseReturned->getStatusCode()
        );
    }

    public function testGetWithLocationMaxlen()
    {
        // mock responses
        $response = new SlimResponse();

        // mock routeParser
        $routeParser = $this->make(
            RouteParser::class,
            [
                'fullUrlFor' => function (
                    UriInterface $uri,
                    $routeName,
                    $dataToPass,
                    $queryParams
                ) {
                    $this->tester->assertSame(
                        'dummyroutename',
                        $routeName
                    );
                    $this->tester->assertSame(
                        [
                            'datavar1' => 'some,array,values',
                            'datavar2' => 5,
                            'datavar3' => 'somevalue'
                        ],
                        $dataToPass
                    );
                    $this->tester->assertSame(
                        [
                            'queryparam1' => 1,
                            'queryparam2' => '/queryparam/like/path/mut/not/be/urlencoded'
                        ],
                        $queryParams
                    );

                    /**
                     * Slim\Routing\RouteParser::fullUrlFor()
                     *     which uses http_build_query() that returns encoded string
                     */
                    return $uri->__toString()
                        . 'dummy-returned-url?'
                        . http_build_query($queryParams);
                }
            ]
        );

        // mock routeCollector
        $routeCollector = $this->make(
            RouteCollector::class,
            [
                'getRouteParser' => $routeParser
            ]
        );

        // mock app
        $app = $this->make(
            App::class,
            [
                'getRouteCollector' => $routeCollector
            ]
        );

        // create mocked service
        $responseHelperService = $this->construct(
            ResponseHelper::class,
            [
                'app' => $app,
                'requestHelper' => new RequestHelper()
            ]
        );

        $request = $this->make(
            SlimRequest::class,
            [
                'getUri' => function () {
                    return new SlimUri('https', 'dummyhost');
                },
                'getHeaderLine' => function () {
                    return '';
                }
            ]
        );

        $result = $responseHelperService->getWithLocation(
            $request,
            $response,
            'dummyroutename',
            [
                'datavar1' => ['some', 'array', 'values'],
                'datavar2' => 5,
                'datavar3' => 'somevalue'
            ],
            [
                'queryparam1' => 1,
                'queryparam2' => '/queryparam/like/path/mut/not/be/urlencoded'
            ],
            302,
            200
        );

        $this->tester->assertSame(
            302,
            $result->getStatusCode()
        );

        $this->tester->assertSame(
            [
                'Location' => [
                    'https://dummyhost/dummy-returned-url?queryparam1=1&queryparam2=/queryparam/like/path/mut/not/be/urlencoded'
                ]
            ],
            $result->getHeaders()
        );

        $this->tester->expectThrowable(
            new RuntimeException(
                "Location URL is too long!"
                    . " \n"
                    . " Reduce generated URL max strlen 10 chars."
                    . " \n"
                    . " (Actual location URL strlen is 106)"
            ),
            function () use ($responseHelperService, $request, $response) {
                $result = $responseHelperService->getWithLocation(
                    $request,
                    $response,
                    'dummyroutename',
                    [
                        'datavar1' => ['some', 'array', 'values'],
                        'datavar2' => 5,
                        'datavar3' => 'somevalue'
                    ],
                    [
                        'queryparam1' => 1,
                        'queryparam2' => '/queryparam/like/path/mut/not/be/urlencoded'
                    ],
                    302,
                    10
                );
            }
        );
    }

    public function testGetWithLocation()
    {
        // mock responses
        $response = new SlimResponse();

        // mock routeParser
        $routeParser = $this->make(
            RouteParser::class,
            [
                'fullUrlFor' => function (
                    UriInterface $uri,
                    $routeName,
                    $dataToPass,
                    $queryParams
                ) {
                    $this->tester->assertSame(
                        'dummyroutename',
                        $routeName
                    );
                    $this->tester->assertSame(
                        [
                            'datavar1' => 'some,array,values',
                            'datavar2' => 5,
                            'datavar3' => 'somevalue'
                        ],
                        $dataToPass
                    );
                    $this->tester->assertSame(
                        [
                            'queryparam1' => 1,
                            'queryparam2' => '/queryparam/like/path/mut/not/be/urlencoded'
                        ],
                        $queryParams
                    );

                    /**
                     * Slim\Routing\RouteParser::fullUrlFor()
                     *     which uses http_build_query() that returns encoded string
                     */
                    return $uri->__toString()
                        . 'dummy-returned-url?'
                        . http_build_query($queryParams);
                }
            ]
        );

        // mock routeCollector
        $routeCollector = $this->make(
            RouteCollector::class,
            [
                'getRouteParser' => $routeParser
            ]
        );

        // mock app
        $app = $this->make(
            App::class,
            [
                'getRouteCollector' => $routeCollector
            ]
        );

        // create mocked service
        $responseHelperService = $this->construct(
            ResponseHelper::class,
            [
                'app' => $app,
                'requestHelper' => new RequestHelper()
            ]
        );

        $requestJson = $this->make(
            SlimRequest::class,
            [
                'getUri' => function () {
                    return new SlimUri('https', 'dummyhost.json');
                },
                'getHeaderLine' => function () {
                    return 'application/json';
                }
            ]
        );
        $request = $this->make(
            SlimRequest::class,
            [
                'getUri' => function () {
                    return new SlimUri('https', 'dummyhost');
                },
                'getHeaderLine' => function () {
                    return '';
                }
            ]
        );

        // test method
        $result = $responseHelperService->getWithLocation(
            $request,
            $response,
            'dummyroutename',
            [
                'datavar1' => ['some', 'array', 'values'],
                'datavar2' => 5,
                'datavar3' => 'somevalue'
            ],
            [
                'queryparam1' => 1,
                'queryparam2' => '/queryparam/like/path/mut/not/be/urlencoded'
            ]
        );

        $this->tester->assertSame(
            302,
            $result->getStatusCode()
        );

        $this->tester->assertSame(
            [
                'Location' => [
                    'https://dummyhost/dummy-returned-url?queryparam1=1&queryparam2=/queryparam/like/path/mut/not/be/urlencoded'
                ]
            ],
            $result->getHeaders()
        );

        $resultJson = $responseHelperService->getWithLocation(
            $requestJson,
            $response,
            'dummyroutename',
            [
                'datavar1' => ['some', 'array', 'values'],
                'datavar2' => 5,
                'datavar3' => 'somevalue'
            ],
            [
                'queryparam1' => 1,
                'queryparam2' => '/queryparam/like/path/mut/not/be/urlencoded'
            ]
        );

        $this->tester->assertSame(
            200,
            $resultJson->getStatusCode()
        );

        $this->tester->assertSame(
            [
                'Access-Control-Allow-Origin' => [
                    0 => '*',
                ],
                'Access-Control-Allow-Methods' => [
                    0 => 'GET, POST, DELETE, PUT, PATCH, OPTIONS',
                ],
                'Access-Control-Allow-Headers' => [
                    0 => 'Content-Type, Authorization, X-Requested-With',
                ],
                'Access-Control-Expose-Headers' => [
                    0 => 'Content-Type, Location',
                ],
                'Content-Type' => [
                    0 => 'application/json',
                ]
            ],
            $resultJson->getHeaders()
        );

        $this->tester->assertSame(
            '{"redirect":true,"location":"https://dummyhost.json/dummy-returned-url?queryparam1=1&queryparam2=/queryparam/like/path/mut/not/be/urlencoded"}',
            $resultJson->getBody()->__toString()
        );
    }

    public function testGetWithLocationUri()
    {
        // mock responses
        $response = new SlimResponse();

        // mock routeParser
        $routeParser = $this->make(
            RouteParser::class,
            [
                'urlFor' => function ($routeName, $dataToPass, $queryParams) {
                    $this->tester->assertSame(
                        'dummyroutename',
                        $routeName
                    );
                    $this->tester->assertSame(
                        [
                            'datavar1' => 'some,array,values',
                            'datavar2' => 5,
                            'datavar3' => 'somevalue'
                        ],
                        $dataToPass
                    );
                    $this->tester->assertSame(
                        [
                            'queryparam1' => 1,
                            'queryparam2' => '/queryparam/like/path/mut/not/be/urlencoded'
                        ],
                        $queryParams
                    );

                    /**
                     * Slim\Routing\RouteParser::urlFor() uses relativeUrlFor()
                     *     which uses http_build_query() that returns encoded string
                     */
                    return 'dummy-returned-url?' . http_build_query($queryParams);
                }
            ]
        );

        // mock routeCollector
        $routeCollector = $this->make(
            RouteCollector::class,
            [
                'getRouteParser' => $routeParser
            ]
        );

        // mock app
        $app = $this->make(
            App::class,
            [
                'getRouteCollector' => $routeCollector
            ]
        );

        // create mocked service
        $responseHelperService = $this->construct(
            ResponseHelper::class,
            [
                'app' => $app,
                'requestHelper' => new RequestHelper()
            ]
        );

        $requestJson = $this->make(
            SlimRequest::class,
            [
                'getHeaderLine' => function () {
                    return 'application/json';
                }
            ]
        );
        $request = $this->make(
            SlimRequest::class,
            [
                'getHeaderLine' => function () {
                    return '';
                }
            ]
        );

        // test method
        $result = $responseHelperService->getWithLocationUri(
            $request,
            $response,
            'https://dummyurl.some',
            303
        );

        $this->tester->assertSame(
            303,
            $result->getStatusCode()
        );
        $this->tester->assertSame(
            [
                'Location' => [
                    'https://dummyurl.some'
                ]
            ],
            $result->getHeaders()
        );

        $resultJson = $responseHelperService->getWithLocationUri(
            $requestJson,
            $response,
            'https://dummyurl.some',
            303
        );

        $this->tester->assertSame(
            200,
            $resultJson->getStatusCode()
        );
        $this->tester->assertSame(
            [
                'Access-Control-Allow-Origin' => [
                    0 => '*',
                ],
                'Access-Control-Allow-Methods' => [
                    0 => 'GET, POST, DELETE, PUT, PATCH, OPTIONS',
                ],
                'Access-Control-Allow-Headers' => [
                    0 => 'Content-Type, Authorization, X-Requested-With',
                ],
                'Access-Control-Expose-Headers' => [
                    0 => 'Content-Type, Location',
                ],
                'Content-Type' => [
                    0 => 'application/json',
                ]
            ],
            $resultJson->getHeaders()
        );

        $this->tester->assertSame(
            '{"redirect":true,"location":"https://dummyurl.some"}',
            $resultJson->getBody()->__toString()
        );
    }
}
