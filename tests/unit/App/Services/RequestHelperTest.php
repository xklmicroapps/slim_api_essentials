<?php

declare(strict_types=1);

namespace SlimApiEssentials\Tests\App\Services;

use Codeception\Test\Unit;
use SlimApiEssentials\App\Services\RequestHelper;
use Slim\Psr7\Request as SlimRequest;
use Slim\Psr7\Stream as SlimStream;

class RequestHelperTest extends Unit
{
    /**
     * @var \Tests\UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    public function testIsAjax()
    {
        // mock request
        $requestAccept = $this->make(
            SlimRequest::class,
            [
                'getHeaderLine' => function ($header) {
                    if ($header === 'X-Requested-With') {
                        return 'XMLHttpRequest';
                    }
                    return '';
                }
            ]
        );
        $requestOther = $this->make(
            SlimRequest::class,
            [
                'getHeaderLine' => 'anything'
            ]
        );

        // create mocked service
        $requestHelperService = $this->make(RequestHelper::class);

        // run tested method
        $this->tester->assertTrue(
            $requestHelperService->isAjax($requestAccept)
        );
        $this->tester->assertFalse(
            $requestHelperService->isAjax($requestOther)
        );
    }

    public function testIsFormUrlEncoded()
    {
        // mock request
        $requestContentType = $this->make(
            SlimRequest::class,
            [
                'getHeaderLine' => function ($header) {
                    if ($header === 'Content-Type') {
                        return 'application/x-www-form-urlencoded';
                    }
                    return '';
                }
            ]
        );
        $requestOther = $this->make(
            SlimRequest::class,
            [
                'getHeaderLine' => 'anything'
            ]
        );

        // create mocked service
        $requestHelperService = $this->make(RequestHelper::class);

        // run tested method
        $this->tester->assertTrue(
            $requestHelperService->isFormUrlEncoded($requestContentType)
        );
        $this->tester->assertFalse(
            $requestHelperService->isFormUrlEncoded($requestOther)
        );
    }

    public function testIsHtml()
    {
        // mock request
        $requestAccept = $this->make(
            SlimRequest::class,
            [
                'getHeaderLine' => function ($header) {
                    if ($header === 'Accept') {
                        return 'text/html';
                    }
                    return '';
                }
            ]
        );
        $requestAccept2 = $this->make(
            SlimRequest::class,
            [
                'getHeaderLine' => function ($header) {
                    if ($header === 'Accept') {
                        return '*/*';
                    }
                    return '';
                }
            ]
        );
        $requestOther = $this->make(
            SlimRequest::class,
            [
                'getHeaderLine' => 'anything'
            ]
        );
        $requestContentType = $this->make(
            SlimRequest::class,
            [
                'getHeaderLine' => function ($header) {
                    if ($header === 'Content-Type') {
                        return 'text/html';
                    }
                    return '';
                }
            ]
        );
        $requestContentType2 = $this->make(
            SlimRequest::class,
            [
                'getHeaderLine' => function ($header) {
                    if ($header === 'Content-Type') {
                        return '*/*';
                    }
                    return '';
                }
            ]
        );

        // create mocked service
        $requestHelperService = $this->make(RequestHelper::class);

        // run tested method
        $this->tester->assertTrue(
            $requestHelperService->isAcceptHtml($requestAccept)
        );
        $this->tester->assertTrue(
            $requestHelperService->isAcceptHtml($requestAccept2)
        );
        $this->tester->assertFalse(
            $requestHelperService->isAcceptHtml($requestOther)
        );
        $this->tester->assertTrue(
            $requestHelperService->isHtml($requestContentType)
        );
        $this->tester->assertTrue(
            $requestHelperService->isHtml($requestContentType2)
        );
        $this->tester->assertTrue(
            $requestHelperService->isHtml($requestOther)
        );
    }

    public function testIsJson()
    {
        // mock request
        $requestContentType = $this->make(
            SlimRequest::class,
            [
                'getHeaderLine' => function ($header) {
                    if ($header === 'Content-Type') {
                        return 'application/json';
                    }
                    return '';
                }
            ]
        );
        $requestAccept = $this->make(
            SlimRequest::class,
            [
                'getHeaderLine' => function ($header) {
                    if ($header === 'Accept') {
                        return 'application/json';
                    }
                    return '';
                }
            ]
        );
        $requestOther = $this->make(
            SlimRequest::class,
            [
                'getHeaderLine' => 'anything'
            ]
        );

        // create mocked service
        $requestHelperService = $this->make(RequestHelper::class);

        // run tested method
        $this->tester->assertTrue(
            $requestHelperService->isJson($requestContentType)
        );
        $this->tester->assertTrue(
            $requestHelperService->isAcceptJson($requestAccept)
        );
        $this->tester->assertFalse(
            $requestHelperService->isJson($requestOther)
        );
    }

    public function testIsWithBody()
    {
        // set some streams
        $streamEmpty = fopen('php://memory', 'r+');
        $streamWithContent = fopen('php://memory', 'r+');
        fwrite($streamWithContent, 'some dummy content');
        rewind($streamWithContent);

        // mock request
        $requestWithBody = $this->make(SlimRequest::class, ['getBody' => new SlimStream($streamWithContent)]);
        $requestNotWithBody = $this->make(SlimRequest::class, ['getBody' => new SlimStream($streamEmpty)]);

        // create mocked service
        $requestHelperService = $this->make(RequestHelper::class);

        // run tested method
        $this->tester->assertTrue(
            $requestHelperService->isWithBody($requestWithBody)
        );
        $this->tester->assertFalse(
            $requestHelperService->isWithBody($requestNotWithBody)
        );
    }
}
