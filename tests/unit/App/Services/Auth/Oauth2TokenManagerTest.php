<?php

declare(strict_types=1);

namespace SlimApiEssentials\Tests\App\Services\Auth;

use Codeception\Test\Unit;
use SlimApiEssentials\App\Services\Auth\Oauth2TokenManager;
use SlimApiEssentials\App\Services\Helpers\Security as SecurityHelper;
use SlimApiEssentials\App\Services\Helpers\Common as CommonHelper;
use DateTime;
use Codeception\Stub\Expected;

class Oauth2TokenManagerTest extends Unit
{
    /**
     * @var \Tests\UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    /**
     * just some basic test for now is enough
     */
    public function testGetNewTokenFromDataAndPayload()
    {
        $commonHelper = $this->make(
            commonHelper::class
        );
        /**
         * we must use the real instance of security helper to get the resuls
         */
        $securityHelper = new securityHelper(
            $commonHelper,
            'secret key',
            'secret iv'
        );
        $datetimenow = new DateTime('now');

        $service = new Oauth2TokenManager(
            $securityHelper,
            $commonHelper
        );

        $token = $service->getNewTokenFromData(
            $datetimenow,
            'client id',
            'client name'
        );

        /**
         * test new token
         */

        $this->tester->assertRegExp(
            '#[A-Za-z0-9+/=]+#',
            $token->access_token
        );
        $this->tester->assertSame(
            'Bearer',
            $token->token_type
        );
        $this->tester->assertSame(
            0,
            $token->valid_in
        );
        $this->tester->assertSame(
            (Oauth2TokenManager::TOKEN_MINUTES_VALID * 60),
            $token->expires_in
        );

        /**
         * test payload
         */

        $this->assertSame(
            [
                'typ' => 'JWT',
                'alg' => 'HS256'
            ],
            $service->getTokenHeaderFromAccessToken($token->access_token)
        );

        $tokenPayload = $service->getTokenPayloadFromAccessToken($token->access_token);
        $this->assertSame(
            [
                'sub',
                'name',
                'iat',
                'nbf',
                'exp',
                'jti'
            ],
            array_keys($tokenPayload)
        );
        $this->assertSame(
            'client id',
            $tokenPayload['sub']
        );
        $this->assertSame(
            'client name',
            $tokenPayload['name']
        );
        $this->assertSame(
            $datetimenow->getTimestamp(),
            $tokenPayload['iat']
        );
        $this->assertSame(
            $datetimenow->getTimestamp(),
            $tokenPayload['nbf']
        );
        $this->assertSame(
            $datetimenow->getTimestamp() + (Oauth2TokenManager::TOKEN_MINUTES_VALID * 60),
            $tokenPayload['exp']
        );
        $this->tester->assertRegExp(
            '#^jti_[a-z0-9]+\.[0-9]+\.[0-9]+$#',
            $tokenPayload['jti']
        );
        $this->tester->assertRegExp(
            // sha256
            '#^[A-Fa-f0-9]{64}$#',
            $service->getSignatureFromAccessToken($token->access_token)
        );
    }
}
