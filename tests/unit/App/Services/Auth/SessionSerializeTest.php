<?php

declare(strict_types=1);

namespace SlimApiEssentials\Tests\App\Services\Auth;

use Codeception\Test\Unit;
use Slim\Psr7\Request as SlimRequest;
use SlimApiEssentials\App\Services\Auth\SessionSerialize;
use SlimApiEssentials\App\Services\Helpers\Common as CommonHelper;
use SlimApiEssentials\App\Services\Helpers\Security as SecurityHelper;
use Slim\Psr7\Uri;
use Slim\Psr7\Headers;
use Slim\Psr7\Stream;
use SlimApiEssentials\App\Di\Container;
use Codeception\Stub\Expected;
use Slim\Routing\RouteContext;
use Slim\Routing\RouteResolver;
use Slim\Routing\RouteParser;
use Slim\Routing\RoutingResults;
use Slim\Middleware\RoutingMiddleware;

class SessionSerializeTest extends Unit
{
    /**
     * @var \Tests\UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    public function testGetSerializedUnserializedRequest()
    {
        $routingResults = $this->makeEmpty(
            RoutingResults::class,
            [
                'getRouteStatus' => RoutingResults::FOUND
            ]
        );
        $routeResolver = $this->makeEmpty(
            RouteResolver::class,
            [
                'computeRoutingResults' => $routingResults
            ]
        );
        $routeParser = $this->makeEmpty(
            RouteParser::class
        );
        $service = new SessionSerialize(
            $this->make(
                Container::class,
                [
                    'get' => Expected::exactly(
                        2,
                        function ($servicename) {

                            $this->tester->assertSame(
                                SecurityHelper::class,
                                $servicename
                            );

                            return new SecurityHelper(
                                new CommonHelper(),
                                'secretKey',
                                'secretIv'
                            );
                        }
                    ),
                    'getRoutingMiddleware' => Expected::exactly(
                        1,
                        function () use ($routeResolver, $routeParser) {
                            return $this->construct(
                                RoutingMiddleware::class,
                                [
                                    'routeResolver' => $routeResolver,
                                    'routeParser' => $routeParser
                                ]
                            );
                        }
                    )
                ]
            )
        );

        $request = new SlimRequest(
            'method',
            new Uri('https://', 'host'),
            new Headers([], []),
            [],
            [],
            new Stream(
                fopen(
                    sys_get_temp_dir()
                        . '/xkl.test.debug.stream.'
                        . sha1_file(__FILE__),
                    'w'
                )
            )
        );
        /**
         * not allowed to serialize() anonymous objects etc
         */
        $attrRouteParser = new class ()
        {
        };
        $attrRoutingResults = new class ()
        {
        };
        $attrRouteContext = new class ()
        {
        };
        $attrBasepath = 'basepathattrvalue';

        $request = $request->withAttribute(
            RouteContext::ROUTE_PARSER,
            $attrRouteParser
        )->withAttribute(
            RouteContext::ROUTING_RESULTS,
            $attrRoutingResults
        )->withAttribute(
            RouteContext::ROUTE,
            $attrRouteContext
        )->withAttribute(
            RouteContext::BASE_PATH,
            $attrBasepath
        );

        $this->assertSame(
            $attrRouteParser,
            $request->getAttribute(RouteContext::ROUTE_PARSER)
        );
        $this->assertSame(
            $attrRoutingResults,
            $request->getAttribute(RouteContext::ROUTING_RESULTS)
        );
        $this->assertSame(
            $attrRouteContext,
            $request->getAttribute(RouteContext::ROUTE)
        );
        $this->assertSame(
            $attrBasepath,
            $request->getAttribute(RouteContext::BASE_PATH)
        );

        $requestSerialized = $service->getSerializedRequest($request);
        $this->assertSame(
            trim(
                file_get_contents(
                    codecept_data_dir('unit/App/Services/Auth/serializedRequest.txt')
                )
            ),
            $requestSerialized
        );

        $requestUnserialized = $service->getUnserializedRequest($requestSerialized);

        $this->assertInstanceOf(SlimRequest::class, $requestUnserialized);
        $this->assertSame(
            $request->getMethod(),
            $requestUnserialized->getMethod()
        );
        $this->assertSame(
            $request->getUri()->__toString(),
            $requestUnserialized->getUri()->__toString()
        );

        $this->assertNotSame(
            $attrRouteParser,
            $requestUnserialized->getAttribute(RouteContext::ROUTE_PARSER)
        );
        $this->assertNotSame(
            $attrRoutingResults,
            $requestUnserialized->getAttribute(RouteContext::ROUTING_RESULTS)
        );
        $this->assertNotSame(
            $attrRouteContext,
            $requestUnserialized->getAttribute(RouteContext::ROUTE)
        );

        $this->assertSame(
            $attrBasepath,
            $requestUnserialized->getAttribute(RouteContext::BASE_PATH)
        );

        /**
         * instanceof base request attributes
         *     instance of "route" attribute is unknown as it is auto created in some mocked object
         */
        $this->assertInstanceOf(
            get_class($routeParser),
            $requestUnserialized->getAttribute(RouteContext::ROUTE_PARSER)
        );
        $this->assertInstanceOf(
            get_class($routingResults),
            $requestUnserialized->getAttribute(RouteContext::ROUTING_RESULTS)
        );
    }

    public function testGetEncodedDecoded()
    {
        $service = new SessionSerialize(
            $this->make(
                Container::class,
                [
                    'get' => Expected::exactly(
                        2,
                        function ($servicename) {

                            $this->tester->assertSame(
                                CommonHelper::class,
                                $servicename
                            );

                            return new CommonHelper();
                        }
                    )
                ]
            )
        );

        $valueToEncode = 'somevalue';

        $valueEncoded = $service->getEncoded($valueToEncode);

        $this->assertNotSame($valueToEncode, $valueEncoded);

        $valueDecoded = $service->getDecoded($valueEncoded);

        $this->assertSame($valueToEncode, $valueDecoded);
    }

    public function testGetEncryptedDecrypted()
    {
        $service = new SessionSerialize(
            $this->make(
                Container::class,
                [
                    'get' => Expected::exactly(
                        2,
                        function ($servicename) {

                            $this->tester->assertSame(
                                SecurityHelper::class,
                                $servicename
                            );

                            return new SecurityHelper(
                                new CommonHelper(),
                                'secretKey',
                                'secretIv'
                            );
                        }
                    )
                ]
            )
        );

        $valueToEncode = 'somevalue';

        $valueEncoded = $service->getEncrypted($valueToEncode);

        $this->assertNotSame($valueToEncode, $valueEncoded);

        $valueDecoded = $service->getDecrypted($valueEncoded);

        $this->assertSame($valueToEncode, $valueDecoded);
    }
}
