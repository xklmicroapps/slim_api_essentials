<?php

declare(strict_types=1);

namespace SlimApiEssentials\Tests\App\Services\Auth;

use Codeception\Test\Unit;
use SlimApiEssentials\App\Services\Auth\Oauth2;
use SlimApiEssentials\App\Services\Auth\Oauth2TokenManager;
use Slim\Psr7\Request as SlimRequest;
use SlimApiEssentials\Tests\Data\App\Di\AuthUserResolverOauth2Dummy;
use SlimApiEssentials\App\Services\Auth\AuthActions;
use SlimApiEssentials\App\Exceptions\AuthOauth2Exception;
use Codeception\Stub\Expected;
use SlimApiEssentials\App\Entities\Oauth2JwtToken;

class Oauth2Test extends Unit
{
    /**
     * @var \Tests\UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    public function testGetNewTokenFromRequestBadClientSecret()
    {
        // mock request
        $request = $this->make(
            SlimRequest::class,
            [
                'getParsedBody' => Expected::once(
                    [
                        'grant_type' => 'client_credentials'
                    ]
                )
            ]
        );
        $userResolver = $this->make(
            AuthUserResolverOauth2Dummy::class,
            [
                'getClientByClientId' => Expected::once(
                    function ($clientid) {
                        $this->tester->assertSame('clientid', $clientid);
                        return ['some client data'];
                    }
                ),
                'isClientSecretOk' => Expected::once(
                    function ($client) {
                        $this->tester->assertSame(['some client data'], $client);
                        /**
                         * false as an bad secret
                         */
                        return false;
                    }
                )
            ]
        );
        $authActions = $this->make(
            AuthActions::class,
            [
                'getBasicCredentialsFromRequest' => Expected::once(
                    ['clientid', 'clientsecret']
                )
            ]
        );
        $tokenManger = $this->make(Oauth2TokenManager::class);

        $authService = new Oauth2(
            $userResolver,
            $authActions,
            $tokenManger
        );

        // run tested method
        $this->tester->expectThrowable(
            new AuthOauth2Exception(
                'Invalid client secret!'
            ),
            function () use ($authService, $request) {
                $authService->getNewTokenFromRequest($request);
            }
        );
    }

    public function testGetNewTokenFromRequestBadClientId()
    {
        // mock request
        $request = $this->make(
            SlimRequest::class,
            [
                'getParsedBody' => Expected::once(
                    [
                        'grant_type' => 'client_credentials'
                    ]
                )
            ]
        );
        $userResolver = $this->make(
            AuthUserResolverOauth2Dummy::class,
            [
                'getClientByClientId' => Expected::once(
                    function ($clientid) {
                        $this->tester->assertSame('clientid', $clientid);
                        /**
                         * null as an empty client data
                         */
                        return null;
                    }
                ),
                'isClientSecretOk' => Expected::never()
            ]
        );
        $authActions = $this->make(
            AuthActions::class,
            [
                'getBasicCredentialsFromRequest' => Expected::once(
                    ['clientid', 'clientsecret']
                )
            ]
        );
        $tokenManger = $this->make(Oauth2TokenManager::class);

        $authService = new Oauth2(
            $userResolver,
            $authActions,
            $tokenManger
        );

        // run tested method
        $this->tester->expectThrowable(
            new AuthOauth2Exception(
                'Client not found!'
            ),
            function () use ($authService, $request) {
                $authService->getNewTokenFromRequest($request);
            }
        );
    }

    public function testCheckBadExpiresIn()
    {
        // mock request
        $request = $this->make(SlimRequest::class);
        $userResolver = $this->make(AuthUserResolverOauth2Dummy::class);
        $authActions = $this->make(
            AuthActions::class,
            [
                'getAuthorizationHeaderContent' => Expected::once(
                    'Bearer somedummytoken'
                )
            ]
        );
        $tokenManger = $this->make(
            Oauth2TokenManager::class,
            [
                'getSignatureFromAccessToken' => Expected::once(
                    'tokensignature-same'
                ),
                'getSignatureFromPayload' => Expected::once(
                    'tokensignature-same'
                ),
                'getTokenHeaderFromAccessToken' => Expected::once(
                    ['tokenheader']
                ),
                'getTokenPayloadFromAccessToken' => Expected::once(
                    ['tokenpayload']
                ),
                'getValidInFromAccessToken' => Expected::once(
                    0
                ),
                'getExpiresInFromAccessToken' => Expected::once(
                    0
                )
            ]
        );

        $authService = new Oauth2(
            $userResolver,
            $authActions,
            $tokenManger
        );

        // run tested method
        $this->tester->expectThrowable(
            new AuthOauth2Exception(
                'Expired access token!'
            ),
            function () use ($authService, $request) {
                $authService->check($request);
            }
        );
    }

    public function testCheckBadValidIn()
    {
        // mock request
        $request = $this->make(SlimRequest::class);
        $userResolver = $this->make(AuthUserResolverOauth2Dummy::class);
        $authActions = $this->make(
            AuthActions::class,
            [
                'getAuthorizationHeaderContent' => Expected::once(
                    'Bearer somedummytoken'
                )
            ]
        );
        $tokenManger = $this->make(
            Oauth2TokenManager::class,
            [
                'getSignatureFromAccessToken' => Expected::once(
                    'tokensignature-same'
                ),
                'getSignatureFromPayload' => Expected::once(
                    'tokensignature-same'
                ),
                'getTokenHeaderFromAccessToken' => Expected::once(
                    ['tokenheader']
                ),
                'getTokenPayloadFromAccessToken' => Expected::once(
                    ['tokenpayload']
                ),
                'getValidInFromAccessToken' => Expected::once(
                    3
                )
            ]
        );

        $authService = new Oauth2(
            $userResolver,
            $authActions,
            $tokenManger
        );

        // run tested method
        $this->tester->expectThrowable(
            new AuthOauth2Exception(
                'Access token is not yet valid! Seconds to wait for validity: 3'
            ),
            function () use ($authService, $request) {
                $authService->check($request);
            }
        );
    }

    public function testCheckBadSignature()
    {
        // mock request
        $request = $this->make(SlimRequest::class);
        $userResolver = $this->make(AuthUserResolverOauth2Dummy::class);
        $authActions = $this->make(
            AuthActions::class,
            [
                'getAuthorizationHeaderContent' => Expected::once(
                    'Bearer somedummytoken'
                )
            ]
        );
        $tokenManger = $this->make(
            Oauth2TokenManager::class,
            [
                'getSignatureFromAccessToken' => Expected::once(
                    'tokensignature-same'
                ),
                'getSignatureFromPayload' => Expected::once(
                    'tokensignature-different'
                ),
                'getTokenHeaderFromAccessToken' => Expected::once(
                    ['tokenheader']
                ),
                'getTokenPayloadFromAccessToken' => Expected::once(
                    ['tokenpayload']
                )
            ]
        );

        $authService = new Oauth2(
            $userResolver,
            $authActions,
            $tokenManger
        );

        // run tested method
        $this->tester->expectThrowable(
            new AuthOauth2Exception('Invalid access token (signature)!'),
            function () use ($authService, $request) {
                $authService->check($request);
            }
        );
    }

    public function testCheckOk()
    {
        // mock request
        $request = $this->make(SlimRequest::class);
        $userResolver = $this->make(AuthUserResolverOauth2Dummy::class);
        $authActions = $this->make(
            AuthActions::class,
            [
                'getAuthorizationHeaderContent' => Expected::once(
                    'Bearer somedummytoken'
                )
            ]
        );
        $tokenManger = $this->make(
            Oauth2TokenManager::class,
            [
                'getSignatureFromAccessToken' => Expected::once(
                    'tokensignature-same'
                ),
                'getSignatureFromPayload' => Expected::once(
                    function ($header, $payload) {
                        $this->tester->assertSame(['tokenheader'], $header);
                        $this->tester->assertSame(['tokenpayload'], $payload);

                        return 'tokensignature-same';
                    }
                ),
                'getTokenHeaderFromAccessToken' => Expected::once(
                    ['tokenheader']
                ),
                'getTokenPayloadFromAccessToken' => Expected::once(
                    ['tokenpayload']
                ),
                'getValidInFromAccessToken' => Expected::once(
                    0
                ),
                'getExpiresInFromAccessToken' => Expected::once(
                    1
                )
            ]
        );

        $authService = new Oauth2(
            $userResolver,
            $authActions,
            $tokenManger
        );

        // run tested method
        $authService->check($request);
    }
}
