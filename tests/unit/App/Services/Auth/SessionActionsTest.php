<?php

declare(strict_types=1);

namespace SlimApiEssentials\Tests\App\Services\Auth;

use Codeception\Test\Unit;
use SlimApiEssentials\App\Services\Auth\SessionActions;
use SlimApiEssentials\App\Services\RequestHelper;
use SlimApiEssentials\App\Services\ResponseHelper;
use Slim\Psr7\Request as SlimRequest;
use Codeception\Stub\Expected;
use Slim\Routing\RouteContext;
use Slim\Interfaces\RouteInterface;
use Slim\Interfaces\RouteParserInterface;
use Slim\Routing\RoutingResults;
use SlimApiEssentials\Tests\Data\App\Di\AuthUserResolverSessionDummy;
use Slim\Psr7\Factory\ResponseFactory;

class SessionActionsTest extends Unit
{
    /**
     * @var \Tests\UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    public function testIsRouteName()
    {
        $requestHelper = $this->make(
            RequestHelper::class
        );
        $responseHelper = $this->make(
            ResponseHelper::class
        );
        $userResolver = $this->make(
            AuthUserResolverSessionDummy::class
        );
        $responseFactory = $this->make(
            ResponseFactory::class
        );
        $actions = new SessionActions(
            $requestHelper,
            $responseHelper,
            $userResolver,
            $responseFactory
        );

        $request = $this->make(
            SlimRequest::class,
            [
                'getAttribute' => Expected::exactly(
                    8,
                    function ($val) {
                        if ($val === RouteContext::ROUTE) {
                            return $this->makeEmpty(
                                RouteInterface::class,
                                [
                                    'getName' => Expected::once(
                                        'some another route name'
                                    )
                                ]
                            );
                        }
                        if ($val === RouteContext::ROUTE_PARSER) {
                            return $this->makeEmpty(RouteParserInterface::class);
                        }
                        if ($val === RouteContext::ROUTING_RESULTS) {
                            return $this->makeEmpty(RoutingResults::class);
                        }

                        return 'somevalue';
                    }
                )
            ]
        );

        $this->assertTrue(
            $actions->isRouteName($request, 'some another route name')
        );

        $this->assertFalse(
            $actions->isRouteName($request, 'BAD some another route name')
        );
    }

    public function testGetRouteFromRequest()
    {
        $requestHelper = $this->make(
            RequestHelper::class
        );
        $responseHelper = $this->make(
            ResponseHelper::class
        );
        $userResolver = $this->make(
            AuthUserResolverSessionDummy::class
        );
        $responseFactory = $this->make(
            ResponseFactory::class
        );
        $actions = new SessionActions(
            $requestHelper,
            $responseHelper,
            $userResolver,
            $responseFactory
        );

        $request = $this->make(
            SlimRequest::class,
            [
                'getAttribute' => Expected::exactly(
                    4,
                    function ($val) {
                        if ($val === RouteContext::ROUTE) {
                            return $this->makeEmpty(
                                RouteInterface::class
                            );
                        }
                        if ($val === RouteContext::ROUTE_PARSER) {
                            return $this->makeEmpty(RouteParserInterface::class);
                        }
                        if ($val === RouteContext::ROUTING_RESULTS) {
                            return $this->makeEmpty(RoutingResults::class);
                        }

                        return 'somevalue';
                    }
                )
            ]
        );

        $result = $actions->getRouteFromRequest($request);

        $this->assertInstanceOf(RouteInterface::class, $result);
    }

    public function testGetRequestParamsToUseWithBody()
    {
        $requestHelper = $this->make(
            RequestHelper::class,
            [
                'isWithBody' => Expected::exactly(
                    1,
                    true
                )
            ]
        );
        $responseHelper = $this->make(
            ResponseHelper::class
        );
        $userResolver = $this->make(
            AuthUserResolverSessionDummy::class
        );
        $responseFactory = $this->make(
            ResponseFactory::class
        );
        $actions = new SessionActions(
            $requestHelper,
            $responseHelper,
            $userResolver,
            $responseFactory
        );

        $request = $this->make(
            SlimRequest::class,
            [
                'getParsedBody' => Expected::once(
                    ['parsedbodydata']
                ),
                'getQueryParams' => Expected::never(
                    ['queryparamsdata']
                )
            ]
        );

        $result = $actions->getRequestParamsToUse($request);

        $this->tester->assertSame(
            ['parsedbodydata'],
            $result
        );
    }

    public function testGetRequestParamsToUseWithoutBody()
    {
        $requestHelper = $this->make(
            RequestHelper::class,
            [
                'isWithBody' => Expected::exactly(
                    1,
                    false
                )
            ]
        );
        $responseHelper = $this->make(
            ResponseHelper::class
        );
        $userResolver = $this->make(
            AuthUserResolverSessionDummy::class
        );
        $responseFactory = $this->make(
            ResponseFactory::class
        );
        $actions = new SessionActions(
            $requestHelper,
            $responseHelper,
            $userResolver,
            $responseFactory
        );

        $request = $this->make(
            SlimRequest::class,
            [
                'getParsedBody' => Expected::never(
                    ['parsedbodydata']
                ),
                'getQueryParams' => Expected::once(
                    ['queryparamsdata']
                )
            ]
        );

        $result = $actions->getRequestParamsToUse($request);

        $this->tester->assertSame(
            ['queryparamsdata'],
            $result
        );
    }
}
