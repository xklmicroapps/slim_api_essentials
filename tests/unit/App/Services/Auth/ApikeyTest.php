<?php

declare(strict_types=1);

namespace SlimApiEssentials\Tests\App\Services\Auth;

use Codeception\Test\Unit;
use SlimApiEssentials\App\Services\Auth\Apikey;
use Slim\Psr7\Request as SlimRequest;
use SlimApiEssentials\Tests\Data\App\Di\AuthUserResolverApikeyDummy;
use SlimApiEssentials\App\Services\Auth\AuthActions;
use SlimApiEssentials\App\Exceptions\AuthApikeyException;
use Codeception\Stub\Expected;

class ApikeyTest extends Unit
{
    /**
     * @var \Tests\UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    public function testCheckExpiredApikey()
    {
        // mock request
        $request = $this->make(SlimRequest::class);
        $userResolver = $this->make(
            AuthUserResolverApikeyDummy::class,
            [
                'getUserByApikey' => 'someuser',
                // set is expired
                'isApikeyExpired' => true
            ]
        );
        $authActions = $this->make(
            AuthActions::class,
            [
                'getAuthorizationHeaderContent' => Expected::once(
                    'ApiKey dummykey'
                )
            ]
        );

        $authService = new Apikey(
            $userResolver,
            $authActions
        );

        // run tested method
        $this->tester->expectThrowable(
            new AuthApikeyException('Expired api key!'),
            function () use ($authService, $request) {
                $authService->check($request);
            }
        );
    }

    public function testCheckBadApikey()
    {
        // mock request
        $request = $this->make(SlimRequest::class);
        $userResolver = $this->make(
            AuthUserResolverApikeyDummy::class,
            [
                // get empty user
                'getUserByApikey' => '',
                'isApikeyExpired' => false
            ]
        );
        $authActions = $this->make(
            AuthActions::class,
            [
                'getAuthorizationHeaderContent' => Expected::once(
                    'ApiKey dummykey'
                )
            ]
        );

        $authService = new Apikey(
            $userResolver,
            $authActions
        );

        // run tested method
        $this->tester->expectThrowable(
            new AuthApikeyException('Unknown api key!'),
            function () use ($authService, $request) {
                $authService->check($request);
            }
        );
    }

    public function testCheckBadHeader()
    {
        // mock request
        $request = $this->make(SlimRequest::class);
        $userResolver = $this->make(
            AuthUserResolverApikeyDummy::class,
            [
                'getUserByApikey' => ['someuser'],
                'isApikeyExpired' => false
            ]
        );
        $authActions = $this->make(
            AuthActions::class,
            [
                'getAuthorizationHeaderContent' => Expected::once(
                    'bad header content'
                )
            ]
        );

        $authService = new Apikey(
            $userResolver,
            $authActions
        );

        // run tested method
        $this->tester->expectThrowable(
            new AuthApikeyException('Unknown Authorization header! "bad header content"'),
            function () use ($authService, $request) {
                $authService->check($request);
            }
        );
    }

    public function testCheckOk()
    {
        // mock request
        $request = $this->make(SlimRequest::class);
        $userResolver = $this->make(
            AuthUserResolverApikeyDummy::class,
            [
                'getUserByApikey' => ['someuser'],
                'isApikeyExpired' => false
            ]
        );
        $authActions = $this->make(
            AuthActions::class,
            [
                'getAuthorizationHeaderContent' => Expected::once(
                    'ApiKey dummykey'
                )
            ]
        );

        $authService = new Apikey(
            $userResolver,
            $authActions
        );

        // run tested method
        $authService->check($request);
    }
}
