<?php

declare(strict_types=1);

namespace SlimApiEssentials\Tests\App\Services\Auth;

use Codeception\Test\Unit;
use SlimApiEssentials\App\Services\Auth\Remote;
use Slim\Psr7\Request as SlimRequest;
use SlimApiEssentials\App\Services\Auth\AuthActions;
use SlimApiEssentials\App\Exceptions\AuthRemoteException;
use Codeception\Stub\Expected;
use GuzzleHttp\Client as GuzzleClient;
use Psr\Http\Message\ResponseInterface;
use GuzzleHttp\Exception\BadResponseException as GuzzleBadResponseException;
use GuzzleHttp\Psr7\Response as GuzzleResponse;
use Psr\Http\Message\RequestInterface;

class RemoteTest extends Unit
{
    /**
     * @var \Tests\UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    public function testCheckBadResponse()
    {
        // mock request
        $request = $this->make(SlimRequest::class);
        $authActions = $this->make(
            AuthActions::class,
            [
                'getAuthorizationHeaderContent' => Expected::exactly(
                    2,
                    'auth headercontent'
                )
            ]
        );

        $guzzleExceptionResponse = $this->make(
            GuzzleResponse::class,
            [
                'hasHeader' => Expected::exactly(
                    2,
                    function ($value) {
                        return true;
                    }
                ),
                'getHeaderLine' => Expected::exactly(
                    2,
                    function ($value) {
                        if ($value === 'WWW-Authenticate') {
                            return 'WWW-Authenticate-headervalue';
                        }
                        if ($value === 'Xklmicroapps-Authenticate-Location') {
                            return 'Xklmicroapps-Authenticate-Location-headervalue';
                        }
                    }
                ),
                'getStatusCode' => 444
            ]
        );

        /**
         * seems not possible to mock exception
         *     so make the real instance of it with some mocked values
         */
        $guzzleException = new GuzzleBadResponseException(
            'error msg',
            $this->makeEmpty(RequestInterface::class),
            $guzzleExceptionResponse
        );

        $guzzleClient = $this->make(
            GuzzleClient::class,
            [
                'request' => Expected::once(
                    function (string $method, string $url, array $options) use ($guzzleException) {
                        $this->tester->assertSame('GET', $method);
                        $this->tester->assertSame('dummy-url', $url);
                        $this->tester->assertSame(
                            [
                                'verify' => true,
                                'headers' => [
                                    'Authorization' => 'auth headercontent'
                                ]
                            ],
                            $options
                        );

                        throw $guzzleException;
                    }
                )
            ]
        );

        $authService = new Remote(
            [
                'auth' => 'dummy-url'
            ],
            true,
            $authActions,
            $guzzleClient
        );

        try {
            // run tested method
            $authService->check($request);
        } catch (AuthRemoteException $e) {
            // must throw an exception, so use later to check all its values
        }

        $this->tester->assertSame(
            'error msg',
            $e->getMessage()
        );
        $this->tester->assertSame(
            444,
            $e->getCode()
        );
        $this->tester->assertSame(
            'WWW-Authenticate-headervalue',
            $e->getWwwAuthenticate()
        );
        $this->tester->assertSame(
            'Xklmicroapps-Authenticate-Location-headervalue',
            $e->getAuthenticateLocation()
        );
    }

    public function testCheckOk()
    {
        // mock request
        $request = $this->make(SlimRequest::class);
        $authActions = $this->make(
            AuthActions::class,
            [
                'getAuthorizationHeaderContent' => Expected::exactly(
                    2,
                    'auth headercontent'
                )
            ]
        );
        $guzzleClient = $this->make(
            GuzzleClient::class,
            [
                'request' => Expected::once(
                    function (string $method, string $url, array $options) {
                        $this->tester->assertSame('GET', $method);
                        $this->tester->assertSame('dummy-url', $url);
                        $this->tester->assertSame(
                            [
                                'verify' => true,
                                'headers' => [
                                    'Authorization' => 'auth headercontent'
                                ]
                            ],
                            $options
                        );

                        return $this->makeEmpty(ResponseInterface::class);
                    }
                )
            ]
        );

        $authService = new Remote(
            [
                'auth' => 'dummy-url'
            ],
            true,
            $authActions,
            $guzzleClient
        );

        // run tested method
        $authService->check($request);
    }
}
