<?php

declare(strict_types=1);

namespace SlimApiEssentials\Tests\App\Services\Auth;

use Codeception\Test\Unit;
use SlimApiEssentials\App\Services\Helpers\Common as CommonHelper;
use Slim\Psr7\Request as SlimRequest;
use SlimApiEssentials\App\Services\Auth\AuthActions;
use Codeception\Stub\Expected;
use SlimApiEssentials\App\Exceptions\AuthBasicException;
use SlimApiEssentials\App\Exceptions\AuthException;

// use SlimApiEssentials\App\Services\Auth\Apikey;
// use SlimApiEssentials\Tests\Data\App\Di\AuthUserResolverApikeyDummy;
// use SlimApiEssentials\App\Services\Auth\AuthActions;

class AuthActionsTest extends Unit
{
    /**
     * @var \Tests\UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    public function testGetAuthorizationTypeFromRequestNull()
    {
        $commonHelper = $this->make(
            CommonHelper::class
        );
        $request = $this->make(
            SlimRequest::class,
            [
                'getHeaderLine' => Expected::once(
                    'anytning'
                )
            ]
        );
        $service = new AuthActions(
            $commonHelper
        );

        $result = $service->getAuthorizationTypeFromRequest($request);

        $this->tester->assertSame(
            null,
            $result
        );
    }

    public function testGetAuthorizationTypeFromRequestOk()
    {
        $commonHelper = $this->make(
            CommonHelper::class
        );
        $request = $this->make(
            SlimRequest::class,
            [
                'getHeaderLine' => Expected::once(
                    'SomeAuthType andvalue'
                )
            ]
        );
        $service = new AuthActions(
            $commonHelper
        );

        $result = $service->getAuthorizationTypeFromRequest($request);

        $this->tester->assertSame(
            'SomeAuthType',
            $result
        );
    }

    public function testGetAuthorizationHeaderContentBadContent()
    {
        $commonHelper = $this->make(
            CommonHelper::class
        );
        $request = $this->make(
            SlimRequest::class,
            [
                'getHeaderLine' => Expected::once(
                    ''
                )
            ]
        );
        $service = new AuthActions(
            $commonHelper
        );

        $service->getAuthorizationHeaderContent($request);
    }

    public function testGetAuthorizationHeaderContentOk()
    {
        $commonHelper = $this->make(
            CommonHelper::class
        );
        $request = $this->make(
            SlimRequest::class,
            [
                'getHeaderLine' => Expected::once(
                    'authheadercontent'
                )
            ]
        );
        $service = new AuthActions(
            $commonHelper
        );

        $result = $service->getAuthorizationHeaderContent($request);

        $this->tester->assertSame(
            'authheadercontent',
            $result
        );
    }

    public function testGetBasicCredentialsFromRequestBadContent()
    {
        $commonHelper = $this->make(
            CommonHelper::class,
            [
                'base64Decode' => Expected::once(
                    function ($val) {
                        return base64_decode($val);
                    }
                )
            ]
        );
        $request = $this->make(
            SlimRequest::class,
            [
                'getHeaderLine' => Expected::once(
                    'Basic ' . base64_encode('bad-content')
                )
            ]
        );
        $service = new AuthActions(
            $commonHelper
        );

        $this->tester->expectThrowable(
            new AuthBasicException(
                'Invalid Auth header Basic format! Basic Auth format must be "base64Encode(id:pass)"'
            ),
            function () use ($service, $request) {
                $service->getBasicCredentialsFromRequest($request);
            }
        );
    }

    public function testGetBasicCredentialsFromRequestBadHeader()
    {
        $commonHelper = $this->make(
            CommonHelper::class,
            [
                'base64Decode' => Expected::never()
            ]
        );
        $request = $this->make(
            SlimRequest::class,
            [
                'getHeaderLine' => Expected::once(
                    'Basic bad header:-nobase64'
                )
            ]
        );
        $service = new AuthActions(
            $commonHelper
        );

        $this->tester->expectThrowable(
            new AuthBasicException(
                'Unknown Authorization header! "Basic bad header:-nobase64"'
            ),
            function () use ($service, $request) {
                $service->getBasicCredentialsFromRequest($request);
            }
        );
    }

    public function testGetBasicCredentialsFromRequestOk()
    {
        $commonHelper = $this->make(
            CommonHelper::class,
            [
                'base64Decode' => Expected::once(
                    function ($val) {
                        return base64_decode($val);
                    }
                )
            ]
        );
        $request = $this->make(
            SlimRequest::class,
            [
                'getHeaderLine' => Expected::once(
                    'Basic ' . base64_encode('id name:pa sspč:rase')
                )
            ]
        );
        $service = new AuthActions(
            $commonHelper
        );

        $result = $service->getBasicCredentialsFromRequest($request);

        $this->tester->assertSame(
            ['id name', 'pa sspč:rase'],
            $result
        );
    }
}
