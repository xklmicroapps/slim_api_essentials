<?php

declare(strict_types=1);

namespace SlimApiEssentials\Tests\App\Services\Auth;

use Codeception\Test\Unit;
use SlimApiEssentials\App\Services\Auth\Basic;
use Slim\Psr7\Request as SlimRequest;
use SlimApiEssentials\Tests\Data\App\Di\AuthUserResolverBasicDummy;
use SlimApiEssentials\App\Services\Auth\AuthActions;
use SlimApiEssentials\App\Exceptions\AuthBasicException;
use Codeception\Stub\Expected;
use Codeception\Exception\Warning;

class BasicTest extends Unit
{
    /**
     * @var \Tests\UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    public function testCheckBadCredentials()
    {
        // mock request
        $request = $this->make(SlimRequest::class);
        $userResolver = $this->make(
            AuthUserResolverBasicDummy::class,
            [
                'getUserByUsername' => Expected::once(
                    function ($username) {
                        return ['somnedat'];
                    }
                ),
                /**
                 * return false as an password check
                 */
                'isUserPassOk' => true
            ]
        );
        $authActions = $this->make(
            AuthActions::class,
            [
                'getBasicCredentialsFromRequest' => Expected::once(
                    ['bad credentials returned']
                )
            ]
        );

        $authService = new Basic(
            $userResolver,
            $authActions
        );

        // run tested method
        $this->tester->expectThrowable(
            Warning::class,
            function () use ($authService, $request) {
                $authService->check($request);
            }
        );
    }

    public function testCheckBadPassword()
    {
        // mock request
        $request = $this->make(SlimRequest::class);
        $userResolver = $this->make(
            AuthUserResolverBasicDummy::class,
            [
                'getUserByUsername' => Expected::once(
                    function ($username) {
                        $this->tester->assertSame('username', $username);
                        return ['somnedat'];
                    }
                ),
                /**
                 * return false as an password check
                 */
                'isUserPassOk' => false
            ]
        );
        $authActions = $this->make(
            AuthActions::class,
            [
                'getBasicCredentialsFromRequest' => Expected::once(
                    ['username', 'pass']
                )
            ]
        );

        $authService = new Basic(
            $userResolver,
            $authActions
        );

        // run tested method
        $this->tester->expectThrowable(
            new AuthBasicException('Incorrect user or password!'),
            function () use ($authService, $request) {
                $authService->check($request);
            }
        );
    }

    public function testCheckBadUsername()
    {
        // mock request
        $request = $this->make(SlimRequest::class);
        $userResolver = $this->make(
            AuthUserResolverBasicDummy::class,
            [
                'getUserByUsername' => Expected::once(
                    function ($username) {
                        $this->tester->assertSame('username', $username);
                        /**
                         * return null as an user
                         */
                        return null;
                    }
                ),
                'isUserPassOk' => true
            ]
        );
        $authActions = $this->make(
            AuthActions::class,
            [
                'getBasicCredentialsFromRequest' => Expected::once(
                    ['username', 'pass']
                )
            ]
        );

        $authService = new Basic(
            $userResolver,
            $authActions
        );

        // run tested method
        $this->tester->expectThrowable(
            new AuthBasicException('Incorrect user or password!'),
            function () use ($authService, $request) {
                $authService->check($request);
            }
        );
    }

    public function testCheckOk()
    {
        // mock request
        $request = $this->make(SlimRequest::class);
        $userResolver = $this->make(
            AuthUserResolverBasicDummy::class,
            [
                'getUserByUsername' => Expected::once(
                    function ($username) {
                        $this->tester->assertSame('username', $username);
                        return 'userdata';
                    }
                ),
                'isUserPassOk' => true
            ]
        );
        $authActions = $this->make(
            AuthActions::class,
            [
                'getBasicCredentialsFromRequest' => Expected::once(
                    ['username', 'pass']
                )
            ]
        );

        $authService = new Basic(
            $userResolver,
            $authActions
        );

        // run tested method
        $authService->check($request);
    }
}
