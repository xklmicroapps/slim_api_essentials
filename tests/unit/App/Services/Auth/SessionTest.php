<?php

declare(strict_types=1);

namespace SlimApiEssentials\Tests\App\Services\Auth;

use LogicException;
use Codeception\Test\Unit;
use SlimApiEssentials\App\Services\Auth\Session;
use SlimApiEssentials\App\Services\Auth\SessionActions;
use Slim\Psr7\Request as SlimRequest;
use SlimApiEssentials\Tests\Data\App\Di\AuthUserResolverSessionDummy;
use SlimApiEssentials\App\Exceptions\AuthSessionException;
use Codeception\Stub\Expected;
use Codeception\Exception\Warning;
use SlimApiEssentials\App\Services\RequestHelper;
use SlimApiEssentials\App\Services\ResponseHelper;
use Slim\Routing\RouteContext;
use Slim\Interfaces\RouteInterface;
use Slim\Interfaces\RouteParserInterface;
use Slim\Routing\RoutingResults;
use Slim\Routing\RouteParser;
use Slim\Psr7\Factory\ResponseFactory;
use Slim\Psr7\Response as SlimResponse;
use SlimApiEssentials\App\Services\Auth\SessionSerialize;
use SlimApiEssentials\App\Di\Container;
use SlimApiEssentials\App\Di\ContainerActions;
use Psr\Http\Server\RequestHandlerInterface;
use Psr\Http\Message\UriInterface;
use Slim\Psr7\Uri;
use Slim\Psr7\Headers;
use Slim\Psr7\Stream;
use SlimApiEssentials\App\Di\Services;

class SessionTest extends Unit
{
    /**
     * @var \Tests\UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    public function testIsRouteNameGetFalse()
    {
        // mock request
        $request = $this->make(
            SlimRequest::class,
            [
                'getAttribute' => Expected::exactly(
                    4,
                    function ($val) {
                        if ($val === RouteContext::ROUTE) {
                            return $this->makeEmpty(
                                RouteInterface::class,
                                [
                                    'getName' => Expected::once(
                                        'some another route name'
                                    )
                                ]
                            );
                        }
                        if ($val === RouteContext::ROUTE_PARSER) {
                            return $this->makeEmpty(RouteParserInterface::class);
                        }
                        if ($val === RouteContext::ROUTING_RESULTS) {
                            return $this->makeEmpty(RoutingResults::class);
                        }

                        return 'somevalue';
                    }
                )
            ]
        );
        $userResolver = $this->make(
            AuthUserResolverSessionDummy::class
        );
        $routeParser = $this->make(
            RouteParser::class
        );
        $responseFactory = $this->make(
            ResponseFactory::class
        );
        $sessionActions = $this->make(
            sessionActions::class
        );
        $responseHelper = $this->make(
            ResponseHelper::class
        );

        $authService = new Session(
            [
                'keyUsername' => null,
                'keyPassword' => null,
                'routeNameGet' => 'routeNameGet-dummy',
                'routeNameSet' => null,
                'routeNameLoggedFirst' => null,
                'routeArgsPersistent' => ['lang'],
                'expiredRedirectPost' => []
            ],
            $sessionActions,
            $userResolver,
            $routeParser,
            $responseFactory,
            $responseHelper,
            new SessionSerialize(
                $this->make(Container::class)
            )
        );

        $result = $authService->isRouteNameGet(
            $request
        );

        $this->assertFalse($result);
    }

    public function testIsRouteNameGetTrue()
    {
        // mock request
        $request = $this->make(
            SlimRequest::class,
            [
                'getAttribute' => Expected::exactly(
                    4,
                    function ($val) {
                        if ($val === RouteContext::ROUTE) {
                            return $this->makeEmpty(
                                RouteInterface::class,
                                [
                                    'getName' => Expected::once(
                                        'routeNameGet-dummy'
                                    )
                                ]
                            );
                        }
                        if ($val === RouteContext::ROUTE_PARSER) {
                            return $this->makeEmpty(RouteParserInterface::class);
                        }
                        if ($val === RouteContext::ROUTING_RESULTS) {
                            return $this->makeEmpty(RoutingResults::class);
                        }

                        return 'somevalue';
                    }
                )
            ]
        );
        $userResolver = $this->make(
            AuthUserResolverSessionDummy::class
        );
        $routeParser = $this->make(
            RouteParser::class
        );
        $responseFactory = $this->make(
            ResponseFactory::class
        );
        $sessionActions = $this->make(
            sessionActions::class
        );
        $responseHelper = $this->make(
            ResponseHelper::class
        );

        $authService = new Session(
            [
                'keyUsername' => null,
                'keyPassword' => null,
                'routeNameGet' => 'routeNameGet-dummy',
                'routeNameSet' => null,
                'routeNameLoggedFirst' => null,
                'routeArgsPersistent' => ['lang'],
                'expiredRedirectPost' => []
            ],
            $sessionActions,
            $userResolver,
            $routeParser,
            $responseFactory,
            $responseHelper,
            new SessionSerialize(
                $this->make(Container::class)
            )
        );

        $result = $authService->isRouteNameGet(
            $request
        );

        $this->assertTrue($result);
    }

    public function testIsRouteNameSetFalse()
    {
        // mock request
        $request = $this->make(
            SlimRequest::class,
            [
                'getAttribute' => Expected::exactly(
                    4,
                    function ($val) {
                        if ($val === RouteContext::ROUTE) {
                            return $this->makeEmpty(
                                RouteInterface::class,
                                [
                                    'getName' => Expected::once(
                                        'any other route'
                                    )
                                ]
                            );
                        }
                        if ($val === RouteContext::ROUTE_PARSER) {
                            return $this->makeEmpty(RouteParserInterface::class);
                        }
                        if ($val === RouteContext::ROUTING_RESULTS) {
                            return $this->makeEmpty(RoutingResults::class);
                        }

                        return 'somevalue';
                    }
                )
            ]
        );
        $userResolver = $this->make(
            AuthUserResolverSessionDummy::class
        );
        $routeParser = $this->make(
            RouteParser::class
        );
        $responseFactory = $this->make(
            ResponseFactory::class
        );
        $sessionActions = $this->make(
            sessionActions::class
        );
        $responseHelper = $this->make(
            ResponseHelper::class
        );

        $authService = new Session(
            [
                'keyUsername' => null,
                'keyPassword' => null,
                'routeNameGet' => null,
                'routeNameSet' => 'routeNameSet-dummy',
                'routeNameLoggedFirst' => null,
                'routeArgsPersistent' => ['lang'],
                'expiredRedirectPost' => []
            ],
            $sessionActions,
            $userResolver,
            $routeParser,
            $responseFactory,
            $responseHelper,
            new SessionSerialize(
                $this->make(Container::class)
            )
        );

        $result = $authService->isRouteNameSet(
            $request
        );

        $this->assertFalse($result);
    }

    public function testIsRouteNameSetTrue()
    {
        // mock request
        $request = $this->make(
            SlimRequest::class,
            [
                'getAttribute' => Expected::exactly(
                    4,
                    function ($val) {
                        if ($val === RouteContext::ROUTE) {
                            return $this->makeEmpty(
                                RouteInterface::class,
                                [
                                    'getName' => Expected::once(
                                        'routeNameSet-dummy'
                                    )
                                ]
                            );
                        }
                        if ($val === RouteContext::ROUTE_PARSER) {
                            return $this->makeEmpty(RouteParserInterface::class);
                        }
                        if ($val === RouteContext::ROUTING_RESULTS) {
                            return $this->makeEmpty(RoutingResults::class);
                        }

                        return 'somevalue';
                    }
                )
            ]
        );
        $userResolver = $this->make(
            AuthUserResolverSessionDummy::class
        );
        $routeParser = $this->make(
            RouteParser::class
        );
        $responseFactory = $this->make(
            ResponseFactory::class
        );
        $sessionActions = $this->make(
            sessionActions::class
        );
        $responseHelper = $this->make(
            ResponseHelper::class
        );

        $authService = new Session(
            [
                'keyUsername' => null,
                'keyPassword' => null,
                'routeNameGet' => null,
                'routeNameSet' => 'routeNameSet-dummy',
                'routeNameLoggedFirst' => null,
                'routeArgsPersistent' => ['lang'],
                'expiredRedirectPost' => []
            ],
            $sessionActions,
            $userResolver,
            $routeParser,
            $responseFactory,
            $responseHelper,
            new SessionSerialize(
                $this->make(Container::class)
            )
        );

        $result = $authService->isRouteNameSet(
            $request
        );

        $this->assertTrue($result);
    }

    public function testGetLoginActionUri()
    {
        // mock request
        $request = $this->make(
            SlimRequest::class,
            [
                'getQueryParams' => Expected::once(
                    [
                        'queryparam1' => 'queryparam1value',
                        'queryparam2' => 'queryparam2value',
                        Session::PARAM_NAME_REQUESTSAVEDFILEPATH => 'somevalue'
                    ]
                ),
                'getUri' => Expected::once(
                    $this->makeEmpty(UriInterface::class)
                ),
                'getAttribute' => Expected::exactly(
                    4,
                    function ($val) {
                        if ($val === RouteContext::ROUTE) {
                            return $this->makeEmpty(
                                RouteInterface::class
                            );
                        }
                        if ($val === RouteContext::ROUTE_PARSER) {
                            return $this->makeEmpty(RouteParserInterface::class);
                        }
                        if ($val === RouteContext::ROUTING_RESULTS) {
                            return $this->makeEmpty(RoutingResults::class);
                        }
                        return 'somevalue';
                    }
                )
            ]
        );
        $userResolver = $this->make(
            AuthUserResolverSessionDummy::class
        );
        $routeParser = $this->make(
            RouteParser::class,
            [
                'fullUrlFor' => Expected::once(
                    function ($uri, $routeName, $data, $queryParams) {
                        $this->assertSame('routeNameSet-dummy', $routeName);
                        $this->assertSame(['lang' => null], $data);
                        $this->assertSame(
                            [
                                Session::PARAM_NAME_REQUESTSAVEDFILEPATH => 'somevalue'
                            ],
                            $queryParams
                        );

                        return 'someurl';
                    }
                )
            ]
        );
        $responseFactory = $this->make(
            ResponseFactory::class
        );
        $sessionActions = $this->make(
            sessionActions::class
        );
        $responseHelper = $this->make(
            ResponseHelper::class
        );

        $authService = new Session(
            [
                'keyUsername' => null,
                'keyPassword' => null,
                'routeNameGet' => null,
                'routeNameSet' => 'routeNameSet-dummy',
                'routeNameLoggedFirst' => null,
                'routeArgsPersistent' => ['lang'],
                'expiredRedirectPost' => []
            ],
            $sessionActions,
            $userResolver,
            $routeParser,
            $responseFactory,
            $responseHelper,
            new SessionSerialize(
                $this->make(Container::class)
            )
        );

        $result = $authService->getLoginActionUri(
            $request
        );

        $this->assertSame('someurl', $result);
    }

    public function testGetResponseRedirectLoggedGet()
    {
        $dbgfilepath = sys_get_temp_dir() . '/xkl.test.debug';
        file_put_contents($dbgfilepath, '1');

        // mock request
        $request = $this->make(
            SlimRequest::class,
            [
                'getQueryParams' => Expected::once(
                    [
                        'queryparam1' => 'queryparam1value',
                        'queryparam2' => 'queryparam2value',
                        Session::PARAM_NAME_REQUESTSAVEDFILEPATH => 'somevalue'
                    ]
                )
            ]
        );
        $userResolver = $this->make(
            AuthUserResolverSessionDummy::class
        );
        $routeParser = $this->make(
            RouteParser::class
        );
        $responseFactory = $this->make(
            ResponseFactory::class
        );
        $sessionActions = $this->makeEmpty(
            sessionActions::class,
            [
                'getResponseRedirectLoggedSaved' => Expected::once(
                    function (
                        $expiredRedirectPost,
                        $request,
                        $requestSaved
                    ) {
                        $this->assertInstanceOf(SlimRequest::class, $request);
                        $this->assertInstanceOf(SlimRequest::class, $requestSaved);

                        $this->assertSame([], $expiredRedirectPost);
                        $this->assertSame('get', $requestSaved->getMethod());

                        return new SlimResponse();
                    }
                )
            ]
        );
        $responseHelper = $this->make(
            ResponseHelper::class
        );

        $authService = new Session(
            [
                'keyUsername' => null,
                'keyPassword' => null,
                'routeNameGet' => null,
                'routeNameSet' => null,
                'routeNameLoggedFirst' => 'routeNameLoggedFirst-dummy',
                'routeArgsPersistent' => ['lang'],
                'expiredRedirectPost' => []
            ],
            $sessionActions,
            $userResolver,
            $routeParser,
            $responseFactory,
            $responseHelper,
            $this->construct(
                SessionSerialize::class,
                [
                    'container' => $this->make(Container::class)
                ],
                [
                    'getDecoded' => Expected::once($dbgfilepath),
                    'getUnserializedRequest' => Expected::once(
                        $this->make(
                            SlimRequest::class,
                            [
                                'getMethod' => Expected::once(
                                    'get'
                                )
                            ]
                        )
                    )
                ]
            )
        );

        $result = $authService->getResponseRedirectLogged(
            $request,
            $this->makeEmpty(
                RequestHandlerInterface::class
            )
        );

        $this->assertInstanceOf(SlimResponse::class, $result);
    }

    public function testGetResponseRedirectLoggedPost()
    {
        $dbgfilepath = sys_get_temp_dir() . '/xkl.test.debug';
        file_put_contents($dbgfilepath, '1');

        // mock request
        $request = $this->make(
            SlimRequest::class,
            [
                'getQueryParams' => Expected::once(
                    [
                        'queryparam1' => 'queryparam1value',
                        'queryparam2' => 'queryparam2value',
                        Session::PARAM_NAME_REQUESTSAVEDFILEPATH => 'somevalue'
                    ]
                )
            ]
        );
        $userResolver = $this->make(
            AuthUserResolverSessionDummy::class
        );
        $routeParser = $this->make(
            RouteParser::class
        );
        $responseFactory = $this->make(
            ResponseFactory::class
        );
        $sessionActions = $this->makeEmpty(
            sessionActions::class,
            [
                'getResponseRedirectLoggedSaved' => Expected::once(
                    function (
                        $expiredRedirectPost,
                        $request,
                        $requestSaved
                    ) {
                        $this->assertInstanceOf(SlimRequest::class, $request);
                        $this->assertInstanceOf(SlimRequest::class, $requestSaved);

                        $this->assertSame(['#some#' => 'val'], $expiredRedirectPost);
                        $this->assertSame('post', $requestSaved->getMethod());

                        return new SlimResponse();
                    }
                )
            ]
        );
        $responseHelper = $this->make(
            ResponseHelper::class,
            [
                'getWithLocation' => Expected::never()
            ]
        );

        $authService = new Session(
            [
                'keyUsername' => null,
                'keyPassword' => null,
                'routeNameGet' => null,
                'routeNameSet' => null,
                'routeNameLoggedFirst' => 'routeNameLoggedFirst-dummy',
                'routeArgsPersistent' => ['lang'],
                'expiredRedirectPost' => [
                    '#some#' => 'val'
                ]
            ],
            $sessionActions,
            $userResolver,
            $routeParser,
            $responseFactory,
            $responseHelper,
            $this->construct(
                SessionSerialize::class,
                [
                    'container' => $this->make(Container::class)
                ],
                [
                    'getDecoded' => Expected::once($dbgfilepath),
                    'getUnserializedRequest' => Expected::once(
                        $this->make(
                            SlimRequest::class,
                            [
                                'getMethod' => Expected::once(
                                    'post'
                                )
                            ]
                        )
                    )
                ]
            )
        );

        $result = $authService->getResponseRedirectLogged(
            $request,
            $this->makeEmpty(
                RequestHandlerInterface::class
            )
        );

        $this->assertInstanceOf(SlimResponse::class, $result);
    }

    public function testGetResponseRedirectLoggedRequestSavedEmpty()
    {
        // mock request
        $request = $this->make(
            SlimRequest::class,
            [
                'getQueryParams' => Expected::once(
                    [
                        'queryparam1' => 'queryparam1value',
                        'queryparam2' => 'queryparam2value',
                        Session::PARAM_NAME_REQUESTSAVEDFILEPATH => 'somevalue'
                    ]
                ),
                'getAttribute' => Expected::exactly(
                    4,
                    function ($val) {
                        if ($val === RouteContext::ROUTE) {
                            return $this->makeEmpty(
                                RouteInterface::class
                            );
                        }
                        if ($val === RouteContext::ROUTE_PARSER) {
                            return $this->makeEmpty(RouteParserInterface::class);
                        }
                        if ($val === RouteContext::ROUTING_RESULTS) {
                            return $this->makeEmpty(RoutingResults::class);
                        }
                        return 'somevalue';
                    }
                )
            ]
        );
        $userResolver = $this->make(
            AuthUserResolverSessionDummy::class
        );
        $routeParser = $this->make(
            RouteParser::class
        );
        $responseFactory = $this->make(
            ResponseFactory::class
        );
        $sessionActions = $this->make(
            sessionActions::class
        );
        $responseHelper = $this->make(
            ResponseHelper::class,
            [
                'getWithLocation' => Expected::once(
                    function ($request, $response, $routename, $data, $queryParams) {
                        $this->assertInstanceOf(SlimRequest::class, $request);
                        $this->assertInstanceOf(SlimResponse::class, $response);
                        $this->assertSame('routeNameLoggedFirst-dummy', $routename);
                        $this->assertSame(['lang' => null], $data);
                        $this->assertSame(
                            [],
                            $queryParams
                        );

                        return new SlimResponse();
                    }
                )
            ]
        );

        $authService = new Session(
            [
                'keyUsername' => null,
                'keyPassword' => null,
                'routeNameGet' => null,
                'routeNameSet' => null,
                'routeNameLoggedFirst' => 'routeNameLoggedFirst-dummy',
                'routeArgsPersistent' => ['lang'],
                'expiredRedirectPost' => []
            ],
            $sessionActions,
            $userResolver,
            $routeParser,
            $responseFactory,
            $responseHelper,
            $this->construct(
                SessionSerialize::class,
                [
                    'container' => $this->make(Container::class)
                ],
                [
                    'getDecoded' => Expected::once('')
                ]
            )
        );

        $authService->getResponseRedirectLogged(
            $request,
            $this->makeEmpty(
                RequestHandlerInterface::class
            )
        );
    }

    public function testGetResponseRedirectLoggedRedirEmpty()
    {
        // mock request
        $request = $this->make(
            SlimRequest::class,
            [
                'getQueryParams' => Expected::once(
                    [
                        'queryparam1' => 'queryparam1value',
                        'queryparam2' => 'queryparam2value',
                        Session::PARAM_NAME_REQUESTSAVEDFILEPATH => ''
                    ]
                ),
                'getAttribute' => Expected::exactly(
                    4,
                    function ($val) {
                        if ($val === RouteContext::ROUTE) {
                            return $this->makeEmpty(
                                RouteInterface::class
                            );
                        }
                        if ($val === RouteContext::ROUTE_PARSER) {
                            return $this->makeEmpty(RouteParserInterface::class);
                        }
                        if ($val === RouteContext::ROUTING_RESULTS) {
                            return $this->makeEmpty(RoutingResults::class);
                        }
                        return 'somevalue';
                    }
                )
            ]
        );
        $userResolver = $this->make(
            AuthUserResolverSessionDummy::class
        );
        $routeParser = $this->make(
            RouteParser::class
        );
        $responseFactory = $this->make(
            ResponseFactory::class
        );
        $sessionActions = $this->make(
            sessionActions::class
        );
        $responseHelper = $this->make(
            ResponseHelper::class,
            [
                'getWithLocation' => Expected::once(
                    function ($request, $response, $routename, $data, $queryParams) {
                        $this->assertInstanceOf(SlimRequest::class, $request);
                        $this->assertInstanceOf(SlimResponse::class, $response);
                        $this->assertSame('routeNameLoggedFirst-dummy', $routename);
                        $this->assertSame(['lang' => null], $data);
                        $this->assertSame(
                            [],
                            $queryParams
                        );

                        return new SlimResponse();
                    }
                )
            ]
        );

        $authService = new Session(
            [
                'keyUsername' => null,
                'keyPassword' => null,
                'routeNameGet' => null,
                'routeNameSet' => null,
                'routeNameLoggedFirst' => 'routeNameLoggedFirst-dummy',
                'routeArgsPersistent' => ['lang'],
                'expiredRedirectPost' => []
            ],
            $sessionActions,
            $userResolver,
            $routeParser,
            $responseFactory,
            $responseHelper,
            new SessionSerialize(
                $this->make(Container::class)
            )
        );

        $authService->getResponseRedirectLogged(
            $request,
            $this->makeEmpty(
                RequestHandlerInterface::class
            )
        );
    }

    public function testGetResponseRedirectLoggedException()
    {
        // mock request
        $request = $this->make(
            SlimRequest::class,
            [
                'getQueryParams' => Expected::once(
                    [
                        'queryparam1' => 'queryparam1value',
                        'queryparam2' => 'queryparam2value'
                    ]
                )
            ]
        );
        $userResolver = $this->make(
            AuthUserResolverSessionDummy::class
        );
        $routeParser = $this->make(
            RouteParser::class
        );
        $responseFactory = $this->make(
            ResponseFactory::class
        );
        $sessionActions = $this->make(
            sessionActions::class
        );
        $responseHelper = $this->make(
            ResponseHelper::class
        );

        $authService = new Session(
            [
                'keyUsername' => null,
                'keyPassword' => null,
                'routeNameGet' => null,
                'routeNameSet' => null,
                'routeNameLoggedFirst' => null,
                'routeArgsPersistent' => ['lang'],
                'expiredRedirectPost' => []
            ],
            $sessionActions,
            $userResolver,
            $routeParser,
            $responseFactory,
            $responseHelper,
            new SessionSerialize(
                $this->make(Container::class)
            )
        );

        $this->tester->expectThrowable(
            new LogicException(
                "Missing required query parameter"
                    . " \"" . Session::PARAM_NAME_REQUESTSAVEDFILEPATH . "\"!"
                    . " Login action should be created with "
                    . Session::class . "::getLoginActionUri()"
            ),
            function () use ($authService, $request) {
                $authService->getResponseRedirectLogged(
                    $request,
                    $this->makeEmpty(
                        RequestHandlerInterface::class
                    )
                );
            }
        );
    }

    public function testGetResponseRedirectNotLoggedRouteAny()
    {
        // mock request
        $request = $this->make(
            SlimRequest::class,
            [
                'getQueryParams' => Expected::once(
                    [
                        'queryparam1' => 'queryparam1value',
                        'queryparam2' => 'queryparam2value'
                    ]
                ),
                'getAttribute' => Expected::exactly(
                    8,
                    function ($val) {
                        if ($val === RouteContext::ROUTE) {
                            return $this->makeEmpty(
                                RouteInterface::class
                            );
                        }
                        if ($val === RouteContext::ROUTE_PARSER) {
                            return $this->makeEmpty(RouteParserInterface::class);
                        }
                        if ($val === RouteContext::ROUTING_RESULTS) {
                            return $this->makeEmpty(RoutingResults::class);
                        }
                        return 'somevalue';
                    }
                )
            ]
        );
        $userResolver = $this->make(
            AuthUserResolverSessionDummy::class
        );
        $routeParser = $this->make(
            RouteParser::class
        );
        $responseFactory = $this->make(
            ResponseFactory::class
        );
        $sessionActions = $this->make(
            sessionActions::class,
            [
                'getRequestParamsToUse' => []
            ]
        );
        $responseHelper = $this->make(
            ResponseHelper::class,
            [
                'getWithLocation' => Expected::once(
                    function ($request, $response, $routename, $data, $queryParams) {
                        $this->assertInstanceOf(SlimRequest::class, $request);
                        $this->assertInstanceOf(SlimResponse::class, $response);
                        $this->assertSame('routeNameGet-dummy', $routename);
                        $this->assertSame(['lang' => null], $data);
                        $this->assertSame(
                            [
                                'queryparam1' => 'queryparam1value',
                                'queryparam2' => 'queryparam2value',
                                Session::PARAM_NAME_REQUESTSAVEDFILEPATH => 'encodedfilepath'
                            ],
                            $queryParams
                        );

                        return new SlimResponse();
                    }
                )
            ]
        );

        $authService = new Session(
            [
                'keyUsername' => null,
                'keyPassword' => null,
                'routeNameGet' => 'routeNameGet-dummy',
                'routeNameSet' => 'routeNameSet-dummy',
                'routeNameLoggedFirst' => null,
                'routeArgsPersistent' => ['lang'],
                'expiredRedirectPost' => []
            ],
            $sessionActions,
            $userResolver,
            $routeParser,
            $responseFactory,
            $responseHelper,
            $this->construct(
                SessionSerialize::class,
                [
                    'container' => $this->make(Container::class)
                ],
                [
                    'getSerializedRequest' => Expected::once(''),
                    'getEncoded' => Expected::once('encodedfilepath')
                ]
            )
        );

        // run tested method
        $authService->getResponseRedirectNotLogged($request);
    }

    public function testGetResponseRedirectNotLoggedRouteSet()
    {
        // mock request
        $request = $this->construct(
            SlimRequest::class,
            [
                'method',
                new Uri(
                    'https://',
                    'host',
                    null,
                    '/',
                    'queryparam1=queryparam1value&queryparam2=queryparam2value'
                ),
                new Headers([], []),
                [],
                [],
                new Stream(
                    fopen(
                        sys_get_temp_dir()
                            . '/xkl.test.debug.stream.'
                            . sha1_file(__FILE__),
                        'w'
                    )
                )
            ]
        )->withAttribute(
            RouteContext::ROUTE,
            $this->makeEmpty(RouteInterface::class)
        )->withAttribute(
            RouteContext::ROUTE_PARSER,
            $this->makeEmpty(RouteParserInterface::class)
        )->withAttribute(
            RouteContext::ROUTING_RESULTS,
            $this->makeEmpty(RoutingResults::class)
        );

        $userResolver = $this->make(
            AuthUserResolverSessionDummy::class
        );
        $routeParser = $this->make(
            RouteParser::class
        );
        $responseFactory = $this->make(
            ResponseFactory::class
        );
        $sessionActions = $this->make(
            sessionActions::class,
            [
                'getRequestParamsToUse' => []
            ]
        );
        $responseHelper = $this->make(
            ResponseHelper::class,
            [
                'getWithLocation' => Expected::once(
                    function ($request, $response, $routename, $data, $queryParams) {
                        $this->assertInstanceOf(SlimRequest::class, $request);
                        $this->assertInstanceOf(SlimResponse::class, $response);
                        $this->assertSame('routeNameGet-dummy', $routename);
                        $this->assertSame(['lang' => null], $data);
                        $this->assertSame(
                            [
                                'queryparam1',
                                'queryparam2',
                                Session::PARAM_NAME_REQUESTSAVEDFILEPATH
                            ],
                            array_keys($queryParams)
                        );
                        $this->assertSame(
                            'queryparam1value',
                            $queryParams['queryparam1']
                        );
                        $this->assertSame(
                            'queryparam2value',
                            $queryParams['queryparam2']
                        );

                        $this->assertStringStartsWith(
                            sys_get_temp_dir() . '/' . Session::FILEPREFIX_REQUESTSAVED,
                            base64_decode($queryParams[Session::PARAM_NAME_REQUESTSAVEDFILEPATH])
                        );

                        return new SlimResponse();
                    }
                )
            ]
        );

        $container = $this->construct(
            Container::class,
            [
                'actions' => $this->make(
                    ContainerActions::class
                )
            ]
        );
        $container->setSubContainers(
            [
                new Services()
            ]
        );
        $container->setConfigParameters(
            [
                'security' => [
                    'secret' => 'securitySecret',
                    'iv' => 'securityIv'
                ]
            ]
        );

        $authService = new Session(
            [
                'keyUsername' => null,
                'keyPassword' => null,
                'routeNameGet' => 'routeNameGet-dummy',
                'routeNameSet' => 'routeNameSet-dummy',
                'routeNameLoggedFirst' => null,
                'routeArgsPersistent' => ['lang'],
                'expiredRedirectPost' => []
            ],
            $sessionActions,
            $userResolver,
            $routeParser,
            $responseFactory,
            $responseHelper,
            new SessionSerialize(
                $container
            )
        );

        // run tested method
        $authService->getResponseRedirectNotLogged($request);
    }

    public function testCheckErrorSession()
    {
        // mock request
        $request = $this->make(
            SlimRequest::class,
            [
                'getAttribute' => Expected::exactly(
                    4,
                    function ($val) {
                        if ($val === RouteContext::ROUTE) {
                            return $this->makeEmpty(
                                RouteInterface::class,
                                [
                                    'getName' => 'some dummy routename'
                                ]
                            );
                        }
                        if ($val === RouteContext::ROUTE_PARSER) {
                            return $this->makeEmpty(RouteParserInterface::class);
                        }
                        if ($val === RouteContext::ROUTING_RESULTS) {
                            return $this->makeEmpty(RoutingResults::class);
                        }
                        return 'somevalue';
                    }
                )
            ]
        );
        $userResolver = $this->make(
            AuthUserResolverSessionDummy::class,
            [
                'getUserSession' => Expected::once(null),
                'getUserByUsername' => Expected::never(),
                'isUserPassOk' => Expected::never(),
                'setUserSession' => Expected::never()
            ]
        );
        $routeParser = $this->make(
            RouteParser::class
        );
        $responseFactory = $this->make(
            ResponseFactory::class
        );
        $sessionActions = $this->make(
            sessionActions::class,
            [
                'getRequestParamsToUse' => []
            ]
        );
        $responseHelper = $this->make(
            ResponseHelper::class
        );

        $authService = new Session(
            [
                'keyUsername' => 'username',
                'keyPassword' => 'password',
                'routeNameGet' => null,
                'routeNameSet' => 'post/login/routename/set',
                'routeNameLoggedFirst' => null,
                'routeArgsPersistent' => ['lang'],
                'expiredRedirectPost' => []
            ],
            $sessionActions,
            $userResolver,
            $routeParser,
            $responseFactory,
            $responseHelper,
            new SessionSerialize(
                $this->make(Container::class)
            )
        );

        // run tested method
        $this->tester->expectThrowable(
            new AuthSessionException(
                'User session expired!'
            ),
            function () use ($authService, $request) {
                $authService->check($request);
            }
        );
    }

    public function testLoginErrorPost()
    {
        // mock request
        $request = $this->make(
            SlimRequest::class,
            [
                'getAttribute' => Expected::exactly(
                    4,
                    function ($val) {
                        if ($val === RouteContext::ROUTE) {
                            return $this->makeEmpty(
                                RouteInterface::class,
                                [
                                    'getName' => 'post/login/routename/set'
                                ]
                            );
                        }
                        if ($val === RouteContext::ROUTE_PARSER) {
                            return $this->makeEmpty(RouteParserInterface::class);
                        }
                        if ($val === RouteContext::ROUTING_RESULTS) {
                            return $this->makeEmpty(RoutingResults::class);
                        }
                        return 'somevalue';
                    }
                )
            ]
        );
        $userResolver = $this->make(
            AuthUserResolverSessionDummy::class,
            [
                'getUserByUsername' => Expected::once(
                    function ($username) {
                        $this->tester->assertSame('usernamevalue', $username);
                        return 'userdata';
                    }
                ),
                'isUserPassOk' => Expected::once(
                    function ($user, $pass) {
                        $this->tester->assertSame('passvalue', $pass);
                        return false;
                    }
                ),
                'setUserSession' => Expected::never()
            ]
        );
        $routeParser = $this->make(
            RouteParser::class
        );
        $responseFactory = $this->make(
            ResponseFactory::class
        );
        $sessionActions = $this->make(
            sessionActions::class,
            [
                'getRequestParamsToUse' => [
                    'username' => 'usernamevalue',
                    'password' => 'passvalue'
                ]
            ]
        );
        $responseHelper = $this->make(
            ResponseHelper::class
        );

        $authService = new Session(
            [
                'keyUsername' => 'username',
                'keyPassword' => 'password',
                'routeNameGet' => null,
                'routeNameSet' => 'post/login/routename/set',
                'routeNameLoggedFirst' => null,
                'routeArgsPersistent' => ['lang'],
                'expiredRedirectPost' => []
            ],
            $sessionActions,
            $userResolver,
            $routeParser,
            $responseFactory,
            $responseHelper,
            new SessionSerialize(
                $this->make(Container::class)
            )
        );

        // run tested method
        $this->tester->expectThrowable(
            new AuthSessionException(
                'Incorrect user or password!'
            ),
            function () use ($authService, $request) {
                $authService->login($request);
            }
        );
    }

    public function testCheckOkSession()
    {
        // mock request
        $request = $this->make(
            SlimRequest::class
        );
        $userResolver = $this->make(
            AuthUserResolverSessionDummy::class,
            [
                'getUserSession' => Expected::once('anything')
            ]
        );
        $routeParser = $this->make(
            RouteParser::class
        );
        $responseFactory = $this->make(
            ResponseFactory::class
        );
        $sessionActions = $this->make(
            sessionActions::class,
            [
                'getRequestParamsToUse' => []
            ]
        );
        $responseHelper = $this->make(
            ResponseHelper::class
        );

        $authService = new Session(
            [
                'keyUsername' => 'username',
                'keyPassword' => 'password',
                'routeNameGet' => null,
                'routeNameSet' => 'post/login/routename/set',
                'routeNameLoggedFirst' => null,
                'routeArgsPersistent' => ['lang'],
                'expiredRedirectPost' => []
            ],
            $sessionActions,
            $userResolver,
            $routeParser,
            $responseFactory,
            $responseHelper,
            new SessionSerialize(
                $this->make(Container::class)
            )
        );

        // run tested method
        $authService->check($request);
    }

    public function testCheckOkAny()
    {
        // mock request
        $request = $this->make(
            SlimRequest::class
        );
        $userResolver = $this->make(
            AuthUserResolverSessionDummy::class,
            [
                'getUserSession' => Expected::once(
                    function ($request) {
                        $this->tester->assertInstanceOf(
                            SlimRequest::class,
                            $request
                        );

                        return [
                            'anyuser'
                        ];
                    }
                )
            ]
        );
        $routeParser = $this->make(
            RouteParser::class
        );
        $responseFactory = $this->make(
            ResponseFactory::class
        );
        $sessionActions = $this->make(
            sessionActions::class
        );
        $responseHelper = $this->make(
            ResponseHelper::class
        );

        $authService = new Session(
            [
                'keyUsername' => 'username',
                'keyPassword' => 'password',
                'routeNameGet' => null,
                'routeNameSet' => 'post/login/routename/set',
                'routeNameLoggedFirst' => null,
                'routeArgsPersistent' => ['lang'],
                'expiredRedirectPost' => []
            ],
            $sessionActions,
            $userResolver,
            $routeParser,
            $responseFactory,
            $responseHelper,
            new SessionSerialize(
                $this->make(Container::class)
            )
        );

        // run tested method
        $authService->check($request);
    }
}
