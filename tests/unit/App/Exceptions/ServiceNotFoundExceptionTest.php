<?php

declare(strict_types=1);

namespace SlimApiEssentials\Tests\App\Exceptions;

use Codeception\Test\Unit;
use SlimApiEssentials\App\Exceptions\ServiceNotFoundException;
use Psr\Container\NotFoundExceptionInterface;
use Exception;

class ServiceNotFoundExceptionTest extends Unit
{
    /**
     * @var \Tests\UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    public function testException()
    {
        $exception = new ServiceNotFoundException();

        // test instance type
        $this->tester->assertInstanceOf(
            NotFoundExceptionInterface::class,
            $exception
        );
        $this->tester->assertInstanceOf(
            Exception::class,
            $exception
        );
    }
}
