<?php

declare(strict_types=1);

namespace SlimApiEssentials\Tests\App\Exceptions;

use Codeception\Test\Unit;
use SlimApiEssentials\App\Exceptions\AuthOauth2Exception;
use SlimApiEssentials\App\Exceptions\AuthException;
use RuntimeException;
use Psr\Http\Message\ResponseInterface;
use Slim\Psr7\Factory\ResponseFactory;

class AuthOauth2ExceptionTest extends Unit
{
    /**
     * @var \Tests\UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    public function testException()
    {
        $exception = new AuthOauth2Exception();

        // test instance type
        $this->tester->assertInstanceOf(
            AuthException::class,
            $exception
        );
        // test custom error default
        $this->tester->assertSame(
            'invalid_request',
            $exception->getError()
        );
        // test all errors
        $errors = [
            'invalid_request',
            'invalid_client',
            'invalid_grant',
            'unauthorized_client',
            'unsupported_grant_type'
        ];
        foreach ($errors as $error) {
            $exception->setError($error);
            $this->tester->assertSame(
                $error,
                $exception->getError()
            );
        }
        // test wrong error
        $this->tester->expectThrowable(
            RuntimeException::class,
            function () use ($exception) {
                $exception->setError('some_invalid_error');
            }
        );
        // test response with specific header
        $response = (new ResponseFactory())->createResponse();
        $exception->setAuthenticateLocation(
            'header-location-content'
        );
        $responseCustom = $exception->getResponseWithAuthenticate(
            $response
        );
        $this->tester->assertInstanceOf(
            ResponseInterface::class,
            $responseCustom
        );
        $this->tester->assertSame(
            'Bearer realm="Xklmicroapps"',
            $responseCustom->getHeaderLine('WWW-Authenticate')
        );
        $this->tester->assertSame(
            'header-location-content',
            $responseCustom->getHeaderLine('Xklmicroapps-Authenticate-Location')
        );
    }
}
