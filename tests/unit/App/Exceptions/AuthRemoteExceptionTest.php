<?php

declare(strict_types=1);

namespace SlimApiEssentials\Tests\App\Exceptions;

use Codeception\Test\Unit;
use SlimApiEssentials\App\Exceptions\AuthRemoteException;
use SlimApiEssentials\App\Exceptions\AuthException;
use RuntimeException;
use Psr\Http\Message\ResponseInterface;
use Slim\Psr7\Factory\ResponseFactory;

class AuthRemoteExceptionTest extends Unit
{
    /**
     * @var \Tests\UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    public function testException()
    {
        $exception = new AuthRemoteException();

        // test instance type
        $this->tester->assertInstanceOf(
            AuthException::class,
            $exception
        );
        // create response
        $response = (new ResponseFactory())->createResponse();

        // set value and test
        $exception->setWwwAuthenticate('www auth dummy');
        $exception->setAuthenticateLocation(
            'header-location-content'
        );
        $responseCustom = $exception->getResponseWithAuthenticate(
            $response
        );
        $this->tester->assertInstanceOf(
            ResponseInterface::class,
            $responseCustom
        );
        $this->tester->assertSame(
            'www auth dummy',
            $responseCustom->getHeaderLine('WWW-Authenticate')
        );
        $this->tester->assertSame(
            'header-location-content',
            $responseCustom->getHeaderLine('Xklmicroapps-Authenticate-Location')
        );
    }
}
