<?php

declare(strict_types=1);

namespace SlimApiEssentials\Tests\App\Exceptions;

use Codeception\Test\Unit;
use SlimApiEssentials\App\Exceptions\AuthException;
use RuntimeException;

class AuthExceptionTest extends Unit
{
    /**
     * @var \Tests\UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    public function testException()
    {
        $exception = new AuthException();

        // test instance type
        $this->tester->assertInstanceOf(
            RuntimeException::class,
            $exception
        );
        // test attrs values
        $this->tester->assertSame(
            401,
            $exception->getCode()
        );
        $this->tester->assertSame(
            'Unauthorized!',
            $exception->getMessage()
        );
    }
}
