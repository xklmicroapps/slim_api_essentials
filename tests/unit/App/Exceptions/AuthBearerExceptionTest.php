<?php

declare(strict_types=1);

namespace SlimApiEssentials\Tests\App\Exceptions;

use Codeception\Test\Unit;
use SlimApiEssentials\App\Exceptions\AuthBearerException;
use SlimApiEssentials\App\Exceptions\AuthException;
use RuntimeException;
use Psr\Http\Message\ResponseInterface;
use Slim\Psr7\Factory\ResponseFactory;

class AuthBearerExceptionTest extends Unit
{
    /**
     * @var \Tests\UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    public function testException()
    {
        $exception = new AuthBearerException();

        // test instance type
        $this->tester->assertInstanceOf(
            AuthException::class,
            $exception
        );
        // create response
        $response = (new ResponseFactory())->createResponse();

        // set value and test
        $exception->setAuthenticateLocation('auth-location-string');
        $responseCustom = $exception->getResponseWithAuthenticate(
            $response
        );
        $this->tester->assertInstanceOf(
            ResponseInterface::class,
            $responseCustom
        );
        $this->tester->assertSame(
            'Bearer realm="Xklmicroapps"',
            $responseCustom->getHeaderLine('WWW-Authenticate')
        );
        $this->tester->assertSame(
            'auth-location-string',
            $responseCustom->getHeaderLine('Xklmicroapps-Authenticate-Location')
        );
    }
}
