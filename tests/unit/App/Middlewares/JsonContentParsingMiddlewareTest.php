<?php

declare(strict_types=1);

namespace SlimApiEssentials\Tests\App\Middlewares;

use Codeception\Test\Unit;
use SlimApiEssentials\App\Middlewares\JsonContentParsingMiddleware;
use SlimApiEssentials\App\Services\RequestHelper;
use RuntimeException;
use Slim\Psr7\Request as SlimRequest;
use Slim\Psr7\Response as SlimResponse;
use Slim\App;
use Slim\Psr7\Stream as SlimStream;

class JsonContentParsingMiddlewareTest extends Unit
{
    /**
     * @var \Tests\UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    public function testWithBody()
    {
        // mock
        $requestHelper = $this->make(
            RequestHelper::class,
            [
                'isWithBody' => function () {
                    return true;
                }
            ]
        );
        // init middleware
        $middleware = new JsonContentParsingMiddleware(
            $requestHelper
        );
        // process middleware ok
        $processedValue = $middleware->process(
            $this->make(
                SlimRequest::class,
                [
                    'getHeaderLine' => 'someother/contenttype',
                    'getParsedBody' => ['anything'],
                ]
            ),
            $this->make(
                App::class,
                [
                    'handle' => new SlimResponse()
                ]
            )
        );
        $this->tester->assertInstanceOf(
            SlimResponse::class,
            $processedValue
        );
        // set some stream
        $streamError = fopen('php://memory', 'r+');
        fwrite($streamError, 'some dummy ERROR CONTENT');
        rewind($streamError);
        // process middleware error
        $this->tester->expectThrowable(
            RuntimeException::class,
            function () use ($middleware, $streamError) {
                $middleware->process(
                    $this->make(
                        SlimRequest::class,
                        [
                            'getHeaderLine' => 'application/json',
                            'getParsedBody' => [],
                            'getBody' => new SlimStream($streamError)
                        ]
                    ),
                    $this->make(
                        App::class,
                        [
                            'handle' => new SlimResponse()
                        ]
                    )
                );
            }
        );
        // set some stream
        $streamOk = fopen('php://memory', 'r+');
        fwrite($streamOk, '{"jsoncontent": "some dummy OK JSON CONTENT"}');
        rewind($streamOk);
        // process middleware ok
        $processedValue = $middleware->process(
            $this->make(
                SlimRequest::class,
                [
                    'getHeaderLine' => 'application/json',
                    'getParsedBody' => [],
                    'getBody' => new SlimStream($streamOk),
                    'withParsedBody' => $this->make(SlimRequest::class)
                ]
            ),
            $this->make(
                App::class,
                [
                    'handle' => new SlimResponse()
                ]
            )
        );
        $this->tester->assertInstanceOf(
            SlimResponse::class,
            $processedValue
        );
    }

    public function testNotWithBody()
    {
        // mock
        $requestHelper = $this->make(
            RequestHelper::class,
            [
                'isWithBody' => function () {
                    return true;
                }
            ]
        );
        // init middleware
        $middleware = new JsonContentParsingMiddleware(
            $requestHelper
        );
        // process middleware ok
        $processedValue = $middleware->process(
            $this->make(
                SlimRequest::class,
                [
                    'getHeaderLine' => 'someother/contenttype',
                    'getParsedBody' => ['anything']
                ]
            ),
            $this->make(
                App::class,
                [
                    'handle' => new SlimResponse()
                ]
            )
        );
        $this->tester->assertInstanceOf(
            SlimResponse::class,
            $processedValue
        );
        // process middleware ok
        $processedValue = $middleware->process(
            $this->make(
                SlimRequest::class,
                [
                    'getHeaderLine' => 'application/json',
                    'getParsedBody' => ['anything']
                ]
            ),
            $this->make(
                App::class,
                [
                    'handle' => new SlimResponse()
                ]
            )
        );
        $this->tester->assertInstanceOf(
            SlimResponse::class,
            $processedValue
        );
    }
}
