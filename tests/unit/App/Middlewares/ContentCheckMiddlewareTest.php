<?php

declare(strict_types=1);

namespace SlimApiEssentials\Tests\App\Middlewares;

use Codeception\Test\Unit;
use SlimApiEssentials\App\Middlewares\ContentCheckMiddleware;
use SlimApiEssentials\App\Services\RequestHelper;
use RuntimeException;
use Slim\Psr7\Request as SlimRequest;
use Slim\Psr7\Response as SlimResponse;
use Slim\App;
use SlimApiEssentials\App\Exceptions\AuthBearerException;
use Codeception\Stub\Expected;
use Slim\Routing\RouteContext;
use Slim\Interfaces\RouteInterface;
use Slim\Interfaces\RouteParserInterface;
use Slim\Routing\RoutingResults;
use SlimApiEssentials\App\Di\Container;

class ContentCheckMiddlewareTest extends Unit
{
    /**
     * @var \Tests\UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    public function testWithBodyOauth2RouteBadContentType()
    {
        // mock
        $requestHelper = $this->make(
            RequestHelper::class,
            [
                'isWithBody' => function () {
                    return true;
                }
            ]
        );

        $container = $this->make(
            Container::class,
            [
                'getConfigParameter' => Expected::once(
                    function ($val) {
                        if ($val === 'authOauth2') {
                            return [
                                'routeName' => 'oauth2-token-predefined-routename'
                            ];
                        }
                        return null;
                    }
                )
            ]
        );

        $app = $this->make(
            App::class,
            [
                'handle' => Expected::never(
                    function () {
                        return new SlimResponse();
                    }
                ),
                'getContainer' => Expected::once(
                    $container
                )
            ]
        );

        // init middleware
        $middleware = new ContentCheckMiddleware(
            $app,
            $requestHelper
        );

        /**
         * request on some acutal route
         */
        $request = $this->make(
            SlimRequest::class,
            [
                'getHeaderLine' => Expected::once(
                    function ($headerVal) {
                        if ($headerVal === 'Content-Type') {
                            return 'application/json';
                        }
                        return '';
                    }
                ),
                'getAttribute' => Expected::exactly(
                    4,
                    function ($val) {
                        if ($val === RouteContext::ROUTE) {
                            return $this->makeEmpty(
                                RouteInterface::class,
                                [
                                    'getName' => Expected::once(
                                        'oauth2-token-predefined-routename'
                                    )
                                ]
                            );
                        }
                        if ($val === RouteContext::ROUTE_PARSER) {
                            return $this->makeEmpty(RouteParserInterface::class);
                        }
                        if ($val === RouteContext::ROUTING_RESULTS) {
                            return $this->makeEmpty(RoutingResults::class);
                        }
                        return 'somevalue';
                    }
                )
            ]
        );

        $this->tester->expectThrowable(
            new RuntimeException('Unsupported Media Type', 415),
            function () use ($middleware, $request, $app) {
                $middleware->process(
                    $request,
                    $app
                );
            }
        );
    }

    public function testWithBodyOauth2RouteOk()
    {
        // mock
        $requestHelper = $this->make(
            RequestHelper::class,
            [
                'isWithBody' => function () {
                    return true;
                }
            ]
        );

        $container = $this->make(
            Container::class,
            [
                'getConfigParameter' => Expected::once(
                    function ($val) {
                        if ($val === 'authOauth2') {
                            return [
                                'routeName' => 'oauth2-token-predefined-routename'
                            ];
                        }
                        return null;
                    }
                )
            ]
        );

        $app = $this->make(
            App::class,
            [
                'handle' => Expected::once(
                    function () {
                        return new SlimResponse();
                    }
                ),
                'getContainer' => Expected::once(
                    $container
                )
            ]
        );

        // init middleware
        $middleware = new ContentCheckMiddleware(
            $app,
            $requestHelper
        );

        /**
         * request on some acutal route
         */
        $request = $this->make(
            SlimRequest::class,
            [
                'getHeaderLine' => Expected::once(
                    function ($headerVal) {
                        if ($headerVal === 'Content-Type') {
                            return 'application/x-www-form-urlencoded';
                        }
                        return '';
                    }
                ),
                'getAttribute' => Expected::exactly(
                    4,
                    function ($val) {
                        if ($val === RouteContext::ROUTE) {
                            return $this->makeEmpty(
                                RouteInterface::class,
                                [
                                    'getName' => Expected::once(
                                        'oauth2-token-predefined-routename'
                                    )
                                ]
                            );
                        }
                        if ($val === RouteContext::ROUTE_PARSER) {
                            return $this->makeEmpty(RouteParserInterface::class);
                        }
                        if ($val === RouteContext::ROUTING_RESULTS) {
                            return $this->makeEmpty(RoutingResults::class);
                        }
                        return 'somevalue';
                    }
                )
            ]
        );
        // process middleware ok
        $middleware->process(
            $request,
            $app
        );
    }

    public function testWithBodyGeneralRouteBadContentType()
    {
        // mock
        $requestHelper = $this->make(
            RequestHelper::class,
            [
                'isWithBody' => function () {
                    return true;
                }
            ]
        );

        $container = $this->make(
            Container::class,
            [
                'getConfigParameter' => Expected::exactly(
                    2,
                    function ($val) {
                        if ($val === 'authOauth2') {
                            return [
                                'routeName' => 'oauth2-token-predefined-routename'
                            ];
                        }
                        if ($val === 'contentCheck') {
                            return 'json';
                        }
                        return null;
                    }
                )
            ]
        );

        $app = $this->make(
            App::class,
            [
                'handle' => Expected::never(
                    function () {
                        return new SlimResponse();
                    }
                ),
                'getContainer' => Expected::exactly(
                    2,
                    $container
                )
            ]
        );

        // init middleware
        $middleware = new ContentCheckMiddleware(
            $app,
            $requestHelper
        );

        /**
         * request on some acutal route
         */
        $request = $this->make(
            SlimRequest::class,
            [
                'getHeaderLine' => Expected::exactly(
                    1,
                    function ($headerVal) {
                        if ($headerVal === 'Content-Type') {
                            return 'application/x-www-form-urlencoded';
                        }
                        return '';
                    }
                ),
                'getAttribute' => Expected::exactly(
                    4,
                    function ($val) {
                        if ($val === RouteContext::ROUTE) {
                            return $this->makeEmpty(
                                RouteInterface::class,
                                [
                                    'getName' => Expected::once(
                                        'actual-route-name'
                                    )
                                ]
                            );
                        }
                        if ($val === RouteContext::ROUTE_PARSER) {
                            return $this->makeEmpty(RouteParserInterface::class);
                        }
                        if ($val === RouteContext::ROUTING_RESULTS) {
                            return $this->makeEmpty(RoutingResults::class);
                        }
                        return 'somevalue';
                    }
                )
            ]
        );

        $this->tester->expectThrowable(
            new RuntimeException('Unsupported Media Type', 415),
            function () use ($middleware, $request, $app) {
                $middleware->process(
                    $request,
                    $app
                );
            }
        );
    }

    public function testWithBodyGeneralRouteOk()
    {
        // mock
        $requestHelper = $this->make(
            RequestHelper::class,
            [
                'isWithBody' => function () {
                    return true;
                }
            ]
        );

        $container = $this->make(
            Container::class,
            [
                'getConfigParameter' => Expected::exactly(
                    2,
                    function ($val) {
                        if ($val === 'authOauth2') {
                            return [
                                'routeName' => 'oauth2-token-predefined-routename'
                            ];
                        }
                        if ($val === 'contentCheck') {
                            return 'json';
                        }
                        return null;
                    }
                )
            ]
        );

        $app = $this->make(
            App::class,
            [
                'handle' => Expected::once(
                    function () {
                        return new SlimResponse();
                    }
                ),
                'getContainer' => Expected::exactly(
                    2,
                    $container
                )
            ]
        );

        // init middleware
        $middleware = new ContentCheckMiddleware(
            $app,
            $requestHelper
        );

        /**
         * request on some acutal route
         */
        $request = $this->make(
            SlimRequest::class,
            [
                'getHeaderLine' => Expected::once(
                    function ($headerVal) {
                        if ($headerVal === 'Content-Type') {
                            return 'application/json';
                        }
                        return '';
                    }
                ),
                'getAttribute' => Expected::exactly(
                    4,
                    function ($val) {
                        if ($val === RouteContext::ROUTE) {
                            return $this->makeEmpty(
                                RouteInterface::class,
                                [
                                    'getName' => Expected::once(
                                        'actual-route-name'
                                    )
                                ]
                            );
                        }
                        if ($val === RouteContext::ROUTE_PARSER) {
                            return $this->makeEmpty(RouteParserInterface::class);
                        }
                        if ($val === RouteContext::ROUTING_RESULTS) {
                            return $this->makeEmpty(RoutingResults::class);
                        }
                        return 'somevalue';
                    }
                )
            ]
        );
        // process middleware ok
        $middleware->process(
            $request,
            $app
        );
    }

    public function testWithoutBodyOauth2Route()
    {
        // mock
        $requestHelper = $this->make(
            RequestHelper::class,
            [
                'isWithBody' => function () {
                    return false;
                }
            ]
        );

        $container = $this->make(
            Container::class,
            [
                'getConfigParameter' => Expected::once(
                    function ($val) {
                        if ($val === 'authOauth2') {
                            return [
                                'routeName' => 'oauth2-token-predefined-routename'
                            ];
                        }
                        return null;
                    }
                )
            ]
        );

        $app = $this->make(
            App::class,
            [
                'handle' => Expected::never(
                    function () {
                        return new SlimResponse();
                    }
                ),
                'getContainer' => Expected::once(
                    $container
                )
            ]
        );

        // init middleware
        $middleware = new ContentCheckMiddleware(
            $app,
            $requestHelper
        );

        /**
         * request on some acutal route
         */
        $request = $this->make(
            SlimRequest::class,
            [
                'getHeaderLine' => Expected::once(
                    function ($headerVal) {
                        if ($headerVal === 'Content-Type') {
                            return 'application/json';
                        }
                        return '';
                    }
                ),
                'getAttribute' => Expected::exactly(
                    4,
                    function ($val) {
                        if ($val === RouteContext::ROUTE) {
                            return $this->makeEmpty(
                                RouteInterface::class,
                                [
                                    'getName' => Expected::once(
                                        'oauth2-token-predefined-routename'
                                    )
                                ]
                            );
                        }
                        if ($val === RouteContext::ROUTE_PARSER) {
                            return $this->makeEmpty(RouteParserInterface::class);
                        }
                        if ($val === RouteContext::ROUTING_RESULTS) {
                            return $this->makeEmpty(RoutingResults::class);
                        }
                        return 'somevalue';
                    }
                )
            ]
        );

        $this->tester->expectThrowable(
            new RuntimeException('Unsupported Media Type', 415),
            function () use ($middleware, $request, $app) {
                $middleware->process(
                    $request,
                    $app
                );
            }
        );
    }

    public function testWithoutBodyGeneralRoute()
    {
        // mock
        $requestHelper = $this->make(
            RequestHelper::class,
            [
                'isWithBody' => function () {
                    return false;
                }
            ]
        );

        $container = $this->make(
            Container::class,
            [
                'getConfigParameter' => Expected::exactly(
                    2,
                    function ($val) {
                        if ($val === 'authOauth2') {
                            return [
                                'routeName' => 'oauth2-token-predefined-routename'
                            ];
                        }
                        return null;
                    }
                )
            ]
        );

        $app = $this->make(
            App::class,
            [
                'handle' => Expected::once(
                    function () {
                        return new SlimResponse();
                    }
                ),
                'getContainer' => Expected::exactly(
                    2,
                    $container
                )
            ]
        );

        // init middleware
        $middleware = new ContentCheckMiddleware(
            $app,
            $requestHelper
        );

        /**
         * request on some acutal route
         */
        $request = $this->make(
            SlimRequest::class,
            [
                'getAttribute' => Expected::exactly(
                    4,
                    function ($val) {
                        if ($val === RouteContext::ROUTE) {
                            return $this->makeEmpty(
                                RouteInterface::class,
                                [
                                    'getName' => Expected::once(
                                        'actual-route-name'
                                    )
                                ]
                            );
                        }
                        if ($val === RouteContext::ROUTE_PARSER) {
                            return $this->makeEmpty(RouteParserInterface::class);
                        }
                        if ($val === RouteContext::ROUTING_RESULTS) {
                            return $this->makeEmpty(RoutingResults::class);
                        }
                        return 'somevalue';
                    }
                )
            ]
        );
        // process middleware ok
        $middleware->process(
            $request,
            $app
        );
    }
}
