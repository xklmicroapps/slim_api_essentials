<?php

declare(strict_types=1);

namespace SlimApiEssentials\Tests\App\Middlewares\Auth;

use Codeception\Test\Unit;
use SlimApiEssentials\App\Middlewares\Auth\RemoteMiddleware;
use SlimApiEssentials\App\Services\Auth\Remote;
use Codeception\Stub\Expected;
use Slim\Psr7\Request as SlimRequest;
use Slim\Psr7\Response as SlimResponse;
use Slim\App;
use SlimApiEssentials\App\Di\Container;

class RemoteMiddlewareTest extends Unit
{
    /**
     * @var \Tests\UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    public function testProcess()
    {
        $apikey = $this->make(
            Remote::class,
            [
                'check' => Expected::once(
                    ''
                )
            ]
        );
        // mock service to get ok response
        $container = $this->make(
            Container::class,
            [
                'get' => function ($className) use ($apikey) {
                    $this->assertSame(Remote::class, $className);
                    return $apikey;
                }
            ]
        );
        $app = $this->make(
            App::class,
            [
                'handle' => function () {
                    return new SlimResponse();
                },
                'getContainer' => Expected::once(
                    function () use ($container) {
                        return $container;
                    }
                )
            ]
        );
        // init middleware
        $middleware = new RemoteMiddleware(
            $app
        );

        $processedValue = $middleware->process(
            $this->make(SlimRequest::class),
            $app
        );

        $this->tester->assertInstanceOf(
            SlimResponse::class,
            $processedValue
        );
    }
}
