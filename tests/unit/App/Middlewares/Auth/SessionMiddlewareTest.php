<?php

declare(strict_types=1);

namespace SlimApiEssentials\Tests\App\Middlewares\Auth;

use Codeception\Test\Unit;
use SlimApiEssentials\App\Middlewares\Auth\SessionMiddleware;
use SlimApiEssentials\App\Services\Auth\Session;
use SlimApiEssentials\App\Services\Auth\SessionActions;
use Codeception\Stub\Expected;
use Slim\Psr7\Request as SlimRequest;
use Slim\Psr7\Response as SlimResponse;
use Slim\App;
use SlimApiEssentials\App\Di\Container;
use SlimApiEssentials\App\Exceptions\AuthSessionException;
use SlimApiEssentials\App\Services\RequestHelper;
use SlimApiEssentials\App\Services\ResponseHelper;
use SlimApiEssentials\App\Services\Auth\SessionUserResolverInterface;
use Slim\Routing\RouteParser;
use Slim\Psr7\Factory\ResponseFactory;
use SlimApiEssentials\App\Services\Auth\SessionSerialize;
use Slim\Psr7\Uri;
use Slim\Psr7\Headers;
use Slim\Psr7\Stream;
use SlimApiEssentials\App\Services\Helpers\Common as CommonHelper;
use SlimApiEssentials\App\Services\Helpers\Security as SecurityHelper;
use Slim\Routing\RouteContext;
use Slim\Interfaces\RouteInterface;
use Slim\Interfaces\RouteParserInterface;
use Slim\Routing\RoutingResults;

class SessionMiddlewareTest extends Unit
{
    /**
     * @var \Tests\UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    public function testProcessRouteNameSetOk()
    {
        $userResolver = $this->makeEmpty(
            SessionUserResolverInterface::class
        );
        $routeParser = $this->make(
            RouteParser::class
        );
        $responseFactory = $this->make(
            ResponseFactory::class
        );
        $responseHelper = $this->make(
            ResponseHelper::class
        );

        $requestHelper = $this->make(
            RequestHelper::class
        );
        $sessionActions = $this->make(
            SessionActions::class
        );

        $session = $this->construct(
            Session::class,
            [
                'config' => [
                    'keyUsername' => '',
                    'keyPassword' => '',
                    'routeNameGet' => '',
                    'routeNameSet' => '',
                    'routeNameLoggedFirst' => 'routeNameLoggedFirst-dummy',
                    'routeArgsPersistent' => [],
                    'expiredRedirectPost' => []
                ],
                'actions' => $sessionActions,
                'userResolver' => $userResolver,
                'routeParser' => $routeParser,
                'responseFactory' => $responseFactory,
                'responseHelper' => $responseHelper,
                'sessionSerialize' => new SessionSerialize(
                    $this->make(Container::class)
                )
            ],
            [
                'check' => Expected::never(
                    ''
                ),
                'login' => Expected::once(
                    ''
                ),
                'isRouteNameGet' => false,
                'isRouteNameSet' => true,
                'getResponseRedirectLogged' => Expected::once(
                    new SlimResponse()
                )
            ]
        );
        $container = $this->make(
            Container::class,
            [
                'get' => function ($className) use ($session, $requestHelper) {
                    if ($className === 'SlimApiEssentials\App\Services\Auth\Session') {
                        return $session;
                    }

                    if ($className === 'SlimApiEssentials\App\Services\RequestHelper') {
                        return $requestHelper;
                    }

                    $this->fail('unexpected className ' . $className);
                }
            ]
        );
        $app = $this->make(
            App::class,
            [
                'handle' => function () {
                    return new SlimResponse();
                },
                'getContainer' => Expected::exactly(
                    1,
                    function () use ($container) {
                        return $container;
                    }
                )
            ]
        );
        // init middleware
        $middleware = new SessionMiddleware(
            $app
        );

        $request = (
            new SlimRequest(
                'method',
                new Uri(
                    'https://',
                    'host',
                    null,
                    '/',
                    Session::PARAM_NAME_REQUESTSAVEDFILEPATH . '='
                ),
                new Headers([], []),
                [],
                [],
                new Stream(
                    fopen(
                        sys_get_temp_dir()
                            . '/xkl.test.debug.stream.'
                            . sha1_file(__FILE__),
                        'w'
                    )
                )
            )
        )->withAttribute(
            RouteContext::ROUTE,
            $this->makeEmpty(RouteInterface::class)
        )->withAttribute(
            RouteContext::ROUTE_PARSER,
            $this->makeEmpty(RouteParserInterface::class)
        )->withAttribute(
            RouteContext::ROUTING_RESULTS,
            $this->makeEmpty(RoutingResults::class)
        );

        $processedValue = $middleware->process(
            $request,
            $app
        );

        $this->tester->assertInstanceOf(
            SlimResponse::class,
            $processedValue
        );
    }

    public function testProcessRouteNameSetError()
    {
        $userResolver = $this->makeEmpty(
            SessionUserResolverInterface::class
        );
        $routeParser = $this->make(
            RouteParser::class
        );
        $responseFactory = $this->make(
            ResponseFactory::class
        );
        $responseHelper = $this->make(
            ResponseHelper::class
        );

        $requestHelper = $this->make(
            RequestHelper::class
        );
        $sessionActions = $this->make(
            SessionActions::class
        );

        $session = $this->construct(
            Session::class,
            [
                'config' => [
                    'keyUsername' => '',
                    'keyPassword' => '',
                    'routeNameGet' => '',
                    'routeNameSet' => '',
                    'routeNameLoggedFirst' => 'routeNameLoggedFirst-dummy',
                    'routeArgsPersistent' => [],
                    'expiredRedirectPost' => []
                ],
                'actions' => $sessionActions,
                'userResolver' => $userResolver,
                'routeParser' => $routeParser,
                'responseFactory' => $responseFactory,
                'responseHelper' => $responseHelper,
                'sessionSerialize' => new SessionSerialize(
                    $this->make(Container::class)
                )
            ],
            [
                'check' => Expected::never(
                    ''
                ),
                'login' => Expected::exactly(
                    2,
                    function () {
                        throw new AuthSessionException(
                            "Error Processing Request (dummy)"
                        );
                    }
                ),
                'isRouteNameGet' => false,
                'isRouteNameSet' => true,
                'getResponseRedirectNotLogged' => Expected::exactly(
                    2,
                    new SlimResponse()
                )
            ]
        );
        $container = $this->make(
            Container::class,
            [
                'get' => function ($className) use ($session, $requestHelper) {
                    if ($className === 'SlimApiEssentials\App\Services\Auth\Session') {
                        return $session;
                    }

                    if ($className === 'SlimApiEssentials\App\Services\RequestHelper') {
                        return $requestHelper;
                    }

                    $this->fail('unexpected className ' . $className);
                }
            ]
        );
        $app = $this->make(
            App::class,
            [
                'handle' => function () {
                    return new SlimResponse();
                },
                'getContainer' => Expected::exactly(
                    4,
                    function () use ($container) {
                        return $container;
                    }
                )
            ]
        );
        // init middleware
        $middleware = new SessionMiddleware(
            $app
        );

        $request = (
            new SlimRequest(
                'method',
                new Uri(
                    'https://',
                    'host',
                    null,
                    '/',
                    Session::PARAM_NAME_REQUESTSAVEDFILEPATH . '='
                ),
                new Headers([], []),
                [],
                [],
                new Stream(
                    fopen(
                        sys_get_temp_dir()
                            . '/xkl.test.debug.stream.'
                            . sha1_file(__FILE__),
                        'w'
                    )
                )
            )
        )->withAttribute(
            RouteContext::ROUTE,
            $this->makeEmpty(RouteInterface::class)
        )->withAttribute(
            RouteContext::ROUTE_PARSER,
            $this->makeEmpty(RouteParserInterface::class)
        )->withAttribute(
            RouteContext::ROUTING_RESULTS,
            $this->makeEmpty(RoutingResults::class)
        );
        $requestJson = (
            new SlimRequest(
                'method',
                new Uri(
                    'https://',
                    'host',
                    null,
                    '/',
                    Session::PARAM_NAME_REQUESTSAVEDFILEPATH . '='
                ),
                new Headers(
                    [
                        'Accept' => 'application/json'
                    ],
                    []
                ),
                [],
                [],
                new Stream(
                    fopen(
                        sys_get_temp_dir()
                            . '/xkl.test.debug.stream.'
                            . sha1_file(__FILE__),
                        'w'
                    )
                )
            )
        )->withAttribute(
            RouteContext::ROUTE,
            $this->makeEmpty(RouteInterface::class)
        )->withAttribute(
            RouteContext::ROUTE_PARSER,
            $this->makeEmpty(RouteParserInterface::class)
        )->withAttribute(
            RouteContext::ROUTING_RESULTS,
            $this->makeEmpty(RoutingResults::class)
        );

        $processedValue = $middleware->process(
            $request,
            $app
        );

        $this->tester->assertInstanceOf(
            SlimResponse::class,
            $processedValue
        );

        $this->tester->expectThrowable(
            new AuthSessionException("Error Processing Request (dummy)"),
            function () use ($middleware, $app, $requestJson) {
                $middleware->process(
                    $requestJson,
                    $app
                );
            }
        );
    }

    public function testProcessRouteNameAnyOk()
    {
        $userResolver = $this->makeEmpty(
            SessionUserResolverInterface::class
        );
        $routeParser = $this->make(
            RouteParser::class
        );
        $responseFactory = $this->make(
            ResponseFactory::class
        );
        $responseHelper = $this->make(
            ResponseHelper::class
        );

        $requestHelper = $this->make(
            RequestHelper::class
        );
        $sessionActions = $this->make(
            SessionActions::class
        );

        $session = $this->construct(
            Session::class,
            [
                'config' => [
                    'keyUsername' => '',
                    'keyPassword' => '',
                    'routeNameGet' => '',
                    'routeNameSet' => '',
                    'routeNameLoggedFirst' => 'routeNameLoggedFirst-dummy',
                    'routeArgsPersistent' => [],
                    'expiredRedirectPost' => []
                ],
                'actions' => $sessionActions,
                'userResolver' => $userResolver,
                'routeParser' => $routeParser,
                'responseFactory' => $responseFactory,
                'responseHelper' => $responseHelper,
                'sessionSerialize' => new SessionSerialize(
                    $this->make(Container::class)
                )
            ],
            [
                'check' => Expected::once(
                    ''
                ),
                'login' => Expected::never(),
                'isRouteNameGet' => false,
                'isRouteNameSet' => false,
                'getResponseRedirectLogged' => Expected::never(),
                'getResponseRedirectNotLogged' => Expected::never()
            ]
        );
        $container = $this->make(
            Container::class,
            [
                'get' => function ($className) use ($session, $requestHelper) {
                    if ($className === 'SlimApiEssentials\App\Services\Auth\Session') {
                        return $session;
                    }

                    if ($className === 'SlimApiEssentials\App\Services\RequestHelper') {
                        return $requestHelper;
                    }

                    $this->fail('unexpected className ' . $className);
                }
            ]
        );
        $app = $this->make(
            App::class,
            [
                'handle' => function () {
                    return new SlimResponse();
                },
                'getContainer' => Expected::exactly(
                    1,
                    function () use ($container) {
                        return $container;
                    }
                )
            ]
        );
        // init middleware
        $middleware = new SessionMiddleware(
            $app
        );

        $request = (
            new SlimRequest(
                'method',
                new Uri(
                    'https://',
                    'host',
                    null,
                    '/',
                    Session::PARAM_NAME_REQUESTSAVEDFILEPATH . '='
                ),
                new Headers([], []),
                [],
                [],
                new Stream(
                    fopen(
                        sys_get_temp_dir()
                            . '/xkl.test.debug.stream.'
                            . sha1_file(__FILE__),
                        'w'
                    )
                )
            )
        )->withAttribute(
            RouteContext::ROUTE,
            $this->makeEmpty(RouteInterface::class)
        )->withAttribute(
            RouteContext::ROUTE_PARSER,
            $this->makeEmpty(RouteParserInterface::class)
        )->withAttribute(
            RouteContext::ROUTING_RESULTS,
            $this->makeEmpty(RoutingResults::class)
        );

        $processedValue = $middleware->process(
            $request,
            $app
        );

        $this->tester->assertInstanceOf(
            SlimResponse::class,
            $processedValue
        );
    }

    public function testProcessRouteNameAnyError()
    {
        $userResolver = $this->makeEmpty(
            SessionUserResolverInterface::class
        );
        $routeParser = $this->make(
            RouteParser::class
        );
        $responseFactory = $this->make(
            ResponseFactory::class
        );
        $responseHelper = $this->make(
            ResponseHelper::class
        );

        $requestHelper = $this->make(
            RequestHelper::class
        );
        $sessionActions = $this->make(
            SessionActions::class
        );

        $session = $this->construct(
            Session::class,
            [
                'config' => [
                    'keyUsername' => '',
                    'keyPassword' => '',
                    'routeNameGet' => '',
                    'routeNameSet' => '',
                    'routeNameLoggedFirst' => 'routeNameLoggedFirst-dummy',
                    'routeArgsPersistent' => [],
                    'expiredRedirectPost' => []
                ],
                'actions' => $sessionActions,
                'userResolver' => $userResolver,
                'routeParser' => $routeParser,
                'responseFactory' => $responseFactory,
                'responseHelper' => $responseHelper,
                'sessionSerialize' => new SessionSerialize(
                    $this->make(Container::class)
                )
            ],
            [
                'check' => Expected::exactly(
                    2,
                    function () {
                        throw new AuthSessionException(
                            "Error Processing Request (dummy)"
                        );
                    }
                ),
                'login' => Expected::never(),
                'isRouteNameGet' => false,
                'isRouteNameSet' => false,
                'getResponseRedirectLogged' => Expected::never(),
                'getResponseRedirectNotLogged' => Expected::exactly(
                    2,
                    new SlimResponse()
                )
            ]
        );
        $container = $this->make(
            Container::class,
            [
                'get' => function ($className) use ($session, $requestHelper) {
                    if ($className === 'SlimApiEssentials\App\Services\Auth\Session') {
                        return $session;
                    }

                    if ($className === 'SlimApiEssentials\App\Services\RequestHelper') {
                        return $requestHelper;
                    }

                    $this->fail('unexpected className ' . $className);
                }
            ]
        );
        $app = $this->make(
            App::class,
            [
                'handle' => function () {
                    return new SlimResponse();
                },
                'getContainer' => Expected::exactly(
                    2,
                    function () use ($container) {
                        return $container;
                    }
                )
            ]
        );
        // init middleware
        $middleware = new SessionMiddleware(
            $app
        );

        $request = (
            new SlimRequest(
                'method',
                new Uri(
                    'https://',
                    'host',
                    null,
                    '/',
                    Session::PARAM_NAME_REQUESTSAVEDFILEPATH . '='
                ),
                new Headers([], []),
                [],
                [],
                new Stream(
                    fopen(
                        sys_get_temp_dir()
                            . '/xkl.test.debug.stream.'
                            . sha1_file(__FILE__),
                        'w'
                    )
                )
            )
        )->withAttribute(
            RouteContext::ROUTE,
            $this->makeEmpty(RouteInterface::class)
        )->withAttribute(
            RouteContext::ROUTE_PARSER,
            $this->makeEmpty(RouteParserInterface::class)
        )->withAttribute(
            RouteContext::ROUTING_RESULTS,
            $this->makeEmpty(RoutingResults::class)
        );

        $requestJson = (
            new SlimRequest(
                'method',
                new Uri(
                    'https://',
                    'host',
                    null,
                    '/',
                    Session::PARAM_NAME_REQUESTSAVEDFILEPATH . '='
                ),
                new Headers(
                    [
                        'Accept' => 'application/json'
                    ],
                    []
                ),
                [],
                [],
                new Stream(
                    fopen(
                        sys_get_temp_dir()
                            . '/xkl.test.debug.stream.'
                            . sha1_file(__FILE__),
                        'w'
                    )
                )
            )
        )->withAttribute(
            RouteContext::ROUTE,
            $this->makeEmpty(RouteInterface::class)
        )->withAttribute(
            RouteContext::ROUTE_PARSER,
            $this->makeEmpty(RouteParserInterface::class)
        )->withAttribute(
            RouteContext::ROUTING_RESULTS,
            $this->makeEmpty(RoutingResults::class)
        );

        $processedValue = $middleware->process(
            $request,
            $app
        );

        $this->tester->assertInstanceOf(
            SlimResponse::class,
            $processedValue
        );

        $processedValue = $middleware->process(
            $requestJson,
            $app
        );

        $this->tester->assertInstanceOf(
            SlimResponse::class,
            $processedValue
        );
    }
}
