<?php

declare(strict_types=1);

namespace SlimApiEssentials\Tests\App\Di;

use Slim\App as SlimApp;
use Slim\Routing\RouteCollector;
use Slim\Routing\RouteParser;
use Slim\Psr7\Factory\ResponseFactory;
use Codeception\Test\Unit;
use SlimApiEssentials\App\Di\Services;
use SlimApiEssentials\App\Di\Container;
use SlimApiEssentials\App\Di\ContainerActions;
use SlimApiEssentials\App\Services\Database;
use SlimApiEssentials\App\Services\Auth\Apikey as AuthApikey;
use SlimApiEssentials\App\Services\Auth\Basic as AuthBasic;
use SlimApiEssentials\App\Services\Auth\Oauth2 as AuthOauth2;
use SlimApiEssentials\App\Services\Auth\Remote as AuthRemote;
use SlimApiEssentials\App\Services\Auth\Session as AuthSession;
// use SlimApiEssentials\App\Services\Auth;
use SlimApiEssentials\Tests\Data\App\Di\AuthUserResolverApikeyDummy;
use SlimApiEssentials\Tests\Data\App\Di\AuthUserResolverBasicDummy;
use SlimApiEssentials\Tests\Data\App\Di\AuthUserResolverOauth2Dummy;
use SlimApiEssentials\Tests\Data\App\Di\AuthUserResolverSessionDummy;
use SlimApiEssentials\App\Services\Auth\Oauth2TokenManager;
use SlimApiEssentials\App\Services\Helpers\Security as SecurityHelper;
use PDO;

class ServicesTest extends Unit
{
    /**
     * @var \Tests\UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    public function testGetSlimApiEssentialsAppServicesHelpersSecurity()
    {
        $services = $this->make(Services::class);

        $containerActions = $this->make(ContainerActions::class);

        $container = $this->construct(
            Container::class,
            [
                'actions' => $containerActions
            ],
            [
                'getConfigParameters' => [
                    'security' => [
                        'secret' => 'somedummysecret',
                        'iv' => 'somedummyiv'
                    ]
                ],
                'getSubContainers' => [
                    $services
                ]
            ]
        );

        $services->setContainer($container);

        $auth = $services->getSlimApiEssentialsAppServicesHelpersSecurity();

        $this->tester->assertInstanceOf(
            SecurityHelper::class,
            $auth
        );
    }

    public function testGetSlimApiEssentialsAppServicesAuthOauth2TokenManager()
    {
        $services = $this->make(Services::class);

        $containerActions = $this->make(ContainerActions::class);

        $container = $this->construct(
            Container::class,
            [
                'actions' => $containerActions
            ],
            [
                'getConfigParameters' => [
                    'security' => [
                        'secret' => 'somedummysecret',
                        'iv' => 'somedummyiv'
                    ]
                ],
                'getSubContainers' => [
                    $services
                ]
            ]
        );

        $services->setContainer($container);

        $auth = $services->getSlimApiEssentialsAppServicesAuthOauth2TokenManager();

        $this->tester->assertInstanceOf(
            Oauth2TokenManager::class,
            $auth
        );
    }

    public function testGetSlimApiEssentialsAppServicesAuthSession()
    {
        $services = $this->make(Services::class);

        $containerActions = $this->make(ContainerActions::class);

        $routeParser = $this->makeEmpty(
            RouteParser::class
        );
        $routeCollector = $this->makeEmpty(
            RouteCollector::class,
            [
                'getRouteParser' => $routeParser
            ]
        );

        $responseFactory = $this->makeEmpty(ResponseFactory::class);

        $app = $this->make(
            SlimApp::class,
            [
                'getRouteCollector' => $routeCollector,
                'getResponseFactory' => $responseFactory
            ]
        );

        $container = $this->construct(
            Container::class,
            [
                'actions' => $containerActions
            ],
            [
                'getConfigParameters' => [
                    'authSession' => [
                        'userResolver' => AuthUserResolverSessionDummy::class,
                        'keyUsername' => 'keyUsernameVale',
                        'keyPassword' => 'keyPasswordValue',
                        'routeNameGet' => 'routeNameGet-value',
                        'routeNameSet' => 'routeNameSet-value',
                        'routeNameLoggedFirst' => 'routeNameLoggedFirst-value',
                        'routeArgsPersistent' => [],
                        'expiredRedirectPost' => []
                    ]
                ],
                'getSubContainers' => [
                    $services
                ],
                'getApp' => $app
            ]
        );

        $services->setContainer($container);

        $auth = $services->getSlimApiEssentialsAppServicesAuthSession();

        $this->tester->assertInstanceOf(
            AuthSession::class,
            $auth
        );
    }

    public function testGetSlimApiEssentialsAppServicesAuthRemote()
    {
        $services = $this->make(Services::class);

        $containerActions = $this->make(ContainerActions::class);

        $container = $this->construct(
            Container::class,
            [
                'actions' => $containerActions
            ],
            [
                'getConfigParameters' => [
                    'authRemote' => [
                        'url' => [
                            'authtype' => 'some dummy url'
                        ],
                        'certVerify' => true
                    ]
                ],
                'getSubContainers' => [
                    $services
                ]
            ]
        );

        $services->setContainer($container);

        $auth = $services->getSlimApiEssentialsAppServicesAuthRemote();

        $this->tester->assertInstanceOf(
            AuthRemote::class,
            $auth
        );
    }

    public function testGetSlimApiEssentialsAppServicesAuthOauth2()
    {
        $services = $this->make(Services::class);

        $containerActions = $this->make(ContainerActions::class);

        $container = $this->construct(
            Container::class,
            [
                'actions' => $containerActions
            ],
            [
                'getConfigParameters' => [
                    'authOauth2' => [
                        'userResolver' => AuthUserResolverOauth2Dummy::class
                    ],
                    'security' => [
                        'secret' => 'somedummysecret',
                        'iv' => 'somedummyiv'
                    ]
                ],
                'getSubContainers' => [
                    $services
                ]
            ]
        );

        $services->setContainer($container);

        $auth = $services->getSlimApiEssentialsAppServicesAuthOauth2();

        $this->tester->assertInstanceOf(
            AuthOauth2::class,
            $auth
        );
    }

    public function testGetSlimApiEssentialsAppServicesAuthBasic()
    {
        $services = $this->make(Services::class);

        $containerActions = $this->make(ContainerActions::class);

        $container = $this->construct(
            Container::class,
            [
                'actions' => $containerActions
            ],
            [
                'getConfigParameters' => [
                    'authBasic' => [
                        'userResolver' => AuthUserResolverBasicDummy::class
                    ]
                ],
                'getSubContainers' => [
                    $services
                ]
            ]
        );

        $services->setContainer($container);

        $auth = $services->getSlimApiEssentialsAppServicesAuthBasic();

        $this->tester->assertInstanceOf(
            AuthBasic::class,
            $auth
        );
    }

    public function testGetSlimApiEssentialsAppServicesAuthApikey()
    {
        $services = $this->make(Services::class);

        $containerActions = $this->make(ContainerActions::class);

        $container = $this->construct(
            Container::class,
            [
                'actions' => $containerActions
            ],
            [
                'getConfigParameters' => [
                    'authApikey' => [
                        'userResolver' => AuthUserResolverApikeyDummy::class
                    ]
                ],
                'getSubContainers' => [
                    $services
                ]
            ]
        );

        $services->setContainer($container);

        $auth = $services->getSlimApiEssentialsAppServicesAuthApikey();

        $this->tester->assertInstanceOf(
            AuthApikey::class,
            $auth
        );
    }

    /**
     * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
     *
     *     REQUIRES SQLITE3 php module
     *
     * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
     */
    public function testGetPdo()
    {
        $services = $this->make(Services::class);

        $container = $this->make(
            Container::class,
            [
                'getConfigParameters' => [
                    'pdo' => [
                        'dsn' => 'sqlite::memory:'
                    ]
                ]
            ]
        );

        $services->setContainer($container);

        $pdo = $services->getPDO();

        $this->tester->assertInstanceOf(
            PDO::class,
            $pdo
        );
    }

    /**
     * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
     *
     *     REQUIRES SQLITE3 php module
     *
     * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
     */
    public function testGetDatabase()
    {
        $containerActions = $this->make(ContainerActions::class);

        $services = $this->make(Services::class);

        $container = $this->construct(
            Container::class,
            [
                'actions' => $containerActions
            ],
            [
                'getConfigParameters' => [
                    'pdo' => [
                        'dsn' => 'sqlite::memory:'
                    ]
                ],
                'getSubContainers' => [
                    $services
                ]
            ]
        );

        $services->setContainer($container);

        $database = $container->get(Database::class);

        $this->tester->assertInstanceOf(
            Database::class,
            $database
        );
    }
}
