<?php

declare(strict_types=1);

namespace SlimApiEssentials\Tests\App\Di;

use SlimApiEssentials\App\Di\Container;
use SlimApiEssentials\App\Di\ContainerActions;
use SlimApiEssentials\App\Di\AbstractServices;
use Codeception\Test\Unit;
use Slim\App;
use Slim\Psr7\Factory\ResponseFactory;
use LogicException;
use RuntimeException;
use StdClass;
use DateTime;
use SlimApiEssentials\App\Middlewares\ContentCheckMiddleware;
use Slim\Middleware\RoutingMiddleware;
use SlimApiEssentials\App\Middlewares\JsonContentParsingMiddleware;
use Slim\Middleware\ErrorMiddleware;

class ContainerTest extends Unit
{
    /**
     * @var \Tests\UnitTester
     */
    protected $tester;

    protected $container;

    protected function _before()
    {
        $this->container = new Container(
            new ContainerActions()
        );
    }

    protected function _after()
    {
    }

    public function testGetContainer()
    {
        /**
         * get Container must return the same instance of container
         */
        $this->tester->assertSame(
            $this->container,
            $this->container->get($this->container::class)
        );
    }

    public function testSetGetAppMiddlewaresAndRoutingException()
    {
        $this->tester->expectThrowable(
            new LogicException(
                'this->appMiddlewares not set! Must be set before by SlimApiEssentials\App\Bootstrap\Actions::setAppMiddlewares()'
            ),
            function () {
                $this->container->getAppMiddlewares();
            }
        );

        $this->tester->expectThrowable(
            new LogicException(
                'this->appMiddlewares not set! Must be set before by SlimApiEssentials\App\Bootstrap\Actions::setAppMiddlewares()'
            ),
            function () {
                $this->container->getRoutingMiddleware();
            }
        );

        $this->container->setAppMiddlewares(
            [
                $this->make(ContentCheckMiddleware::class),
                $this->make(JsonContentParsingMiddleware::class),
            ]
        );

        $this->tester->expectThrowable(
            new LogicException(
                'Slim\Middleware\RoutingMiddleware not found! Must be set before by SlimApiEssentials\App\Bootstrap\Actions::setAppMiddlewares()'
            ),
            function () {
                $this->container->getRoutingMiddleware();
            }
        );
    }

    public function testSetGetAppMiddlewaresAndRouting()
    {
        $this->container->setAppMiddlewares(
            [
                $this->make(ContentCheckMiddleware::class),
                $this->make(RoutingMiddleware::class),
                $this->make(JsonContentParsingMiddleware::class),
                $this->make(ErrorMiddleware::class)
            ]
        );

        $appMiddlewares = $this->container->getAppMiddlewares();
        $this->tester->assertCount(
            4,
            $appMiddlewares
        );
        $this->tester->assertSame(
            [0, 1, 2, 3],
            array_keys($appMiddlewares)
        );
        $this->tester->assertInstanceOf(
            ContentCheckMiddleware::class,
            $appMiddlewares[0]
        );
        $this->tester->assertInstanceOf(
            RoutingMiddleware::class,
            $appMiddlewares[1]
        );
        $this->tester->assertInstanceOf(
            JsonContentParsingMiddleware::class,
            $appMiddlewares[2]
        );
        $this->tester->assertInstanceOf(
            ErrorMiddleware::class,
            $appMiddlewares[3]
        );

        $this->tester->assertInstanceOf(
            RoutingMiddleware::class,
            $this->container->getRoutingMiddleware()
        );
    }

    public function testSetGetSubContainers()
    {
        $subContainer1Set = new class extends AbstractServices {
            public function somemethod()
            {
            }
        };
        $subContainer2Set = new class extends AbstractServices {
        };

        $this->container->setSubContainers(
            [
                $subContainer1Set,
                $subContainer2Set
            ]
        );
        $subContainersGet = $this->container->getSubContainers();

        $this->tester->assertCount(
            2,
            $subContainersGet
        );

        $this->tester->assertSame(
            $subContainer1Set,
            $subContainersGet[0]
        );

        $this->tester->assertSame(
            $subContainer2Set,
            $subContainersGet[1]
        );
    }

    /**
     * @dataProvider getTestServicesData
     */
    public function testSetServices(string $classname, array $requires, array $args = [])
    {
        // require dummy class file to test
        foreach ($requires as $require) {
            require_once $require;
        }

        // test auto get services
        $object = $this->container->get($classname, ...$args);

        $this->tester->assertInstanceOf(
            $classname,
            $object
        );

        $newService = new StdClass();
        // test explicitly set service and then get service
        $this->container->set($classname, $newService, ...$args);

        $object = $this->container->get($classname, ...$args);
        $this->tester->assertSame(
            $newService,
            $object
        );
    }

    /**
     * @dataProvider getTestServicesData
     */
    public function testGetServicesSeqParameters(string $classname, array $requires, array $args = [])
    {
        // require dummy class file to test
        foreach ($requires as $require) {
            require_once $require;
        }

        $object = $this->container->get($classname, ...$args);

        $this->tester->assertInstanceOf(
            $classname,
            $object
        );
    }

    /**
     * @dataProvider getTestServicesNamedParametersData
     */
    public function testGetServicesNamedParameters(
        string $classname,
        array $requires,
        array $args = [],
        array $resultValues = []
    ) {
        // require dummy class file to test
        foreach ($requires as $require) {
            require_once $require;
        }

        $object = $this->container->get($classname, ...$args);

        $this->tester->assertInstanceOf(
            $classname,
            $object
        );

        foreach ($resultValues as $key => $value) {
            $actual = $object->$key;
            $expected = $value;
            if (is_array($value)) {
                $firstKey = array_key_first($value);
                $actual = $firstKey($actual);
                $expected = $value[$firstKey];
            }
            $this->tester->assertSame(
                $expected,
                $actual
            );
        }
    }

    public function testSetGetConfigParameters()
    {
        $somearray = ['a' => 'b', 'c' => ['d']];
        $someobject = new StdClass();
        $someobject->someval = 'someobjectval';

        $params = [
            ['isDebug', 'false', 'false'],
            ['apiAuthCertVerify', true, true],
            ['somestringbool', false, false],
            ['postgresPort', '111.22', '111.22'],
            ['unauthSleepMiliseconds', '222.33', '222.33'],
            ['someFloat', 222.66, 222.66],
            ['someintValue', 222, 222],
            ['somestring', 'somestringvalue', 'somestringvalue'],
            ['somebooltrue', true, true],
            ['someboolfalse', false, false],
            ['somearray', $somearray, $somearray],
            ['someobject', $someobject, $someobject],
            ['nullvalue', null, null],
        ];

        $paramsSet = [];
        $paramsGet = [];
        foreach ($params as $param) {
            $paramsSet[$param[0]] = $param[1];
            $paramsGet[$param[0]] = $param[2];
        }

        // set without nullvalue param ok
        $this->container->setConfigParameters($paramsSet);

        $actualConfigParameters = $this->container->getConfigParameters();
        // assert array config value
        $this->tester->assertSame(
            $paramsGet['somearray'],
            $actualConfigParameters['somearray']
        );
        // unset array configvalue because it has different refference num
        unset($actualConfigParameters['somearray'], $paramsGet['somearray']);
        // assert same set and get values and types
        $this->tester->assertSame(
            $paramsGet,
            $actualConfigParameters
        );

        // set params again exception
        $this->tester->expectThrowable(
            new RuntimeException('ConfigParameters already set!'),
            function () use ($paramsSet) {
                $this->container->setConfigParameters($paramsSet);
            }
        );
    }

    public function testSetGetApp()
    {
        $appSet = new App(
            new ResponseFactory()
        );

        $this->container->setApp($appSet);
        $appGet = $this->container->getApp();

        $this->tester->assertSame(
            $appSet,
            $appGet
        );
    }

    public function getTestServicesNamedParametersData()
    {
        $dateTime = new DateTime();
        return [
            [
                "\\SlimApiEssentials\\Tests\\Data\\App\\Di\\DummyClassWithConstructorArgsNamed",
                [
                    codecept_data_dir('unit/App/Di/DummyClassWithConstructorArgs.php')
                ],
                [
                    'arg2' => $dateTime
                ],
                [
                    'arg1' => 'arg1DefalutValue',
                    'arg2' => $dateTime
                ]
            ],
            [
                "\\SlimApiEssentials\\Tests\\Data\\App\\Di\\DummyClassWithAnotherDummyClassAutowiredNamed",
                [
                    codecept_data_dir('unit/App/Di/DummyClassWithAnotherDummyClassAutowiredNamed.php'),
                    codecept_data_dir('unit/App/Di/DummyClassToAutowire.php')
                ],
                [
                    'arg3' => 'arg3Value'
                ],
                [
                    'arg1' => null,
                    'arg2' => [
                        'get_class' => 'SlimApiEssentials\Tests\Data\App\Di\DummyClassToAutowire'
                    ],
                    'arg3' => 'arg3Value',
                    'arg4' => null,
                    'arg5' => 'arg5DefaultValue'
                ]
            ]
        ];
    }

    public function getTestServicesData()
    {
        return [
            [
                "\\SlimApiEssentials\\Tests\\Data\\App\\Di\\DummyClassEmpty",
                [
                    codecept_data_dir('unit/App/Di/DummyClassEmpty.php')
                ]
            ],
            [
                "\\SlimApiEssentials\\Tests\\Data\\App\\Di\\DummyClassWithConstructorArgs",
                [
                    codecept_data_dir('unit/App/Di/DummyClassWithConstructorArgs.php')
                ],
                [
                    new DateTime(),
                    new DateTime()
                ]
            ],
            [
                "\\SlimApiEssentials\\Tests\\Data\\App\\Di\\DummyClassWithAnotherDummyClassAutowired",
                [
                    codecept_data_dir('unit/App/Di/DummyClassWithAnotherDummyClassAutowired.php'),
                    codecept_data_dir('unit/App/Di/DummyClassToAutowire.php')
                ],
                [
                    'arg1Value'
                ]
            ]
        ];
    }
}
