<?php

declare(strict_types=1);

namespace SlimApiEssentials\Tests\App\Di;

use Codeception\Test\Unit;
use SlimApiEssentials\App\Di\AbstractServices;
use ReflectionClass;

class AbstractServicesTest extends Unit
{
    /**
     * @var \Tests\UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    public function testRequiredMethods()
    {
        $reflection = new ReflectionClass(AbstractServices::class);

        $this->tester->assertTrue(
            $reflection->isAbstract()
        );

        // test constructor
        $constructor = $reflection->getConstructor();

        $this->tester->assertSame(
            null,
            $constructor
        );

        // test method setContainer
        $this->tester->assertTrue(
            $reflection->hasMethod('setContainer')
        );
        $this->tester->assertCount(
            1,
            $reflection->getMethods()
        );
        $method = $reflection->getMethod('setContainer');
        $this->tester->assertSame(
            1,
            $method->getNumberOfParameters()
        );

        $params = $method->getParameters();
        $this->tester->assertSame(
            'SlimApiEssentials\App\Di\Container',
            $params[0]->getType()->__toString()
        );

        $this->tester->assertSame(
            'void',
            $method->getReturnType()->__toString()
        );
        $this->tester->assertFalse(
            $method->isAbstract()
        );

        // test property $container
        $this->tester->assertTrue(
            $reflection->hasProperty('container')
        );
        $this->tester->assertCount(
            1,
            $reflection->getProperties()
        );
        $property = $reflection->getProperty('container');
        $this->tester->assertSame(
            'SlimApiEssentials\App\Di\Container',
            $property->getType()->__toString()
        );
    }
}
