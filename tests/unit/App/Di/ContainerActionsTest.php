<?php

declare(strict_types=1);

namespace SlimApiEssentials\Tests\App\Di;

use Exception;
use LogicException;
use Codeception\Test\Unit;
use SlimApiEssentials\App\Di\Container;
use SlimApiEssentials\App\Di\ContainerActions;
use ReflectionClass;
use StdClass;
use SlimApiEssentials\Tests\Data\App\Di\DummyDiSubContainer;
use SlimApiEssentials\Tests\Data\App\Di\DummyClassWithAnotherDummyClassAutowired;
use SlimApiEssentials\Tests\Data\App\Di\DummyClassToAutowire;
use SlimApiEssentials\Tests\Data\App\Di\DummyClassWithAnotherDummyInterfaceAutowired;
use SlimApiEssentials\Tests\Data\App\Di\DummyInterfaceToAutowire;

class ContainerActionsTest extends Unit
{
    /**
     * @var \Tests\UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    public function testGetServiceFromIdNameByClass()
    {
        require_once codecept_data_dir('unit/App/Di/DummyClassWithAnotherDummyClassAutowired.php');
        require_once codecept_data_dir('unit/App/Di/DummyClassToAutowire.php');
        require_once codecept_data_dir('unit/App/Di/DummyClassWithAnotherDummyInterfaceAutowired.php');
        require_once codecept_data_dir('unit/App/Di/DummyInterfaceToAutowire.php');

        $actions = $this->make(ContainerActions::class);

        // setcontainer to actions by constucting container
        $container = $this->construct(
            Container::class,
            [
                'actions' => $actions
            ]
        );

        //nonexistent
        $this->tester->assertNull(
            $actions->getServiceFromIdNameByClass(
                'nonexistent',
                []
            )
        );

        //php class names are case insensitive BUT COMPOSER AUTOLOADER IS CASE SENSITIVE
        $service1 = $actions->getServiceFromIdNameByClass(
            'SlimApiEssentials\Tests\Data\app\di\dummyclasswithanotherdummyclassautowired',
            [
                'arg1Value'
            ]
        );

        $service2 = $actions->getServiceFromIdNameByClass(
            'SlimApiEssentials\Tests\Data\App\Di\DummyClassWithAnotherDummyClassAutowired',
            [
                'arg1Value'
            ]
        );

        $this->tester->assertInstanceOf(
            DummyClassWithAnotherDummyClassAutowired::class,
            $service1
        );

        $this->tester->assertInstanceOf(
            DummyClassWithAnotherDummyClassAutowired::class,
            $service2
        );

        $this->tester->expectThrowable(
            new LogicException(
                'Interface can not be autowired (' . DummyInterfaceToAutowire::class . ')'
            ),
            function () use ($actions) {
                $service3 = $actions->getServiceFromIdNameByClass(
                    'SlimApiEssentials\Tests\Data\App\Di\DummyClassWithAnotherDummyInterfaceAutowired',
                    [
                        'arg1Value'
                    ]
                );
            }
        );

        $this->tester->assertSame(
            $container,
            $actions->getServiceFromIdNameByClass($container::class, ['anything'])
        );
    }

    public function testGetServiceFromIdNameByMethod()
    {
        require_once codecept_data_dir('unit/App/Di/DummyDiSubContainer.php');

        $actions = $this->make(ContainerActions::class);

        $container = $this->make(Container::class);

        $container->setSubContainers([new DummyDiSubContainer()]);

        $actions->setContainer($container);

        //nonexistent
        $this->tester->assertNull(
            $actions->getServiceFromIdNameByMethod('nonexistent', ['arg1val', 'arg2val'])
        );

        //php functinos/methods are case insensitive
        $service1 = $actions->getServiceFromIdNameByMethod('SOmeservice', ['arg1val', 'arg2val']);
        $service2 = $actions->getServiceFromIdNameByMethod('someservice', ['arg1val', 'arg2val']);

        $this->tester->assertInstanceOf(
            StdClass::class,
            $service1
        );

        $this->tester->assertInstanceOf(
            StdClass::class,
            $service2
        );

        $this->tester->assertSame(
            'arg1val',
            $service1->arg1
        );

        $this->tester->assertSame(
            'arg2val',
            $service2->arg2
        );
    }

    public function testGetIdxsFromIdnames()
    {
        $actions = $this->make(ContainerActions::class);

        $idxs = $actions->getIdxsFromIdnames(
            [
                'idx1' => 'val1',
                'idx2' => 'val2'
            ],
            [
                'someArg',
                1,
                ['array', 58],
                new StdClass(),
                function ($a) {
                    return 3 + 1 + $a;
                },
                new class () {
                },
                [
                    new class () {
                    }
                ],
                [
                    function ($a) {
                        return 3 + 1 + $a;
                    },
                    new class () {
                    }
                ]
            ]
        );

        $this->tester->assertIsArray($idxs);
        $this->tester->assertCount(2, $idxs);

        // [a-f0-9]{32} is regexp for md5 hash
        $this->tester->assertRegexp('#^idx1:val1-[a-f0-9]{32}$#', $idxs[0]);
        $this->tester->assertRegexp('#^idx2:val2-[a-f0-9]{32}$#', $idxs[1]);
    }

    public function testGetIdxsFromIdnamesError()
    {
        $actions = $this->make(ContainerActions::class);

        /**
         * max level of which getIdxsFromIdnames is able to "serialize" closures and anonymous classes
         *     if it is passed in array args
         *     (will recursively try to "serialize" each array element separately)
         */
        $this->tester->assertSame(1, ContainerActions::IDXFROMNAMES_MAX_DEEP_LEVEL);

        $this->tester->expectThrowable(
            new Exception(
                'Serialization of \'Closure\' is not allowed'
            ),
            function () use ($actions) {
                $actions->getIdxsFromIdnames(
                    [
                        'idx1' => 'val1',
                        'idx2' => 'val2'
                    ],
                    [
                        'someArg',
                        1,
                        ['array', 58],
                        new StdClass(),
                        [
                            [
                                function ($a) {
                                    return 3 + 1 + $a;
                                }
                            ],
                            new class () {
                            }
                        ]
                    ]
                );
            }
        );
    }

    /**
     * @dataProvider getTestGetServiceIdsFromSlimIdData
     */
    public function testGetServiceIdsFromSlimId($funcArg, $methodNameExpected, $classNameExpected)
    {
        $actions = $this->make(ContainerActions::class);

        $idname = $actions->getServiceIdsFromSlimId($funcArg);

        $this->tester->assertIsArray($idname);

        $this->tester->assertArrayHasKey('method', $idname);
        $this->tester->assertArrayHasKey('class', $idname);

        $this->tester->assertSame($methodNameExpected, $idname['method']);
        $this->tester->assertSame($classNameExpected, $idname['class']);
    }

    public function testReflectionAndSetContainer()
    {
        $reflection = new ReflectionClass(ContainerActions::class);

        $this->tester->assertFalse(
            $reflection->isAbstract()
        );

        // test constructor
        $constructor = $reflection->getConstructor();

        $this->tester->assertSame(
            null,
            $constructor
        );

        // test method setContainer
        $this->tester->assertTrue(
            $reflection->hasMethod('setContainer')
        );
        $this->tester->assertCount(
            11,
            $reflection->getMethods()
        );
        $method = $reflection->getMethod('setContainer');
        $this->tester->assertSame(
            1,
            $method->getNumberOfParameters()
        );

        $params = $method->getParameters();
        $this->tester->assertSame(
            'SlimApiEssentials\App\Di\Container',
            $params[0]->getType()->__toString()
        );

        $this->tester->assertSame(
            'void',
            $method->getReturnType()->__toString()
        );
        $this->tester->assertFalse(
            $method->isAbstract()
        );

        // test property $container
        $this->tester->assertTrue(
            $reflection->hasProperty('container')
        );
        $this->tester->assertCount(
            1,
            $reflection->getProperties()
        );
        $property = $reflection->getProperty('container');
        $this->tester->assertSame(
            'SlimApiEssentials\App\Di\Container',
            $property->getType()->__toString()
        );
    }

    public function getTestGetServiceIdsFromSlimIdData()
    {
        return [
            [
                'SOmeName',
                'somename',
                'SOmeName'
            ],
            [
                '\Some\Class\NAME',
                'someclassname',
                'Some\Class\NAME'
            ],
            [
                'lowercased\everything\AndClass',
                'lowercasedeverythingandclass',
                'lowercased\everything\AndClass'
            ]
        ];
    }
}
