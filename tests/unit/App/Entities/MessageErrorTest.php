<?php

declare(strict_types=1);

namespace SlimApiEssentials\Tests\App\Entities;

use Codeception\Test\Unit;
use SlimApiEssentials\App\Entities\MessageError;
use LogicException;

class MessageErrorTest extends Unit
{
    /**
     * @var \Tests\UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    public function testMessageError()
    {
        $this->tester->expectThrowable(
            LogicException::class,
            function () {
                // undefined required param
                new MessageError([]);
            }
        );

        // create fresh object to test
        $object = new MessageError(
            [
                'error' => 'someerror'
            ]
        );

        // test get all params
        $this->tester->assertSame(
            [
                'error' => 'someerror',
                'debug' => null
            ],
            $object->getParameters()
        );
        // test json
        $this->tester->assertJsonStringEqualsJsonString(
            '{"error":"someerror","debug":null}',
            json_encode($object)
        );

        $object = new MessageError(
            [
                'error' => 'someerror',
                'error_description' => 'description',
                'debug' => ['anything']
            ]
        );

        // test get all params
        $this->tester->assertSame(
            [
                'error' => 'someerror',
                'error_description' => 'description',
                'debug' => ['anything']
            ],
            $object->getParameters()
        );
        // test json
        $this->tester->assertJsonStringEqualsJsonString(
            '{"error":"someerror","error_description":"description","debug":["anything"]}',
            json_encode($object)
        );
    }
}
