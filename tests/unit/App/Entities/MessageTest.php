<?php

declare(strict_types=1);

namespace SlimApiEssentials\Tests\App\Entities;

use Codeception\Test\Unit;
use SlimApiEssentials\App\Entities\Message;
use LogicException;

class MessageTest extends Unit
{
    /**
     * @var \Tests\UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    public function testMessage()
    {
        $this->tester->expectThrowable(
            LogicException::class,
            function () {
                // undefined required param
                new Message([]);
            }
        );

        // create fresh object to test
        $object = new Message(
            [
                'message' => 'somemessage'
            ]
        );

        // test get all params
        $this->tester->assertSame(
            [
                'message' => 'somemessage',
                'info' => null
            ],
            $object->getParameters()
        );
        // test json
        $this->tester->assertJsonStringEqualsJsonString(
            '{"message":"somemessage","info":null}',
            json_encode($object)
        );

        $object = new Message(
            [
                'message' => 'somemessage',
                'code' => 'code',
                'info' => ['anything']
            ]
        );

        // test get all params
        $this->tester->assertSame(
            [
                'code' => 'code',
                'message' => 'somemessage',
                'info' => ['anything']
            ],
            $object->getParameters()
        );
        // test json
        $this->tester->assertJsonStringEqualsJsonString(
            '{"code":"code","message":"somemessage","info":["anything"]}',
            json_encode($object)
        );
    }
}
