<?php

declare(strict_types=1);

namespace SlimApiEssentials\Tests\App\Entities;

use Codeception\Test\Unit;
use SlimApiEssentials\App\Entities\AbstractMessage;
use SlimApiEssentials\App\Entities\AbstractBaseMessage;
use JsonSerializable;
use LogicException;
use StdClass;
use ArrayObject;

class AbstractMessageTest extends Unit
{
    /**
     * @var \Tests\UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    public function testCreateMessageWithAllTypes()
    {
        // define class
        $anyclassname = "\\SlimApiEssentials\\Tests\\Data\\App\\Entities\\AnyClass";
        $classname = "\\SlimApiEssentials\\Tests\\Data\\App\\Entities\\DummyMessageWithAllTypes";
        // require dummy class file to test
        require_once codecept_data_dir('unit/App/Entities/DummyMessage.php');
        require_once codecept_data_dir('unit/App/Entities/DummyMessageWithAllTypes.php');
        // set params
        $params = [
            'stdclass' => new StdClass(),
            'anyclass' => new $anyclassname(),
            'traversable' => new ArrayObject(),
            'self' => new $classname([]),
            'parent' => new $classname([]),
            'array' => ['vallue21', 'value22'],
            'bool' => false,
            'float' => 1.123,
            'int' => 3,
            'string' => 'vallue1',
            'iterable' => [],
            'object' => new StdClass(),
            'mixed' => ['anything'],
        ];

        // test errors
        $this->tester->expectThrowable(
            LogicException::class,
            function () use ($classname) {
                new $classname(['undefinedclassProperty' => 'somevalue']);
            }
        );
        $this->tester->expectThrowable(
            LogicException::class,
            function () use ($classname, $params) {
                new $classname($params, ['stdclass', 'undefined_required_param']);
            }
        );

        // create fresh object to test
        $object = new $classname($params);

        // test instance type
        $this->tester->assertInstanceOf(
            AbstractBaseMessage::class,
            $object
        );
        $this->tester->assertInstanceOf(
            AbstractMessage::class,
            $object
        );
        $this->tester->assertInstanceOf(
            JsonSerializable::class,
            $object
        );

        // test var params
        foreach ($params as $key => $value) {
            $this->tester->assertSame(
                $value,
                $object->$key
            );
        }
        // test get all params
        $this->tester->assertSame(
            $params,
            $object->getParameters()
        );
        // test json
        $this->tester->assertJsonStringEqualsJsonString(
            json_encode($params),
            json_encode($object)
        );
    }

    public function testCreateMessage()
    {
        // define class
        $classname = "\\SlimApiEssentials\\Tests\\Data\\App\\Entities\\DummyMessage";
        // require dummy class file to test
        require_once codecept_data_dir('unit/App/Entities/DummyMessage.php');
        // set params
        $params = [
            'var1' => 'vallue1',
            'var2' => ['vallue21', 'value22'],
            'var3' => 3,
        ];

        // test errors
        $this->tester->expectThrowable(
            LogicException::class,
            function () use ($classname) {
                new $classname(['undefinedclassProperty' => 'somevalue']);
            }
        );
        $this->tester->expectThrowable(
            LogicException::class,
            function () use ($classname, $params) {
                new $classname($params, ['var1', 'undefined_required_param']);
            }
        );

        // create fresh object to test
        $object = new $classname($params);

        // test instance type
        $this->tester->assertInstanceOf(
            AbstractBaseMessage::class,
            $object
        );
        $this->tester->assertInstanceOf(
            AbstractMessage::class,
            $object
        );
        $this->tester->assertInstanceOf(
            JsonSerializable::class,
            $object
        );

        // test var params
        foreach ($params as $key => $value) {
            $this->tester->assertSame(
                $value,
                $object->$key
            );
        }
        // test get all params
        $this->tester->assertSame(
            $params,
            $object->getParameters()
        );
        // test json
        $this->tester->assertJsonStringEqualsJsonString(
            json_encode($params),
            json_encode($object)
        );
    }
}
