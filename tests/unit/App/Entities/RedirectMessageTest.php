<?php

declare(strict_types=1);

namespace SlimApiEssentials\Tests\App\Entities;

use Codeception\Test\Unit;
use SlimApiEssentials\App\Entities\RedirectMessage;
use LogicException;

class RedirectMessageTest extends Unit
{
    /**
     * @var \Tests\UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    public function testMessage()
    {
        $this->tester->expectThrowable(
            LogicException::class,
            function () {
                // undefined required param
                new RedirectMessage([]);
            }
        );

        // create fresh object to test
        $object = new RedirectMessage(
            [
                'location' => 'somemessage'
            ]
        );

        // test get all params
        $this->tester->assertSame(
            [
                'redirect' => true,
                'location' => 'somemessage'
            ],
            $object->getParameters()
        );
        // test json
        $this->tester->assertJsonStringEqualsJsonString(
            '{"redirect": true, "location":"somemessage"}',
            json_encode($object)
        );
    }
}
