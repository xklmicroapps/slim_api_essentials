<?php

declare(strict_types=1);

namespace SlimApiEssentials\Tests\App\Entities;

use Codeception\Test\Unit;
use SlimApiEssentials\App\Entities\MessageIterator;
use SlimApiEssentials\App\Entities\AbstractBaseMessage;
use StdClass;
use Iterator;
use Countable;
use SlimApiEssentials\Tests\Data\App\Entities\DummyMessage;

class MessageIteratorTest extends Unit
{
    /**
     * @var \Tests\UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    public function testMessage()
    {
        // require dummy class file to test
        require_once codecept_data_dir('unit/App/Entities/DummyMessage.php');

        // create fresh object to test
        $object = new MessageIterator();

        $this->tester->assertInstanceOf(AbstractBaseMessage::class, $object);
        $this->tester->assertInstanceOf(Iterator::class, $object);
        $this->tester->assertInstanceOf(Countable::class, $object);

        $object->add(new DummyMessage(['var1' => 'val11']));
        $object->add(new DummyMessage(['var1' => 'val12']));
        $object->add(new DummyMessage(['var1' => 'val13']));

        $this->tester->assertCount(3, $object);

        // test json
        $this->tester->assertJsonStringEqualsJsonString(
            '[{"var1":"val11"},{"var1":"val12"},{"var1":"val13"}]',
            json_encode($object)
        );
    }
}
