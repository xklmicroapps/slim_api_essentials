<?php

declare(strict_types=1);

namespace SlimApiEssentials\Tests\App\Entities;

use Codeception\Test\Unit;
use SlimApiEssentials\App\Entities\Oauth2JwtToken;
use LogicException;

class Oauth2JwtTokenTest extends Unit
{
    /**
     * @var \Tests\UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    public function testMessage()
    {
        $this->tester->expectThrowable(
            LogicException::class,
            function () {
                // undefined required param
                new Oauth2JwtToken([]);
            }
        );

        // create fresh object to test
        $object = new Oauth2JwtToken(
            [
                'expires_in' => 100,
                'valid_in' => 60,
                'access_token' => 'sometoken'
            ]
        );

        // test get all params
        $this->tester->assertSame(
            [
                'expires_in' => 100,
                'valid_in' => 60,
                'access_token' => 'sometoken',
                'token_type' => 'Bearer'
            ],
            $object->getParameters()
        );
        // test json
        $this->tester->assertJsonStringEqualsJsonString(
            '{"expires_in":100,"valid_in":60,"access_token":"sometoken","token_type":"Bearer"}',
            json_encode($object)
        );
    }
}
