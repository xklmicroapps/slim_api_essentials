<?php

/**
 * 1) !!! CHANGE WORKING DIRECTORY to bootstrapFile directory TO MAKE IT WORK !!!
 *
 * 2) !!! BOOTSTRAP FILE INCLUDE will CHANGE ERROR HANDLERS !!!
 *     (so do not expect codeception errors if something goes wrong)
 */

declare(strict_types=1);

namespace SlimApiEssentials\Tests\functional;

use SlimApiEssentials\Tests\FunctionalTester;
use SlimApiEssentials\App\Bootstrap\Bootstrap;

// init trester
$tester = new FunctionalTester($scenario);
$tester->wantTo("Test general bootstrap file inclusion");

// assert bootstrap file exists on expected place
$bootstrapFile = codecept_root_dir('src/App/bootstrap.php');
$tester->assertFileExists($bootstrapFile);

// working directory change
chdir(dirname($bootstrapFile));

// bootstrap file include
// and assert return value
$tester->assertInstanceOf(
    Bootstrap::class,
    require $bootstrapFile
);
