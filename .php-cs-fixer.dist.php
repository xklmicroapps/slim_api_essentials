<?php

$finder = PhpCsFixer\Finder::create()
    ->in('src')
    ->in('tests')
    ->notPath('#(^|tests/)_.+(/|$)#') // Ignore path start with underscore (_).
    ;

$config = new PhpCsFixer\Config();
return $config->setRules([
    "declare_strict_types" => true,
    "linebreak_after_opening_tag" => true,
    "single_blank_line_before_namespace" => true,
    '@PSR12' => true
])
->setRiskyAllowed(true)
->setCacheFile(__DIR__ . '/.cache/.php_cs.cache')
->setFinder($finder);
