<?php

declare(strict_types=1);

namespace SlimApiEssentials\App\Di;

use PDO;
use GuzzleHttp\Client as GuzzleClient;
use SlimApiEssentials\App\Services\Auth\Apikey as AuthApikey;
use SlimApiEssentials\App\Services\Auth\Basic as AuthBasic;
use SlimApiEssentials\App\Services\Auth\Oauth2 as AuthOauth2;
use SlimApiEssentials\App\Services\Auth\Remote as AuthRemote;
use SlimApiEssentials\App\Services\Auth\Session as AuthSession;
use SlimApiEssentials\App\Services\Auth\SessionActions as AuthSessionActions;
use SlimApiEssentials\App\Services\Auth\SessionSerialize as AuthSessionSerialize;
use SlimApiEssentials\App\Services\Auth\AuthActions;
use SlimApiEssentials\App\Services\Auth\Oauth2TokenManager;
use SlimApiEssentials\App\Services\Helpers\Common as CommonHelper;
use SlimApiEssentials\App\Services\Helpers\Security as SecurityHelper;
use SlimApiEssentials\App\Services\RequestHelper;
use SlimApiEssentials\App\Services\ResponseHelper;

/**
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class Services extends AbstractServices
{
    public function getSlimApiEssentialsAppServicesAuthSession(): AuthSession
    {
        return new AuthSession(
            array_filter(
                $this->container->getConfigParameter('authSession'),
                function ($key) {
                    return !in_array($key, ['userResolver'], true);
                },
                ARRAY_FILTER_USE_KEY
            ),
            $this->container->get(AuthSessionActions::class),
            $this->container->get(
                $this->container->getConfigParameter('authSession')['userResolver']
            ),
            $this->container->getApp()->getRouteCollector()->getRouteParser(),
            $this->container->getApp()->getResponseFactory(),
            $this->container->get(ResponseHelper::class),
            $this->container->get(AuthSessionSerialize::class)
        );
    }

    public function getSlimApiEssentialsAppServicesAuthSessionActions(): AuthSessionActions
    {
        return new AuthSessionActions(
            $this->container->get(RequestHelper::class),
            $this->container->get(ResponseHelper::class),
            $this->container->get(
                $this->container->getConfigParameter('authSession')['userResolver']
            ),
            $this->container->getApp()->getResponseFactory()
        );
    }

    public function getSlimApiEssentialsAppServicesAuthApikey(): AuthApikey
    {
        return new AuthApikey(
            $this->container->get(
                $this->container->getConfigParameter('authApikey')['userResolver']
            ),
            $this->container->get(AuthActions::class)
        );
    }

    public function getSlimApiEssentialsAppServicesAuthBasic(): AuthBasic
    {
        return new AuthBasic(
            $this->container->get(
                $this->container->getConfigParameter('authBasic')['userResolver']
            ),
            $this->container->get(AuthActions::class)
        );
    }

    public function getSlimApiEssentialsAppServicesAuthOauth2(): AuthOauth2
    {
        return new AuthOauth2(
            $this->container->get(
                $this->container->getConfigParameter('authOauth2')['userResolver']
            ),
            $this->container->get(AuthActions::class),
            $this->container->get(Oauth2TokenManager::class)
        );
    }

    public function getSlimApiEssentialsAppServicesAuthRemote(): AuthRemote
    {
        return new AuthRemote(
            $this->container->getConfigParameter('authRemote')['url'],
            $this->container->getConfigParameter('authRemote')['certVerify'],
            $this->container->get(AuthActions::class),
            $this->container->get(GuzzleClient::class)
        );
    }

    public function getSlimApiEssentialsAppServicesAuthOauth2TokenManager(): Oauth2TokenManager
    {
        return new Oauth2TokenManager(
            $this->container->get(SecurityHelper::class),
            $this->container->get(CommonHelper::class)
        );
    }

    public function getSlimApiEssentialsAppServicesHelpersSecurity(): SecurityHelper
    {
        return new SecurityHelper(
            $this->container->get(CommonHelper::class),
            $this->container->getConfigParameter('security')['secret'],
            $this->container->getConfigParameter('security')['iv']
        );
    }

    /**
     * [getPdo description]
     * @return PDO           [description]
     */
    public function getPDO(): PDO
    {
        $pdoConfig = $this->container->getConfigParameter('pdo');

        return new PDO(
            $pdoConfig['dsn'] ?? '',
            $pdoConfig['username'] ?? null,
            $pdoConfig['password'] ?? null,
            array_replace(
                [
                    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
                    PDO::ATTR_EMULATE_PREPARES => false,
                    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                ],
                $pdoConfig['options'] ?? [],
            )
        );
    }
}
