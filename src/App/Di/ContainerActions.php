<?php

declare(strict_types=1);

namespace SlimApiEssentials\App\Di;

use ReflectionClass;
use ReflectionParameter;
use ReflectionMethod;
use ReflectionUnionType;
use ReflectionIntersectionType;
use LogicException;

class ContainerActions
{
    const IDXFROMNAMES_MAX_DEEP_LEVEL = 1;

    private Container $container;

    public function setContainer(Container $container): void
    {
        $this->container = $container;
    }
    /**
     * [getServiceIdsFromSlimId description]
     * @param  string $idname id as came from Slim call
     * @return array           [
     *                             'method' => (string) // Container::get{'methodname'}()
     *                                                  //  (namespaces included Some\Service will generate 'someservice' method name)
     *                             'class' => (string) // the whole class name with namespaces included
     *                         ]
     */
    public function getServiceIdsFromSlimId(string $idname): array
    {
        /**
         * in case of controllers Slims auto call we get all classname with namespaces
         *     so try to search backslash as an namespace delimiter
         *
         * !!! php class names are case insensitive BUT COMPOSER AUTOLOADER IS CASE SENSITIVE !!!
         * !!! php functions/methods are case insensitive !!!
         */
        return [
            'class' => ltrim($idname, "\\"),
            'method' => strtolower(
                str_replace("\\", '', $idname)
            )
        ];
    }

    /**
     * [getServiceFromIdNameByMethod description]
     * @param  string $idname [description]
     * @param  array  $args   [description]
     * @return mixed         [description]
     */
    public function getServiceFromIdNameByMethod(string $idname, array $args): mixed
    {
        /**
         * !!! php functions/methods are case insensitive !!!
         */
        foreach ($this->container->getSubContainers() as $subContainer) {
            if (method_exists($subContainer, "get" . $idname)) {
                return $subContainer->{"get" . $idname}(...$args);
            }
        }
        if (method_exists($this->container, "get" . $idname)) {
            return $this->container->{"get" . $idname}(...$args);
        }
        return null;
    }

    /**
     * [getServiceFromIdName description]
     * @param  string $idname [description]
     * @param  array  $args   [description]
     * @return mixed         based on method called in container or a class definition as a service
     */
    public function getServiceFromIdNameByClass(string $idname, array $args): mixed
    {
        if ($idname === $this->container::class) {
            return $this->container;
        }
        /**
         * !!! php class names are case insensitive BUT COMPOSER AUTOLOADER IS CASE SENSITIVE !!!
         */
        $className = $idname;
        if (class_exists($idname)) {
            return $this->getServiceFromClassName($className, $args);
        }
        return null;
    }

    private function getServiceFromClassName(string $className, array $args): object
    {
        $constructor = (new ReflectionClass($className))->getConstructor();
        if (!$constructor) {
            return new $className();
        }
        $argsValues = $args;
        $i = 0;
        foreach ($constructor->getParameters() as $param) {
            $idx = $this->isArgsAssoc($args) ? $param->getName() : $i;
            if (isset($argsValues[$idx])) {
                $i++;
                continue;
            }

            $autoWireClassName = $this->getParamAutoWireClassName($param);
            if ($this->isClassToBeAutowired($autoWireClassName, $param)) {
                $argsValues[$idx] = $this->container->get(
                    $autoWireClassName
                );
                $i++;
                continue;
            }

            if ($param->isOptional()) {
                $argsValues[$idx] = $param->getDefaultValue();
            } elseif ($param->allowsNull()) {
                $argsValues[$idx] = null;
            }
            $i++;
        }
        return new $className(...$argsValues);
    }

    private function isClassToBeAutowired(
        string|null $autoWireClassName,
        ReflectionParameter $param
    ) {
        if (!$autoWireClassName) {
            return false;
        }

        /**
         * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
         *
         * do not autowire optional params
         *
         * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
         */
        if ($param->isOptional()) {
            return false;
        }

        $this->checkAutowireInterface($autoWireClassName);

        return true;
    }

    private function isArgsAssoc(array $args)
    {
        return $args && array_keys($args) !== range(0, count($args) - 1);
    }

    public function checkAutowireInterface(string $autoWireClassName)
    {
        /**
         * it is not possible to autowire class by interface name
         */
        if (interface_exists($autoWireClassName, false)) {
            // $argsValues[$idx] = null;
            throw new LogicException(
                "Interface can not be autowired"
                 . " ($autoWireClassName)"
            );
        }
    }


    /**
     * [getIdxsFromIdnames description]
     * @param  array  $idnames [description]
     * @param  array  $args    [description]
     * @return array          [description]
     */
    public function getIdxsFromIdnames(array $idnames, array $args): array
    {
        $idxs = [];
        $argsIdxs = $this->getArgsIdxs($args);
        foreach ($idnames as $key => $val) {
            $idxs[] = $key . ':' . $val . '-' . md5(implode(',', $argsIdxs));
        }
        return $idxs;
    }

    private function getArgsIdxs(array $args, int $level = 0): array
    {
        $argsIdxs = [];
        foreach ($args as $key => $arg) {
            if (is_array($arg) && $level < self::IDXFROMNAMES_MAX_DEEP_LEVEL) {
                foreach ($this->getArgsIdxs($arg, ++$level) as $argsIdx) {
                    $argsIdxs[] = $argsIdx;
                }
                $level = 0;
                continue;
            }
            $argsIdxs[] = $key . ':' . (is_object($arg) ? spl_object_hash($arg) : serialize($arg));
        }
        return $argsIdxs;
    }

    /**
     * [getParamAutoWireClassName description]
     * @param  ReflectionParameter $param [description]
     * @return string|null                     return null for isBuiltin type or unknown
     * @throws LogicException for $param->getType() to be ReflectionUnionType (multiple types declaration)
     */
    private function getParamAutoWireClassName(ReflectionParameter $param): ?string
    {
        $type = $param->getType();
        if (!$type) {
            return null;
        }
        if ($type instanceof ReflectionUnionType || $type instanceof ReflectionIntersectionType) {
            throw new LogicException(
                "Constructor parameter"
                . "\"" . $param->getName() . "\""
                . " MUST NOT be declared as \"" . get_class($type) . "\""
                . " (for multiple types) to be auto instantiable!"
            );
        }
        if (!$type->isBuiltin()) {
            return $type->getName();
        }
        return null;
    }
}
