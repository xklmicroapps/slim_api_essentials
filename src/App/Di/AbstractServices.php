<?php

declare(strict_types=1);

namespace SlimApiEssentials\App\Di;

abstract class AbstractServices
{
    protected Container $container;

    public function setContainer(Container $container): void
    {
        $this->container = $container;
    }
}
