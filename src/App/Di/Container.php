<?php

declare(strict_types=1);

namespace SlimApiEssentials\App\Di;

use LogicException;
use Psr\Container\ContainerInterface;
use Slim\App;
use SlimApiEssentials\App\Exceptions\ServiceNotFoundException;
use RuntimeException;
use InvalidArgumentException;
use Slim\Middleware\RoutingMiddleware;
use SlimApiEssentials\App\Bootstrap\Actions as BootstrapActions;

/**
 * very simple container for DI
 */
class Container implements ContainerInterface
{
    private array $configParameters = [];

    private array $services = [];

    private App $app;

    private array $subContainers = [];

    private array $appMiddlewares = [];

    public function __construct(private ContainerActions $actions)
    {
        $this->actions->setContainer($this);
    }

    public function setAppMiddlewares(array $appMiddlewares)
    {
        $this->appMiddlewares = $appMiddlewares;
    }

    public function getAppMiddlewares()
    {
        if (!$this->appMiddlewares) {
            throw new LogicException(
                "this->appMiddlewares not set!"
                    . " Must be set before by "
                    . BootstrapActions::class
                    . "::setAppMiddlewares()"
            );
        }

        return $this->appMiddlewares;
    }

    public function getRoutingMiddleware()
    {
        foreach ($this->getAppMiddlewares() as $middleware) {
            if ($middleware instanceof RoutingMiddleware) {
                return $middleware;
            }
        }

        throw new LogicException(
            RoutingMiddleware::class . " not found!"
                . " Must be set before by "
                . BootstrapActions::class
                . "::setAppMiddlewares()"
        );
    }

    /**
     * set "sub containers" to allow custom methods
     *     for custom way to get services
     *
     * @param AbstractServices[] $subContainers [description]
     */
    public function setSubContainers(array $subContainers)
    {
        foreach ($subContainers as $subContainer) {
            if (!($subContainer instanceof AbstractServices)) {
                throw new InvalidArgumentException(
                    "subcontainer must be instance of "
                    . AbstractServices::class
                    . " but instance of "
                    . get_class($subContainer)
                    . ' given'
                );
            }
            $subContainer->setContainer($this);
            $this->subContainers[] = $subContainer;
        }
    }

    /**
     * [getSubContainer description]
     * @return AbstractServices[] [description]
     */
    public function getSubContainers(): array
    {
        return $this->subContainers;
    }

    /**
     * have to set app before using services that are using app
     * @param App $app [description]
     */
    public function setApp(App $app)
    {
        $this->app = $app;
    }

    /**
     * [getApp description]
     * @return App [description]
     */
    public function getApp(): App
    {
        return $this->app;
    }

    /**
     * alias for $this->getApp
     * @return App [description]
     */
    public function getSlimApp(): App
    {
        return $this->getApp();
    }

    public function getConfigParameter(string $name, mixed $default = null): mixed
    {
        $params = $this->getConfigParameters();
        if (array_key_exists($name, $params)) {
            return $params[$name];
        }
        return $default;
    }

    /**
     * get config parameters
     * @return array [description]
     */
    public function getConfigParameters(): array
    {
        return $this->configParameters;
    }

    /**
     * set config parameters AS IS!!!
     * (will not retype any config param value)
     * (!!!proper value types has to be defined in config itself!!!)
     *
     * @param array $params [description]
     */
    public function setConfigParameters(array $params)
    {
        if ($this->configParameters) {
            throw new RuntimeException('ConfigParameters already set!');
        }

        $this->configParameters = $params;
    }

    /**
     * set instance by $idname and optional $args (@see $this->get())
     *
     * @param string $idname Identifier of the entry to look for.
     * @param mixed  $service service passed to container
     * @param mixed $args optional getService... methods arguments
     *
     * @return void
     */
    public function set(string $idname, mixed $service, ...$args): void
    {
        $idnames = $this->actions->getServiceIdsFromSlimId($idname);
        $idxs = $this->actions->getIdxsFromIdnames($idnames, $args);

        foreach ($idxs as $idx) {
            $this->services[$idx] = $service;
        }
    }

    /**
     * Finds an entry of the container by its identifier and returns it.
     *
     * @param string $idname Identifier of the entry to look for.
     * @param mixed $args optional getService... methods arguments
     *                            can be associative array for the "named parameters" use
     *
     * @throws ServiceNotFoundException  No entry was found for **this** identifier.
     * @throws RuntimeException Error while retrieving the entry.
     *
     * @return mixed Entry.
     */
    public function get(string $idname, ...$args)
    {
        $idnames = $this->actions->getServiceIdsFromSlimId($idname);
        /**
         * get each service one instance
         *     (depends on $idnames and $args defined)
         */
        $idxs = $this->actions->getIdxsFromIdnames($idnames, $args);

        foreach ($idxs as $idx) {
            if (isset($this->services[$idx])) {
                return $this->services[$idx];
            }
        }

        if (!$this->has($idname)) {
            throw new ServiceNotFoundException("'$idname' not found!");
        }

        $service = $this->actions->getServiceFromIdNameByMethod($idnames['method'], $args);
        if ($service === null) {
            $service = $this->actions->getServiceFromIdNameByClass($idnames['class'], $args);
        }

        if ($service === null) {
            throw new RuntimeException("Can't initialize service from idname:'$idname'!");
        }

        /**
         * fill services stack
         */
        foreach ($idxs as $idx) {
            $this->services[$idx] = $service;
        }
        /**
         * add auto alias for object service
         */
        if (is_object($service)) {
            /**
             * get each service one instance
             *     (depends on $idnames and $args defined)
             */
            $idxs = $this->actions->getIdxsFromIdnames(
                $this->actions->getServiceIdsFromSlimId(
                    get_class($service)
                ),
                $args
            );
            foreach ($idxs as $idx) {
                $this->services[$idx] = $service;
            }
        }

        return $service;
    }

    /**
     * Returns true if the container can return an entry for the given identifier.
     * Returns false otherwise.
     *
     * `has($idname)` returning true does not mean that `get($idname)` will not throw an exception.
     * It does however mean that `get($idname)` will not throw a `NotFoundExceptionInterface`.
     *
     * @param string $idname Identifier of the entry to look for.
     *
     * @return bool
     */
    public function has(string $idname): bool
    {
        $idnames = $this->actions->getServiceIdsFromSlimId($idname);

        foreach ($this->getSubContainers() as $subContainer) {
            if (method_exists($subContainer, "get" . ucfirst($idnames['method']))) {
                return true;
            }
        }
        if (method_exists($this, "get" . ucfirst($idnames['method']))) {
            return true;
        }
        return class_exists($idnames['class']);
    }
}
