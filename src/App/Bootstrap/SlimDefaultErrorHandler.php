<?php

declare(strict_types=1);

namespace SlimApiEssentials\App\Bootstrap;

use Throwable;
use SlimApiEssentials\App\Services\ResponseHelper;
use SlimApiEssentials\App\Services\RequestHelper;
use SlimApiEssentials\App\Exceptions\AuthOauth2Exception;
use SlimApiEssentials\App\Exceptions\AuthBearerException;
use SlimApiEssentials\App\Exceptions\AuthSessionException;
use SlimApiEssentials\App\Exceptions\AuthException;
use Slim\App;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;

class SlimDefaultErrorHandler
{
    public function __construct(
        private App $app,
        private Functions $functions
    ) {
    }

    public function __invoke(
        ServerRequestInterface $request,
        Throwable $exception,
        bool $displayErrorDetails,
        bool $logErrors,
        bool $logErrorDetails
    ) {
        if (
            in_array(
                $this->functions->getExceptionHttpStatus($exception),
                [401, 403],
                true
            )
        ) {
            // keep client wait for a little time if UNAUTHORIZED
            usleep(
                $this->app->getContainer()->getConfigParameter(
                    'unauthSleepMiliseconds',
                    0
                ) * 1000
            );
        }

        if ($logErrors || $logErrorDetails) {
            /**
             * @todo mabye get some logger service and log something similar to the $paylod above
             */
        }

        $this->setExceptionAuthLocation($exception, $request);

        return $this->getResponse($request, $exception, $displayErrorDetails);
    }

    private function getResponse(
        ServerRequestInterface $request,
        Throwable $exception,
        bool $displayErrorDetails
    ) {
        $responseHelper = $this->app->getContainer()->get(ResponseHelper::class);

        $responseEmpty = $this->app->getResponseFactory()->createResponse();
        $exceptionHttpStatus = $this->functions->getExceptionHttpStatus($exception);

        $requestHelper = $this->app->getContainer()->get(RequestHelper::class);

        if ($requestHelper->isAcceptHtml($request)) {
            return $this->getResponseWithAuthenticate(
                $exception,
                $responseHelper->getHtml(
                    $responseEmpty,
                    $this->functions->getErrorFromExceptionHtml(
                        $exception,
                        $displayErrorDetails
                    ),
                    $exceptionHttpStatus
                )
            );
        }

        return $this->getResponseWithAuthenticate(
            $exception,
            $responseHelper->getJsonFromText(
                $responseEmpty,
                $this->functions->getErrorFromExceptionJson(
                    $exception,
                    $displayErrorDetails
                ),
                $exceptionHttpStatus
            )
        );
    }

    private function setExceptionAuthLocation(
        Throwable $exception,
        ServerRequestInterface $request
    ) {
        if (
            $exception instanceof AuthOauth2Exception
            || $exception instanceof AuthBearerException
        ) {
            $exception->setAuthenticateLocation(
                $this->app->getRouteCollector()->getRouteParser()->fullUrlFor(
                    $request->getUri(),
                    $this->app->getContainer()->getConfigParameter(
                        'authOauth2'
                    )['routeName']
                )
            );
        } elseif (
            $exception instanceof AuthSessionException
        ) {
            $exception->setAuthenticateLocation(
                $this->app->getRouteCollector()->getRouteParser()->fullUrlFor(
                    $request->getUri(),
                    $this->app->getContainer()->getConfigParameter(
                        'authSession'
                    )['routeNameGet'],
                    $exception->getArgs(),
                    $request->getQueryParams()
                )
            );
        }
    }

    private function getResponseWithAuthenticate(
        Throwable $exception,
        ResponseInterface $response
    ) {
        if ($exception instanceof AuthException) {
            return $exception->getResponseWithAuthenticate(
                $response
            );
        }

        return $response;
    }
}
