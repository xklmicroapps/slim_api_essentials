<?php

declare(strict_types=1);

namespace SlimApiEssentials\App\Bootstrap;

use Throwable;
use Slim\App;
use Psr\Http\Message\ServerRequestInterface;
use SlimApiEssentials\App\Exceptions\AuthOauth2Exception;
use SlimApiEssentials\App\Services\Auth\Session as AuthSession;

class Functions
{
    public function getErrorFromExceptionJson(Throwable $exception, bool $displayErrorDetails): string
    {
        return json_encode(
            $this->getExceptionPayload($exception, $displayErrorDetails),
            JSON_THROW_ON_ERROR
            | JSON_UNESCAPED_UNICODE
            | JSON_UNESCAPED_SLASHES
        );
    }

    public function getErrorFromExceptionHtml(Throwable $exception, bool $displayErrorDetails): string
    {
        if ($displayErrorDetails) {
            return $this->getErrorFromExceptionHtmlDebug($exception);
        }

        return $this->getErrorFromExceptionHtmlProduction($exception);
    }

    public function getExceptionHttpStatus(Throwable $exception): int
    {
        $httpStatus = 500;
        $code = (int) $exception->getCode();
        if ($code > 99 && $code < 600) {
            $httpStatus = $code;
        }
        return $httpStatus;
    }

    public function getExceptionPayload(Throwable $exception, bool $displayErrorDetails): array
    {
        $payload = [
            'error' => $exception->getCode(),
            'error_description' => $exception->getMessage()
        ];
        /**
         * special requirements for AuthOauth2Exception output
         * @see  https://tools.ietf.org/html/rfc6749
         */
        if ($exception instanceof AuthOauth2Exception) {
            $payload = [
                'error' => $exception->getError(),
                'error_description' => $exception->getCode() . ': ' . $exception->getMessage()
            ];
        }

        if ($displayErrorDetails) {
            $payload['debug']['class'] = get_class($exception);
            if (method_exists($exception, 'getFile')) {
                $payload['debug']['file'] = $exception->getFile();
            }
            if (method_exists($exception, 'getLine')) {
                $payload['debug']['line'] = $exception->getLine();
            }
            if (method_exists($exception, 'getTrace')) {
                $payload['debug']['trace'] = $this->getExceptionPayloadTrace($exception);
            }
            if (method_exists($exception, 'getPrevious') && $exception->getPrevious()) {
                $payload['debug']['previous'] = $this->getExceptionPayload(
                    $exception->getPrevious(),
                    $displayErrorDetails
                );
            }
        }
        return $payload;
    }

    private function getExceptionPayloadTrace(Throwable $exception)
    {
        try {
            $trace = $exception->getTrace();
            /**
             * try to encode json
             *     ??can some traces contain objects so it is not possible to json_encode??
             */
            json_encode(
                $trace,
                JSON_THROW_ON_ERROR
                | JSON_UNESCAPED_UNICODE
                | JSON_UNESCAPED_SLASHES
            );
        } catch (Throwable $e) {
            $trace = '-- unresolvable --';
            if (method_exists($exception, 'getTraceAsString')) {
                $trace = $exception->getTraceAsString();
            }
        }
        return $trace;
    }

    private function getErrorFromExceptionHtmlProduction(Throwable $exception)
    {
        return $this->getHtmlError(
            $exception->getCode(),
            json_encode(
                $this->getExceptionPayload($exception, false),
                JSON_THROW_ON_ERROR
                | JSON_UNESCAPED_UNICODE
                | JSON_UNESCAPED_SLASHES
                | JSON_UNESCAPED_LINE_TERMINATORS
                | JSON_PRETTY_PRINT
            )
        );
    }

    private function getErrorFromExceptionHtmlDebug(Throwable $exception)
    {
        return $this->getHtmlError(
            $exception->getCode(),
            $this->getErrorFromExceptionMessage($exception, true),
            [
                'background: #fffedf'
            ],
            [
                'font-size: 1.2em',
                'padding-top: 0px',
                'padding-left: 20px'
            ]
        );
    }

    private function getErrorFromExceptionMessage(Throwable $exception, $displayErrorDetails)
    {
        return json_encode(
            $this->getExceptionPayload($exception, $displayErrorDetails),
            JSON_THROW_ON_ERROR
            | JSON_UNESCAPED_UNICODE
            | JSON_UNESCAPED_SLASHES
            | JSON_UNESCAPED_LINE_TERMINATORS
            | JSON_PRETTY_PRINT
        );
    }

    private function getHtmlError(
        int|string $code,
        string $message,
        array $addStyles = [],
        array $addStylesSub = []
    ) {
        $partsHtml = [];
        $partsHtml[] = '
                    </html>
                        <head>
                            <title>Error #' . $code . '</title>
                        </head>
                        <body>';
        /**
         * some pure javascript in case error is rendered in the <script></script> tag
         *     and html error render would cause javascript to fail without some proper info
         */
        if ($code !== 404) {
            $partsHtml[] = $this->getHtmlErrorJs($code, $message);
        }

        /**
         * render html error
         */
        $partsHtml[] = '
            <div
                class="h30-debug-wrap"
                style="
                    position: fixed;
                    top: 0;
                    left: 0;
                    z-index: 9222333;
                    background: #ffffff;
                    '
                    . implode('; ', $addStyles)
                    . '"
            >';
        $partsHtml[] = '
            <button
                type="button"
                class="btn btn-sm btn-danger"
                onclick="
                    let display = \'none\';
                    document.querySelectorAll(\'.h30-debug-wrap-sub\').forEach((item, key) => {
                        if (!key && item.style.display === \'none\') {
                            display = \'block\';
                        }
                        item.style.display = display;
                    });
                "
            >
                !!!
            </button>';
        $partsHtml[] = '
            <div
                class="h30-debug-wrap-sub"
                style="
                    width: 100vw;
                    height: 100vh;
                    padding: 133px;
                    color: #2e2e2e;
                    font-weight: bold;
                    font-size: 2.2em;
                    overflow: auto;
                    '
                    . implode('; ', $addStylesSub)
                    . '"
                "
            >';
        $partsHtml[] = '<pre>';

        $partsHtml[] =  $message;

        $partsHtml[] =  '</pre>';
        $partsHtml[] =  '</div>';
        $partsHtml[] =  '</div>';
        $partsHtml[] =  '
            <script type="text/javascript">
                document.querySelectorAll(\'.h30-debug-wrap\').forEach((item) => {
                    let el = document.querySelector(\'body\');
                    el.append(item);
                });
            </script>';

        $partsHtml[] = '
                            </body>
                        </html>';

        return implode("\n", $partsHtml);
    }

    private function getHtmlErrorJs(int|string $code, string $message)
    {
        $pureJs = [];
        $pureJs[] = '</script>">';
        $pureJs[] = '<script>';
        $pureJs[] = '(function() {';
        $pureJs[] = '   alert(\'Server Error... #' . strval($code) . '\');';
        $pureJs[] = '   console.error(\''
            . str_replace(
                "'",
                "\\'",
                json_encode(
                    ['message' => $message],
                    JSON_THROW_ON_ERROR
                    | JSON_UNESCAPED_UNICODE
                    | JSON_UNESCAPED_SLASHES
                )
            )
            . '\');';
        $pureJs[] = '})();';
        $pureJs[] = '</script>';

        return implode(' ', $pureJs);
    }
}
