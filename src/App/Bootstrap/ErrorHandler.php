<?php

declare(strict_types=1);

namespace SlimApiEssentials\App\Bootstrap;

use Throwable;
use ErrorException;

class ErrorHandler
{
    /**
     * [handleError description]
     * @param  int    $errNum  [description]
     * @param  string $message [description]
     * @param  string $file    [description]
     * @param  int    $line    [description]
     * @throws ErrorException [<description>]
     * @return [type]          [description]
     */
    public function handleError(int $errNum, string $message, string $file, int $line): void
    {
        throw new ErrorException($message, $errNum, $errNum, $file, $line);
    }

    /**
     * echo JSON string with error description
     *
     * @param  Throwable $exception [description]
     * @param  bool      $isDebug   [description]
     * @param  Functions $functions [description]
     * @return [type]               [description]
     */
    public function handleException(Throwable $exception, bool $isDebug, Functions $functions): void
    {
        /**
         * error details here is based only on env variable
         * because we should not parse other files like configs here
         */
        $displayErrorDetails = false;
        if ($isDebug) {
            $displayErrorDetails = true;
        }

        if ($this->isAcceptHtmlRaw()) {
            $this->handleExceptionHtml(
                $exception,
                $displayErrorDetails,
                $functions
            );
            return;
        }

        if ($this->isAcceptJsonRaw()) {
            $this->handleExceptionJson(
                $exception,
                $displayErrorDetails,
                $functions
            );
            return;
        }

        $this->handleExceptionGeneric(
            $exception,
            $displayErrorDetails,
            $functions
        );
    }

    private function handleExceptionGeneric(
        Throwable $exception,
        bool $displayErrorDetails,
        Functions $functions
    ) {
        /**
         * primary for api use
         */
        $this->handleExceptionJson(
            $exception,
            $displayErrorDetails,
            $functions
        );
    }

    private function handleExceptionHtml(
        Throwable $exception,
        bool $displayErrorDetails,
        Functions $functions
    ) {
        $httpStatus = $functions->getExceptionHttpStatus($exception);

        header('Content-Type: text/html');
        http_response_code($httpStatus);
        echo $functions->getErrorFromExceptionHtml(
            $exception,
            $displayErrorDetails
        );
    }

    private function handleExceptionJson(
        Throwable $exception,
        bool $displayErrorDetails,
        Functions $functions
    ) {
        $httpStatus = $functions->getExceptionHttpStatus($exception);

        header('Content-Type: application/json');
        http_response_code($httpStatus);
        echo $functions->getErrorFromExceptionJson(
            $exception,
            $displayErrorDetails
        );
    }

    /**
     * @SuppressWarnings(PHPMD.Superglobals)
     */
    private function isAcceptHtmlRaw()
    {
        if ($this->isAcceptJsonRaw()) {
            return false;
        }

        if (
            stripos(
                $_SERVER['Accept'] ?? '',
                'text/html'
            ) !== false
        ) {
            return true;
        }

        return stripos(
            $_SERVER['Accept'] ?? '',
            '*/*'
        ) !== false;
    }

    /**
     * @SuppressWarnings(PHPMD.Superglobals)
     */
    private function isAcceptJsonRaw()
    {
        return stripos(
            $_SERVER['Content-Type'] ?? '',
            'application/json'
        ) !== false
        || stripos(
            $_SERVER['Accept'] ?? '',
            'application/json'
        ) !== false;
    }
}
