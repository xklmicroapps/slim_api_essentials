<?php

declare(strict_types=1);

namespace SlimApiEssentials\App\Bootstrap;

use ErrorException;
use Throwable;
use Slim\App;
use SlimApiEssentials\App\Di\Container;
use SlimApiEssentials\App\Di\ContainerActions;
use SlimApiEssentials\App\Di\Services;
use SlimApiEssentials\App\Middlewares\ContentCheckMiddleware;
use SlimApiEssentials\App\Middlewares\JsonContentParsingMiddleware;
use InvalidArgumentException;
use Psr\Http\Server\MiddlewareInterface;

class Actions
{
    private $container;

    public function __construct(
        private Functions $functions,
        private ErrorHandler $errorHandler
    ) {
    }

    public function setAppMiddlewares(App $app)
    {
        $container = $app->getContainer();

        $appMiddlewares = [];
        $appMiddlewares[] = $this->addAppMiddlware(
            $app,
            $container->get(ContentCheckMiddleware::class)
        );

        /**
         * just position the routingmiddleware?
         *
         * The routing middleware should be added earlier than the ErrorMiddleware
         * Otherwise exceptions thrown from it will not be handled by the middleware
         */
        $appMiddlewares[] = $app->addRoutingMiddleware();
        /**
         * custom json body parser
         */
        $appMiddlewares[] = $this->addAppMiddlware(
            $app,
            $container->get(JsonContentParsingMiddleware::class)
        );

        /**
         *
         * @param bool $displayErrorDetails -> Should be set to false in production
         * @param bool $logErrors -> Parameter is passed to the default ErrorHandler
         * @param bool $logErrorDetails -> Display error details in error log
         * which can be replaced by a callable of your choice.
         *
         * Note: This middleware should be added last. It will not handle any exceptions/errors
         * for middleware added after it.
         *
         */
        $errorMiddleware = $app->addErrorMiddleware(
            $container->getConfigParameter('isDebug', false),
            true,
            true
        );
        $errorMiddleware->setDefaultErrorHandler(
            $container->get(
                SlimDefaultErrorHandler::class,
                $app,
                $this->functions
            )
        );
        $appMiddlewares[] = $errorMiddleware;

        $container->setAppMiddlewares(
            $appMiddlewares
        );
    }

    /**
     * Create DI Container
     */
    public function getContainer(array $configParams): Container
    {
        if (!$this->container) {
            $container = new Container(
                new ContainerActions()
            );
            /**
             * set configuration parameters by config file
             */
            $container->setConfigParameters($configParams);
            /**
             * get sub containers instances
             */
            $subContainers = [];
            $customSubContainers = $container->getConfigParameter('subContainers');
            if ($customSubContainers) {
                if (!is_array($customSubContainers)) {
                    throw new InvalidArgumentException(
                        "subContainers config param must be an array type"
                        . " but the type "
                        . gettype($customSubContainers)
                        . ' given'
                    );
                }
                foreach ($customSubContainers as $subContainerClass) {
                    $subContainers[] = $container->get($subContainerClass);
                }
            }
            /**
             * get project default subcontainer instance
             */
            $subContainers[] = $container->get(Services::class);
            /**
             * set subcontainers
             */
            $container->setSubContainers($subContainers);
            /**
             * set container
             */
            $this->container = $container;
        }
        return $this->container;
    }

    /**
     * setErrorHandler
     */
    public function setErrorHandler(bool $isDebug)
    {
        /**
         * set simple error handler for php errors - notices, warnings, ...
         * @throws ErrorException that is going to be caught by Slims error middleware
         *                         or exception handler and rendered
         */
        set_error_handler(function (int $errNum, string $message, string $file, int $line) {
            $this->errorHandler->handleError($errNum, $message, $file, $line);
        }, E_ALL);
        /**
         * simple exception handler for some fatal errors etc
         */
        set_exception_handler(function (Throwable $exception) use ($isDebug) {
            $this->errorHandler->handleException($exception, $isDebug, $this->functions);
        });
    }

    private function addAppMiddlware(App $app, MiddlewareInterface $middleware)
    {
        $app->add($middleware);
        return $middleware;
    }
}
