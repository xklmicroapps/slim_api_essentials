<?php

declare(strict_types=1);

namespace SlimApiEssentials\App\Bootstrap;

use Slim\Factory\AppFactory;
use Slim\App;

class Bootstrap
{
    private $app;

    public function __construct(
        private array $configParams,
        private Actions $actions
    ) {
    }

    /**
     * @SuppressWarnings(PHPMD.StaticAccess)
     */
    public function getApp(): App
    {
        if ($this->app) {
            return $this->app;
        }

        $container = $this->actions->getContainer($this->configParams);

        $this->actions->setErrorHandler(
            $container->getConfigParameter('isDebug', false)
        );

        // Set container to create App with an AppFactory
        AppFactory::setContainer($container);
        $app = AppFactory::create();
        /**
         * have to inject app into container ASAP
         *     to be accessible for services etc
         */
        $container->setApp($app);
        /**
         * set basepath if available for slim framework to properly parse paths
         */
        $app->setBasePath(
            $container->getConfigParameter('apiBasepath', '')
        );
        /**
         * set app middlewares
         */
        $this->actions->setAppMiddlewares($app);

        $this->app = $app;
        return $this->app;
    }
}
