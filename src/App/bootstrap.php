<?php

/**
 * this is SAMPLE bootstrap file
 * mabye there should be a need of setting proper $CWD (working directory) after include by chdir()
 * (chdir(__DIR__) can be enough)
 *
 * /////////////////////////////
 * /////////////////////////////
 * sample your app bootstraping:
 *
 * /www/index.php:
 * ####################
 * <?php
 *
 * // app init
 * $bootstrap = require __DIR__ . '/../App/bootstrap.php';
 *
 * // get app from bootstrap
 * $app = $bootstrap->getApp();
 *
 * // set your routes etc...
 * $app->getContainer()->get('router')->setRoutes($app);
 *
 * // run app
 * $app->run();
 *
 * /App/bootstrap.php:
 * ####################
 * <?php
 *
 * // your helper functions
 * require __DIR__ . '/functions.php';
 *
 * chdir(__DIR__);
 * return require __DIR__ . '/../../vendor/xklmicroapps/slim_api_essentials/src/App/bootstrap.php';
 *
 * /////////////////////////////
 * /////////////////////////////
 *
 * RECOMMENDED basic CONFIG file structure:
 * [
 *     // (string)
 *     // if value === 'json', then only application/json Content-Type is accepted
 *     //    (in SlimApiEssentials\App\Middlewares\ContentCheckMiddleware)
 *     'contentCheck' => 'json'
 *
 *     // (array)
 *     // define subcontainers classes to be able to define custom methods
 *     // !!! must be instances of SlimApiEssentials\App\Di\AbstractServices !!!
 *     //   (there is prop $this->container accesible as the "parent" orignal container)
 *     //   to get your custom services like $app->get('serviceName')
 *     //       (will call YourSubContainer::getServiceName)
 *     'subContainers' => [YourSubContainer::class, Another::class, ...]
 *
 *     // (string)
 *     // define api base path
 *     'apiBasepath' => ''
 *
 *     // (bool)
 *     // set debug mode on (or false or ommit to turn it off)
 *     'isDebug' => true
 *
 *     // (int)
 *     // set unauthorized request idle time to siply slow down malicious requests attempts
 *     'unauthSleepMiliseconds' => 3000
 *
 *     // (array)
 *     // set database configuration (pdo) if using database
 *     'pdo' => [
 *         'dsn' => 'pgsql:dbname=databasename;host=hostname;port=5432',
 *         'username' => 'optionalforsomepdodrivers',
 *         'password' => 'pass_id_optionalforsomepdodrivers_too',
 *         // optional PDO clas options (following are defaults, can be overriden if passed another array)
 *         'options' => [
 *             PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
 *             PDO::ATTR_EMULATE_PREPARES => false,
 *             PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
 *         ]
 *     ]
 *
 *     // (array)
 *     'security' => [
 *         // (string)
 *         // encrypt/decrypt secret
 *         'secret' => 'some random string long enough - you should change this to be safe'
 *         // (string)
 *         // encrypt/decrypt iv
 *         'iv' => 'another random string - you should change this to be safe'
 *     ]
 *
 *     // auth service is based on middlewares
 *     // - SlimApiEssentials\App\Middlewares\Auth\Oauth2Middleware
 *     // - SlimApiEssentials\App\Middlewares\Auth\BasicMiddleware
 *     // - SlimApiEssentials\App\Middlewares\Auth\ApikeyMiddleware
 *     // - SlimApiEssentials\App\Middlewares\Auth\RemoteMiddleware
 *     // - or any custom middleware with all refs
 *     //      (or multiple existing refs from app defaults auth Middlewares)
 *     //
 *     // so use any middleware or multiple at once (or none for public routes)
 *
 *     // SlimApiEssentials\App\Middlewares\Auth\Oauth2Middleware
 *     // - checks header "Authorization Bearer <value>" for token value
 *     // - or checks "Authorization Basic <value>" if allowed to generate the new token
 *     // !!! oauth2 uses JWT token type !!!
 *     // !!! in controller (for route post/token), use service SlimApiEssentials\App\Services\Auth\Oauth2 !!!
 *     // !!! and Oauth2::getNewTokenFromRequest(RequestInterface) !!!
 *     // !!! to get the new token from basic auth request !!!
 *     // (array)
 *     'authOauth2' => [
 *          // (string)
 *          // oauth2 route name where to authenticate and get token
 *         'routeName' => 'post/token'
 *         // (string)
 *         // class name for resolve users for auth
 *         //  !!! must implement SlimApiEssentials\App\Services\Auth\Oauth2UserResolverInterface !!!
 *         'userResolver' => App\UserResolverOauth2::class
 *     ]
 *
 *     // SlimApiEssentials\App\Middlewares\Auth\BasicMiddleware
 *     // - checks header "Authorization Basic <value>"
 *     // (array)
 *     'authBasic' => [
 *         // (string)
 *         // class name for resolve users for auth
 *         //  !!! must implement SlimApiEssentials\App\Services\Auth\BasicUserResolverInterface !!!
 *         'userResolver' => App\UserResolver::class
 *     ]
 *
 *     // SlimApiEssentials\App\Middlewares\Auth\ApikeyMiddleware
 *     // - checks header "Authorization ApiKey <value>"
 *     // (array)
 *     'authApikey' => [
 *         // (string)
 *         // class name for resolve users for auth
 *         //  !!! must implement SlimApiEssentials\App\Services\Auth\ApikeyUserResolverInterface !!!
 *         'userResolver' => App\UserResolverApikey::class
 *     ]
 *
 *     // SlimApiEssentials\App\Middlewares\Auth\RemoteMiddleware
 *     // - checks header "Authorization <type> <value>"
 *     // remote auth is like proxy - just resending requests and responses with all required headers
 *     //   1) client want resource from endpoint
 *     //   2) endpoint asks remote auth service if he can give resource
 *     //   3) auth service responds
 *     //   3.x) responds OK, and endpoint gives resource
 *     //   3.x) responds Error and sends some headers and info (how and possibly where to auth)
 *     // (array)
 *     'authRemote' => [
 *         // (bool)
 *         // whether to verify ssl certificate
 *         'certVerify' => true,
 *         // (array)
 *         // remote auth urls (at least one url is mandatory)
 *         'url' => [
 *             // for "Authorization Bearer ***"
 *             'Bearer' => 'https://url.where.to/check/oauth2/token',
 *             // for "Authorization ApiKey ***"
 *             'ApiKey' => 'https://url.where.to/check/apikey',
 *             // for "Authorization Basic ***"
 *             'Basic' => 'https://url.where.to/check/basicauth',
 *             // for "Authorization Anything ***"
 *             'Anything' => 'https://url.where.to/check/anyauth',
 *             //
 *             // ...
 *         ]
 *     ]
 *
 *     // SlimApiEssentials\App\Middlewares\Auth\SessionMiddleware
 *     // - checks POST/GET parameters
 *     // (array)
 *     'authSession' => [
 *         // (string)
 *         // POST/GET parameter name for representing 'username'
 *         //   (probably from html login form)
 *         'keyUsername' => 'username',
 *         // (string)
 *         // POST/GET parameter name for representing 'password'
 *         //   (probably from html login form)
 *         'keyPassword' => 'password',
 *          // (string)
 *          // html route name where to "show loginform"
 *          //     (where to redirect if session expired)
 *          //     (probably html website with login form)
 *         'routeNameGet' => 'get/login'
 *          // (string)
 *          // html route name where to authenticate
 *          //     (probably post route where credentials are checked and session is set)
 *         'routeNameSet' => 'post/login'
 *          // (string)
 *          // html route name where to redirect after the first success login
 *         'routeNameLoggedFirst' => 'get/home'
 *          // (array)
 *          // array of route argument names to be kept during login redirects
 *          // useful e.g. for localized apps via uri path
 *          // (can keep array empty if no app persistent params are requied)
 *         'routeArgsPersistent' => ['lang']
 *         // (string)
 *         // class name for resolve users for auth
 *         //  !!! must implement SlimApiEssentials\App\Services\Auth\SessionUserResolverInterface !!!
 *         'userResolver' => App\Services\Auth\UserResolverSession::class
 *         // (array)
 *         // post -> get redirection on session expiration during post action after redirected to loginform
 *         // after success login, post action is auto-repeated, but need some redirection to not stay on login form
 *         //  !!! if post action returns redirect response itself, this config array is NOT used !!!
 *         //  !!! the first found regexps wins !!!
 *         // array keys are post URL pathquery (path with query string) regexps usable in `preg_replace`
 *         //  (all escaped regexp - start/end delimiters are required - e.g. "/this.is\/regexp/", or "#this.is/regexp#")
 *         // array values are URL pathquery (path with querysting) where to redirect after login and post action repeat
 *         //  (regexp groups refs can be used $1, $2, ... exactly like in `preg_replace`)
 *         'expiredRedirectPost' => [
 *             '#^/(en|cs)/invoices/edit/([0-9]+)?#' => '/$1/invoices/edit/$2',
 *             '#^/(en|cs)/invoices/edit?#' => '/$1/invoices',
 *             '#^/(en|cs)/invoiceitems/edit/([0-9]+)#' => '/$1/invoiceitems/edit/$2',
 *             '#^/(en|cs)/invoiceitems/edit#' => '/$1/invoiceitems',
 *             '#^/(en|cs)/invoiceloggenerate/edit/([0-9]+)#' => '/$1/invoiceloggenerate/edit/$2',
 *             '#^/(en|cs)/invoiceloggenerate/edit#' => '/$1/invoiceloggenerate',
 *             '#^/(en|cs)/dailyitems/edit/([0-9]+)#' => '/$1/dailyitems/edit/$2',
 *             '#^/(en|cs)/dailyitems/edit#' => '/$1/dailyitems',
 *             '#^/(en|cs)/config/edit/([0-9]+)#' => '/$1/config/edit/$2',
 *             '#^/(en|cs)/config/edit#' => '/$1/config',
 *             '#^/(en|cs)/stakeholders/edit/([0-9]+)#' => '/$1/stakeholders/edit/$2',
 *             '#^/(en|cs)/stakeholders/edit#' => '/$1/stakeholders',
 *             '#^/(en|cs)/payments/edit/([0-9]+)#' => '/$1/payments/edit/$2',
 *             '#^/(en|cs)/payments/edit#' => '/$1/payments',
 *             '#^/(en|cs)/tags/edit/([0-9]+)#' => '/$1/tags/edit/$2',
 *             '#^/(en|cs)/tags/edit#' => '/$1/tags',
 *             '#^/(en|cs)/items/edit/([0-9]+)#' => '/$1/items/edit/$2',
 *             '#^/(en|cs)/items/edit#' => '/$1/items',
 *             '#^/(en|cs)/invoice/generate/([a-z]+)/([0-9]+)?#' => '/$1/invoices?in_dtSc[id]=$3'
 *         ]
 *     ]
 * ]
 */

declare(strict_types=1);

namespace SlimApiEssentials\App;

use SlimApiEssentials\App\Bootstrap\Bootstrap;
use SlimApiEssentials\App\Bootstrap\Actions;
use SlimApiEssentials\App\Bootstrap\Functions;
use SlimApiEssentials\App\Bootstrap\ErrorHandler;

require './../../vendor/autoload.php';

$pathConfig = './../config/config.php';
$pathConfigLocal = './../config/config.local.php';

$bootstrap = new Bootstrap(
    array_replace(
        file_exists($pathConfig) ? require $pathConfig : [],
        file_exists($pathConfigLocal) ? require $pathConfigLocal : []
    ),
    new Actions(
        new Functions(),
        new ErrorHandler()
    )
);

return $bootstrap;
