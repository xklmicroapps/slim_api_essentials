<?php

declare(strict_types=1);

namespace SlimApiEssentials\App\Middlewares;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Psr\Http\Server\MiddlewareInterface;
use RuntimeException;
use SlimApiEssentials\App\Services\RequestHelper;
use Slim\Routing\RouteContext;
use Slim\App;

class ContentCheckMiddleware implements MiddlewareInterface
{
    public function __construct(
        protected App $app,
        protected RequestHelper $requestHelper
    ) {
    }

    /**
     * @SuppressWarnings(PHPMD.StaticAccess)
     *
     * check if Content-Type is set to json (or ot her types for routes)
     * @param  ServerRequestInterface  $request [description]
     * @param  RequestHandlerInterface $handler [description]
     * @return ResponseInterface                           [description]
     */
    public function process(
        ServerRequestInterface $request,
        RequestHandlerInterface $handler
    ): ResponseInterface {
        $route = RouteContext::fromRequest($request)->getRoute();
        $routeName = $route ? $route->getName() : '';

        /**
         * specific route for oauth get token
         */
        $authOauth2Config = $this->app->getContainer()->getConfigParameter(
            'authOauth2'
        );

        if (
            isset($authOauth2Config['routeName'])
            && $routeName === $authOauth2Config['routeName']
        ) {
            $this->checkPostToken($request);
            return $handler->handle($request);
        }

        if ($this->app->getContainer()->getConfigParameter('contentCheck') === 'json') {
            $this->checkJson($request);
        }

        return $handler->handle($request);
    }

    /**
     * @param  ServerRequestInterface $request [description]
     * @return void                          [description]
     * @throws RuntimeException [<description>]
     */
    protected function checkJson(ServerRequestInterface $request): void
    {
        if (
            $this->requestHelper->isWithBody($request)
            && !$this->requestHelper->isJson($request)
        ) {
            throw new RuntimeException('Unsupported Media Type', 415);
        }
    }

    /**
     * predefined content-type by rfc for oauth2 token request
     * @param  ServerRequestInterface $request [description]
     * @return void                          [description]
     * @throws RuntimeException [<description>]
     */
    protected function checkPostToken(ServerRequestInterface $request): void
    {
        if (
            !$this->requestHelper->isFormUrlEncoded($request)
        ) {
            throw new RuntimeException('Unsupported Media Type', 415);
        }
    }
}
