<?php

declare(strict_types=1);

namespace SlimApiEssentials\App\Middlewares;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Psr\Http\Server\MiddlewareInterface;
use RuntimeException;
use JsonException;
use SlimApiEssentials\App\Services\RequestHelper;

class JsonContentParsingMiddleware implements MiddlewareInterface
{
    public function __construct(protected RequestHelper $requestHelper)
    {
    }

    /**
     * check if Content-Type is set to json (if required - POST, PUT, DELETE, PATCH)
     * @param  ServerRequestInterface  $request [description]
     * @param  RequestHandlerInterface $handler [description]
     * @return ResponseInterface                           [description]
     */
    public function process(
        ServerRequestInterface $request,
        RequestHandlerInterface $handler
    ): ResponseInterface {
        if ($this->requestHelper->isJson($request)) {
            $parsedBody = $request->getParsedBody();
            if (
                empty($parsedBody)
                && $this->requestHelper->isWithBody($request)
            ) {
                $bodyContent = (string) $request->getBody();
                if ($bodyContent) {
                    try {
                        $parsedBody = json_decode($bodyContent, true, 512, JSON_THROW_ON_ERROR);
                    } catch (JsonException $e) {
                        throw new RuntimeException(
                            'Body Content JSON Error (#' . $e->getCode() . ': ' . $e->getMessage() . ')',
                            400,
                            $e
                        );
                    }
                    $request = $request->withParsedBody($parsedBody);
                }
            }
        }
        return $handler->handle($request);
    }
}
