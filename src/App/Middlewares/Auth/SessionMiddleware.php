<?php

declare(strict_types=1);

namespace SlimApiEssentials\App\Middlewares\Auth;

use Throwable;
use SlimApiEssentials\App\Services\Auth\AuthInterface;
use SlimApiEssentials\App\Services\Auth\Session as AuthSession;
use SlimApiEssentials\App\Exceptions\AuthSessionException;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Psr\Http\Message\ResponseInterface;
use SlimApiEssentials\App\Services\RequestHelper;

class SessionMiddleware extends AbstractAuthMiddleware
{
    /**
     * @inheritdoc
     */
    public function process(
        ServerRequestInterface $request,
        RequestHandlerInterface $handler
    ): ResponseInterface {

        $authService = $this->getAuthService();
        /**
         * routes with "loginform" must be public
         */
        if ($authService->isRouteNameGet($request)) {
            return $handler->handle($request);
        }
        if ($authService->isRouteNameSet($request)) {
            return $this->getLoginResponse($authService, $request, $handler);
        }

        try {
            $authService->check($request);
        } catch (AuthSessionException $e) {
            return $authService->getResponseRedirectNotLogged($request);
        }

        return $handler->handle($request);
    }

    protected function getAuthService(): AuthInterface
    {
        return $this->app->getContainer()->get(AuthSession::class);
    }

    private function getRequestHelper(): RequestHelper
    {
        return $this->app->getContainer()->get(RequestHelper::class);
    }

    private function getLoginResponse(
        AuthSession $authService,
        ServerRequestInterface $request,
        RequestHandlerInterface $handler
    ) {
        /**
         * custom controller can register some callbacks
         *     onLoginSuccess
         *     onLoginError
         * so handle request there at first
         */
        $response = $handler->handle($request);
        try {
            $authService->login($request);

            return $this->getLoginResponseSuccess(
                $authService,
                $request,
                $response
            );
        } catch (AuthSessionException $e) {
            return $this->getLoginResponseError(
                $authService,
                $e,
                $request,
                $response
            );
        }
    }

    private function getLoginResponseSuccess(
        AuthSession $authService,
        ServerRequestInterface $request,
        ResponseInterface $response
    ) {
        $authResponse = $authService->getResponseRedirectLogged(
            $request
        );
        if ($authService->getOnLoginSuccess() !== null) {
            $respCb = ($authService->getOnLoginSuccess())($request, $response, $authResponse);
            if ($respCb instanceof ResponseInterface) {
                return $respCb;
            }
        }
        return $authResponse;
    }
    private function getLoginResponseError(
        AuthSession $authService,
        AuthSessionException $exception,
        ServerRequestInterface $request,
        ResponseInterface $response
    ) {
        $authResponse = $authService->getResponseRedirectNotLogged($request);
        if ($authService->getOnLoginError() !== null) {
            $respCb = ($authService->getOnLoginError())(
                $exception,
                $request,
                $response,
                $authResponse
            );
            if ($respCb instanceof ResponseInterface) {
                return $respCb;
            }
        }
        if ($this->getRequestHelper()->isAcceptJson($request)) {
            throw $exception;
        }
        return $authResponse;
    }
}
