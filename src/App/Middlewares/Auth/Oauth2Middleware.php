<?php

declare(strict_types=1);

namespace SlimApiEssentials\App\Middlewares\Auth;

use SlimApiEssentials\App\Services\Auth\AuthInterface;
use SlimApiEssentials\App\Services\Auth\Oauth2 as AuthOauth2;

class Oauth2Middleware extends AbstractAuthMiddleware
{
    protected function getAuthService(): AuthInterface
    {
        return $this->app->getContainer()->get(AuthOauth2::class);
    }
}
