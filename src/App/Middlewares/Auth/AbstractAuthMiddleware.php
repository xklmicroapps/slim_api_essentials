<?php

declare(strict_types=1);

namespace SlimApiEssentials\App\Middlewares\Auth;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Psr\Http\Server\MiddlewareInterface;
use Slim\App;
use SlimApiEssentials\App\Services\Auth\AuthInterface;

abstract class AbstractAuthMiddleware implements MiddlewareInterface
{
    /**
     * use app to be more dynamic later in processing request
     */
    public function __construct(
        protected App $app
    ) {
    }

    abstract protected function getAuthService(): AuthInterface;

    /**
     * check authorization by api request
     * @param  ServerRequestInterface  $request [description]
     * @param  RequestHandlerInterface $handler [description]
     * @return ResponseInterface                           [description]
     */
    public function process(
        ServerRequestInterface $request,
        RequestHandlerInterface $handler
    ): ResponseInterface {

        $this->getAuthService()->check($request);

        return $handler->handle($request);
    }
}
