<?php

declare(strict_types=1);

namespace SlimApiEssentials\App\Middlewares\Auth;

use SlimApiEssentials\App\Services\Auth\AuthInterface;
use SlimApiEssentials\App\Services\Auth\Basic as AuthBasic;

class BasicMiddleware extends AbstractAuthMiddleware
{
    protected function getAuthService(): AuthInterface
    {
        return $this->app->getContainer()->get(AuthBasic::class);
    }
}
