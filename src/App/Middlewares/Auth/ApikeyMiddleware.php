<?php

declare(strict_types=1);

namespace SlimApiEssentials\App\Middlewares\Auth;

use SlimApiEssentials\App\Services\Auth\AuthInterface;
use SlimApiEssentials\App\Services\Auth\Apikey as AuthApikey;

class ApikeyMiddleware extends AbstractAuthMiddleware
{
    protected function getAuthService(): AuthInterface
    {
        return $this->app->getContainer()->get(AuthApikey::class);
    }
}
