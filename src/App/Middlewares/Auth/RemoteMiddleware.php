<?php

declare(strict_types=1);

namespace SlimApiEssentials\App\Middlewares\Auth;

use SlimApiEssentials\App\Services\Auth\AuthInterface;
use SlimApiEssentials\App\Services\Auth\Remote as AuthRemote;

class RemoteMiddleware extends AbstractAuthMiddleware
{
    protected function getAuthService(): AuthInterface
    {
        return $this->app->getContainer()->get(AuthRemote::class);
    }
}
