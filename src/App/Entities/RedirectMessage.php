<?php

declare(strict_types=1);

namespace SlimApiEssentials\App\Entities;

/**
 * @OA\Schema(required = {"location"})
 */
class RedirectMessage extends AbstractMessage
{
    /**
     * Allways true
     * @OA\Property(property = "redirect", type = "bool")
     */
    protected bool $redirect = true;

    /**
     * Redirect location
     * @OA\Property(property = "location", type = "string")
     */
    protected string $location;

    /**
     * initial set of attributes
     *
     * @param array $params   key => value, where key is class attribute name and value is value to be assigned
     * @param array $required attribute names to be required (have to exist and contain value)
     */
    public function __construct(array $params, array $required = ['location'])
    {
        parent::__construct($params, $required);
    }
}
