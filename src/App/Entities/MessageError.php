<?php

declare(strict_types=1);

namespace SlimApiEssentials\App\Entities;

/**
 * @SuppressWarnings(PHPMD.CamelCasePropertyName)
 *
 * @see  https://tools.ietf.org/html/rfc6749 for proper parameters
 *
 * @OA\Schema(required = {"error"})
 */
class MessageError extends AbstractMessage
{
    /**
     * Error code
     * @OA\Property(property = "error", type = "string")
     */
    protected string $error;

    /**
     * Error message
     * @OA\Property(property = "error_description", type = "string")
     */
    protected string $error_description;

    /**
     * Can be any type/value, including `null`.
     * @OA\Property(property = "debug")
     */
    protected $debug;

    /**
     * initial set of attributes
     *
     * @param array $params   key => value, where key is class attribute name and value is value to be assigned
     * @param array $required attribute names to be required (have to exist and contain value)
     */
    public function __construct(array $params, array $required = ['error'])
    {
        parent::__construct($params, $required);
    }
}
