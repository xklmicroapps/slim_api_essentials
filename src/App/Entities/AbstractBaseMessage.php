<?php

declare(strict_types=1);

namespace SlimApiEssentials\App\Entities;

use JsonSerializable;

abstract class AbstractBaseMessage implements JsonSerializable
{
}
