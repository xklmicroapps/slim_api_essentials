<?php

declare(strict_types=1);

namespace SlimApiEssentials\App\Entities;

use SlimApiEssentials\App\Entities\AbstractMessage;

/**
 * @SuppressWarnings(PHPMD.CamelCasePropertyName)
 *
 * @see  https://tools.ietf.org/html/rfc6749 for proper parameters
 *
 * @OA\Schema(required = {"expires_in", "valid_in", "access_token"})
 */
class Oauth2JwtToken extends AbstractMessage
{
    /**
     * Num of seconds after which token is going to expire
     * @OA\Property(property = "expires_in", type = "integer")
     */
    protected int $expires_in;

    /**
     * Num of seconds after which token is going to start to be considering valid
     * @OA\Property(property = "valid_in", type = "integer")
     */
    protected int $valid_in;

    /**
     * Encoded string token to be used in an Authorization header Bearer
     * @OA\Property(property = "access_token", type = "string")
     */
    protected string $access_token;

    /**
     * Type of the token
     * @OA\Property(property = "token_type", type = "string", default = "Bearer")
     */
    protected string $token_type = "Bearer";

    /**
     * initial set of attributes
     *
     * @param array $params   key => value, where key is class attribute name and value is value to be assigned
     * @param array $required attribute names to be required (have to exist and contain value)
     */
    public function __construct(
        array $params,
        array $required = [
            'expires_in',
            'valid_in',
            'access_token'
        ]
    ) {
        parent::__construct($params, $required);
    }
}
