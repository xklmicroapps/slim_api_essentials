<?php

declare(strict_types=1);

namespace SlimApiEssentials\App\Entities;

/**
 * @OA\Schema(required = {"message"})
 */
class Message extends AbstractMessage
{
    /**
     * Error code
     * @OA\Property(property = "code", type = "string")
     */
    protected string $code;

    /**
     * Error message
     * @OA\Property(property = "message", type = "string")
     */
    protected string $message;

    /**
     * Can be any type/value, including `null`.
     * @OA\Property(property = "info")
     */
    protected $info;

    /**
     * initial set of attributes
     *
     * @param array $params   key => value, where key is class attribute name and value is value to be assigned
     * @param array $required attribute names to be required (have to exist and contain value)
     */
    public function __construct(array $params, array $required = ['message'])
    {
        parent::__construct($params, $required);
    }
}
