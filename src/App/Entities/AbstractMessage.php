<?php

declare(strict_types=1);

namespace SlimApiEssentials\App\Entities;

use LogicException;
use ReflectionProperty;
use ReflectionNamedType;

abstract class AbstractMessage extends AbstractBaseMessage
{
    /**
     * initial set of attributes
     *
     * @param array $params   key => value, where key is class attribute name and value is value to be assigned
     *                        all $params must exists in $this context as a class attributes
     * @param array $required attribute names to be required (have to exist and contain value)
     */
    public function __construct(array $params, array $required = [])
    {
        $paramValues = [];
        foreach ($params as $key => $value) {
            if (!property_exists($this, $key)) {
                throw new LogicException(
                    "Unknown parameter \"$key\". Expected any of "
                        . json_encode(
                            $this->getClassProperties(),
                            JSON_THROW_ON_ERROR
                                | JSON_UNESCAPED_UNICODE
                                | JSON_UNESCAPED_SLASHES
                        )
                );
            }
            $paramValues[$key] = $this->getTypedPropertyValue($key, $value);
        }
        foreach ($required as $key) {
            // check if required class attribute exists and if has some value (not NULL value)
            if (!property_exists($this, $key)) {
                throw new LogicException(
                    "Unknown parameter \"$key\". Expected any of "
                        . json_encode(
                            $this->getClassProperties(),
                            JSON_THROW_ON_ERROR
                                | JSON_UNESCAPED_UNICODE
                                | JSON_UNESCAPED_SLASHES
                        )
                );
            }
            if (!isset($paramValues[$key])) {
                throw new LogicException("Required parameter \"$key\" must not be NULL value");
            }
        }
        foreach ($paramValues as $key => $value) {
            $this->$key = $value;
        }
    }

    /**
     * try to properly retype properties
     * but only "possibly allowed" properties types
     *
     * @param  string $key   [description]
     * @param  mixed  $value [description]
     * @return mixed        [description]
     */
    private function getTypedPropertyValue(string $key, mixed $value): mixed
    {
        $refProperty = new ReflectionProperty(get_class($this), $key);
        if (!$refProperty->hasType()) {
            return $value;
        }
        $refType = $refProperty->getType();
        if (!($refType instanceof ReflectionNamedType) || !$refType->isBuiltin()) {
            return $value;
        }

        if ($value === null && $refType->allowsNull()) {
            return $value;
        }

        $refTypeName = $refType->getName();
        if (!in_array($refTypeName, $this->getSettypeTypesAllowed())) {
            return $value;
        }

        if ($refTypeName !== gettype($value)) {
            settype($value, $refTypeName);
        }

        return $value;
    }

    /**
     * @see https://www.php.net/manual/en/function.settype
     *
     * @return string[] [description]
     */
    private function getSettypeTypesAllowed(): array
    {
        return [
            "boolean",
            "bool",
            "integer",
            "int",
            "float",
            "double",
            "string",
            "array",
            "object",
            "null"
        ];
    }

    private function getClassProperties(): array
    {
        return array_keys(get_class_vars(get_class($this)));
    }

    /**
     * get class parameter
     *
     * @param  string $name parameter name to get
     * @return mixed       [description]
     */
    public function __get(string $name)
    {
        // call getName() first to try to get value
        if (method_exists($this, "get" . ucfirst($name))) {
            return $this->{"get" . ucfirst($name)}();
        }
        // check if class attribute exists
        if (!property_exists($this, $name)) {
            throw new LogicException("Unknown parameter name \"$name\"");
        }

        return $this->$name;
    }

    /**
     * get all params of the object with values defined in __construct
     * @return array [description]
     */
    public function getParameters(): array
    {
        $vars = [];
        foreach (array_keys(get_object_vars($this)) as $key) {
            // because of __get method usage try to access vars with use of this magic method
            $vars[$key] = $this->__get($key);
        }
        return $vars;
    }

    /**
     * Serializes the object to a value that can be serialized natively by json_encode()
     */
    public function jsonSerialize(): mixed
    {
        return $this->getParameters();
    }
}
