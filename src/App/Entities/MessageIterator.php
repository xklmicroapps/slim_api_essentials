<?php

declare(strict_types=1);

namespace SlimApiEssentials\App\Entities;

use Iterator;
use Countable;

class MessageIterator extends AbstractBaseMessage implements Iterator, Countable
{
    private $array = [];

    private $position = 0;

    public function add(AbstractMessage $message)
    {
        $this->array[] = $message;
    }

    public function unset(int $position)
    {
        unset($this->array[$position]);
    }

    public function count(): int
    {
        return count($this->array);
    }

    /**
     * Serializes the object to a value that can be serialized natively by json_encode()
     *     return the real array; without this method, json_encode() for num indexed array
     *         would allways create object (e.g. {"0":"sth", "1":"sth1"}) istead of just array (["sth", "sth1"])
     */
    public function jsonSerialize(): mixed
    {
        $tmp = [];
        foreach ($this->array as $key => $value) {
            $tmp[$key] = $value;
        }
        return $tmp;
    }

    public function rewind(): void
    {
        $this->position = 0;
    }

    public function current(): mixed
    {
        return $this->array[$this->position];
    }

    public function key(): mixed
    {
        return $this->position;
    }

    public function next(): void
    {
        ++$this->position;
    }

    public function valid(): bool
    {
        return isset($this->array[$this->position]);
    }
}
