<?php

declare(strict_types=1);

namespace SlimApiEssentials\App\Exceptions;

use RuntimeException;
use Psr\Http\Message\ResponseInterface;

class AuthException extends RuntimeException
{
    protected $code = 401;
    protected $message = 'Unauthorized!';

    protected ?string $authenticateLocation = null;

    public function setAuthenticateLocation(string $location)
    {
        $this->authenticateLocation = $location;

        return $this;
    }

    public function getAuthenticateLocation()
    {
        return $this->authenticateLocation;
    }

    /**
     * set proper headers to response and return this respose
     *     Xklmicroapps-Authenticate-Location - custom header with location where to authenticate (where get token)
     * @param  ResponseInterface $response [description]
     * @return ResponseInterface                      [description]
     */
    public function getResponseWithAuthenticate(
        ResponseInterface $response
    ): ResponseInterface {
        $location = $this->getAuthenticateLocation();
        if ($location) {
            $response = $response->withHeader(
                'Xklmicroapps-Authenticate-Location',
                $location
            );
        }
        return $response;
    }
}
