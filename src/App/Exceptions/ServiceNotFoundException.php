<?php

declare(strict_types=1);

namespace SlimApiEssentials\App\Exceptions;

use Psr\Container\NotFoundExceptionInterface;
use Exception;

class ServiceNotFoundException extends Exception implements NotFoundExceptionInterface
{
}
