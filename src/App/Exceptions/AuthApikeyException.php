<?php

declare(strict_types=1);

namespace SlimApiEssentials\App\Exceptions;

use Psr\Http\Message\ResponseInterface;

class AuthApikeyException extends AuthException
{
    public function getResponseWithAuthenticate(
        ResponseInterface $response
    ): ResponseInterface {
        return parent::getResponseWithAuthenticate(
            $response
        )->withHeader(
            'WWW-Authenticate',
            'ApiKey realm="Xklmicroapps"'
        );
    }
}
