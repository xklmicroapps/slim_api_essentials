<?php

declare(strict_types=1);

namespace SlimApiEssentials\App\Exceptions;

use Psr\Http\Message\ResponseInterface;

class AuthSessionException extends AuthException
{
    private array $args = [];

    public function getResponseWithAuthenticate(
        ResponseInterface $response
    ): ResponseInterface {
        return parent::getResponseWithAuthenticate(
            $response
        )->withHeader(
            'WWW-Authenticate',
            'Session realm="Xklmicroapps"'
        );
    }

    public function getArgs()
    {
        return $this->args;
    }

    public function setArgs(array $values): self
    {
        $this->args = $values;

        return $this;
    }
}
