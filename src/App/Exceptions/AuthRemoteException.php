<?php

declare(strict_types=1);

namespace SlimApiEssentials\App\Exceptions;

use Psr\Http\Message\ResponseInterface;

class AuthRemoteException extends AuthException
{
    protected ?string $wwwAuthenticate = null;

    public function setWwwAuthenticate(string $value)
    {
        $this->wwwAuthenticate = $value;

        return $this;
    }

    public function getWwwAuthenticate(): ?string
    {
        return $this->wwwAuthenticate;
    }

    public function getResponseWithAuthenticate(
        ResponseInterface $response
    ): ResponseInterface {
        $response = parent::getResponseWithAuthenticate(
            $response
        );

        $wwwAuth = $this->getWwwAuthenticate();
        if ($wwwAuth !== null) {
            $response = $response->withHeader(
                'WWW-Authenticate',
                $wwwAuth
            );
        }

        return $response;
    }
}
