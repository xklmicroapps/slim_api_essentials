<?php

declare(strict_types=1);

namespace SlimApiEssentials\App\Exceptions;

use RuntimeException;
use Psr\Http\Message\ResponseInterface;

class AuthOauth2Exception extends AuthException
{
    protected string $error = 'invalid_request';

    protected array $errorsAllowed = [
        // The request is missing a required parameter, includes an
        // unsupported parameter value (other than grant type),
        // repeats a parameter, includes multiple credentials,
        // utilizes more than one mechanism for authenticating the
        // client, or is otherwise malformed.
        'invalid_request',
        // Client authentication failed (e.g., unknown client, no
        // client authentication included, or unsupported
        // authentication method).  The authorization server MAY
        // return an HTTP 401 (Unauthorized) status code to indicate
        // which HTTP authentication schemes are supported.  If the
        // client attempted to authenticate via the "Authorization"
        // request header field, the authorization server MUST
        // respond with an HTTP 401 (Unauthorized) status code and
        // include the "WWW-Authenticate" response header field
        // matching the authentication scheme used by the client.
        'invalid_client',
        // The provided authorization grant (e.g., authorization
        // code, resource owner credentials) or refresh token is
        // invalid, expired, revoked, does not match the redirection
        // URI used in the authorization request, or was issued to
        // another client.
        'invalid_grant',
        // The authenticated client is not authorized to use this
        // authorization grant type.
        'unauthorized_client',
        // The authorization grant type is not supported by the
        // authorization server.
        'unsupported_grant_type'
    ];

    /**
     * set specific OAuth2 error "code"
     * @param string $error [description]
     * @return AuthOauth2Exception [description]
     */
    public function setError(string $error): AuthOauth2Exception
    {
        if (!in_array($error, $this->errorsAllowed, true)) {
            throw new RuntimeException(
                "Invalid error \"$error\"! Allowed only one of: " . json_encode($this->errorsAllowed)
            );
        }
        $this->error = $error;

        return $this;
    }

    /**
     * [getError description]
     * @return string [description]
     */
    public function getError(): string
    {
        return $this->error;
    }

    public function getResponseWithAuthenticate(
        ResponseInterface $response
    ): ResponseInterface {
        $response = parent::getResponseWithAuthenticate(
            $response
        );

        switch ($this->getError()) {
            case 'invalid_client':
                $response = $response->withHeader('WWW-Authenticate', 'Basic realm="Xklmicroapps"');
                break;
            default:
                $response = $response->withHeader('WWW-Authenticate', 'Bearer realm="Xklmicroapps"');
                break;
        }
        return $response;
    }
}
