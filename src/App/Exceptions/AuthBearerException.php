<?php

declare(strict_types=1);

namespace SlimApiEssentials\App\Exceptions;

use Psr\Http\Message\ResponseInterface;

class AuthBearerException extends AuthException
{
    public function getResponseWithAuthenticate(
        ResponseInterface $response
    ): ResponseInterface {
        return parent::getResponseWithAuthenticate(
            $response
        )->withHeader(
            'WWW-Authenticate',
            'Bearer realm="Xklmicroapps"'
        );
    }
}
