<?php

declare(strict_types=1);

namespace SlimApiEssentials\App\Services;

use PDO;
use PDOStatement;
use RuntimeException;
use SlimApiEssentials\App\Services\Helpers\DatabaseKeywords;

class Database
{
    public function __construct(
        private PDO $pdo,
        private DatabaseKeywords $keywordsHelper
    ) {
    }

    public function run(string $sql, array $args = []): PDOStatement
    {
        $stmt = $this->pdo->prepare($sql);
        $params = [];
        foreach ($args as $key => $value) {
            switch (gettype($value)) {
                case 'array':
                    foreach ($value as $key2 => $value2) {
                        $this->bindParam($stmt, "$key$key2", $value2);
                        $params["$key$key2"] = $value2;
                    }
                    break;
                default:
                    $this->bindParam($stmt, $key, $value);
                    $params[$key] = $value;
                    break;
            }
        }
        $stmt->execute($params);
        return $stmt;
    }

    private function bindParam(PDOStatement $stmt, $key, &$value)
    {
        switch (gettype($value)) {
            case 'integer':
                $stmt->bindParam($key, $value, PDO::PARAM_INT);
                break;
            case 'boolean':
                /**
                 * have to retype to int (true ===> '1', false ===> '0')
                 * because of buggy behaviour with PDO::PARAM_BOOL and postgres boolean type
                 */
                $value = $value ? '1' : '0';
                $stmt->bindParam($key, $value, PDO::PARAM_BOOL);
                break;
            default:
                $stmt->bindParam($key, $value, PDO::PARAM_STR);
                break;
        }
    }

    /**
     * get string parameter filter parsed as an array
     * @param  string $filterParam JSON string to parse and return array|null
     * @return array|null              [description]
     * @throws RuntimeException       422 error
     */
    public function getFilterParamParsed(?string $filterParam): ?array
    {
        try {
            return $filterParam ? json_decode($filterParam, true, 512, JSON_THROW_ON_ERROR) : null;
        } catch (\JsonException $e) {
            throw new RuntimeException('"Filter parsing" Error! ' . $e->getMessage(), 422, $e);
        }
    }

    /**
     * get part of the sql representing conditions applicable in WHERE
     * @param  array  $definedConditions array containing subarrays;
     *                                   subarrays are $key => $value as a argName => predefinedCondition
     *                                   subarrays can contain '__join_string' key with value for subFilter join string (mostly enum('and', 'or'))
     * @param  array|null  $filterParamParsed    result from $this->getFilterParamParsed()
     * @param  string $joinString        mostly enum('and', 'or')
     * @return string                    [description]
     */
    public function getSqlFilterCondition(array $definedConditions, ?array $filterParamParsed, string $joinString = 'AND'): string
    {
        $sqlFilter = [];
        foreach ($definedConditions as $definedCondition) {
            // special key => value pair
            $joinStringPart = $definedCondition['__join_string'] ?? 'AND';
            unset($definedCondition['__join_string']);

            $sqlFilterPart = [];
            foreach ($definedCondition as $param => $condition) {
                if (isset($filterParamParsed[$param])) {
                    switch (gettype($filterParamParsed[$param])) {
                        case 'array':
                            $sqlFilterPart[] = "$condition ("
                                . implode(
                                    ',',
                                    preg_filter('#^#', ":$param", array_keys($filterParamParsed[$param]))
                                )
                                . ')';
                            break;
                        default:
                            $sqlFilterPart[] = strpos($condition, ":$param") === false ? "$condition :$param" : $condition;
                            break;
                    }
                }
            }
            if ($sqlFilterPart) {
                $sqlFilter[] = '(' . implode(" $joinStringPart ", $sqlFilterPart) . ')';
            }
        }
        return implode(" $joinString ", $sqlFilter);
    }

    /**
     * get par of the sql representing ORDER BY based on params pased
     * @param  array  $orderByAllowedCols [description]
     * @param  string|null $orderByArg    list string of columns with optional direction (default is :asc)
     *                                    id:desc,name,something:asc,anhything:desc
     * @return string                     [description]
     */
    public function getSqlOrderBy(array $orderByAllowedCols, ?string $orderByArg = null): string
    {
        $sql = '';
        if (!$orderByArg) {
            return $sql;
        }

        $orderBy = [];
        foreach ($this->getOrderByArgs($orderByAllowedCols, $orderByArg) as $field => $direction) {
            $orderByArgDir = "$field $direction";
            if ($this->keywordsHelper->isKeyword($field)) {
                $orderByArgDir = "\"$field\" $direction";
            }
            $orderBy[] = $orderByArgDir;
        }
        if ($orderBy) {
            $sql .= " ORDER BY " . implode(", ", $orderBy);
        }

        return $sql;
    }

    /**
     * get array with fields and directions usable for orderby
     * @param  array  $orderByAllowedCols [description]
     * @param  string|null $orderByArg    list string of columns with optional direction (default is :asc)
     *                                    id:desc,name,something:asc,anhything:desc
     * @return array                     [field => direction, ...]
     *                                          e.g. ['column1' => 'asc', 'column2' => 'desc', ...]
     * @throws RuntimeException       422 error                        [description]
     */
    public function getOrderByArgs(array $orderByAllowedCols, ?string $orderByArg = null): array
    {
        try {
            $args = [];
            if (!$orderByArg) {
                return $args;
            }

            $orderByAllowedDirs = ['asc', 'desc'];
            $orderByArgs = explode(',', trim($orderByArg, ','));
            foreach ($orderByArgs as $columnDirection) {
                $columnDirectionTrim = trim($columnDirection);
                $columnDirectionParts = explode(':', strrev($columnDirection), 2);
                $directionParsed = strrev(trim($columnDirectionParts[0]));
                $column = $columnDirectionTrim;
                $direction = 'asc';
                if (
                    in_array(strtolower($directionParsed), $orderByAllowedDirs)
                    && isset($columnDirectionParts[1])
                ) {
                    $column = strrev(trim($columnDirectionParts[1]));
                    $direction = $directionParsed;
                }
                if (!in_array($column, $orderByAllowedCols)) {
                    throw new RuntimeException(
                        "Column name \"$column\" is not allowed for sorting. (Allowed: " .
                            json_encode($orderByAllowedCols) . ')'
                    );
                }
                $args[$column] = $direction;
            }
        } catch (\Exception $e) {
            throw new RuntimeException('"Order By" Error! ' . $e->getMessage(), 422, $e);
        }

        return $args;
    }

    /**
     * get part of the sql representing LIMIT and OFFSET
     * @param  string|null $limit  numeric string (string integer from url query string - going to be retyped to the real int)
     * @param  string|null $offset numeric string (string integer from url query string - going to be retyped to the real int)
     * @return string           [description]
     */
    public function getSqlLimitOffset(?string $limitParam = null, ?string $offsetParam = null): string
    {
        $sql = "";
        if ($limitParam !== null) {
            $sql .= " LIMIT " . (int) $limitParam;
        }
        if ($offsetParam !== null) {
            $sql .= " OFFSET " . (int) $offsetParam;
        }

        return $sql;
    }

    /**
     * preferably call $this->pdo methods
     *
     * @param  string $name      [description]
     * @param  array  $arguments [description]
     * @return mixed            [description]
     */
    public function __call(string $name, array $arguments): mixed
    {
        return $this->pdo->{$name}(...$arguments);
    }
}
