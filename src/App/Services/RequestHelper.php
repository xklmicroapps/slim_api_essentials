<?php

declare(strict_types=1);

namespace SlimApiEssentials\App\Services;

use Psr\Http\Message\RequestInterface;

class RequestHelper
{
    /**
     * true if header X-Requested-With === XMLHttpRequest
     *     (for javascript .fetch api, there must be some workaround to append such header)
     */
    public function isAjax(RequestInterface $request): bool
    {
        return strcasecmp(
            $request->getHeaderLine('X-Requested-With'),
            'XMLHttpRequest'
        ) === 0;
    }

    public function isFormUrlEncoded(RequestInterface $request): bool
    {
        return stripos(
            $request->getHeaderLine('Content-Type'),
            'application/x-www-form-urlencoded'
        ) !== false;
    }

    public function isHtml(RequestInterface $request): bool
    {
        if ($this->isJson($request)) {
            return false;
        }

        // all other requests consider as html
        return true;
    }

    public function isJson(RequestInterface $request): bool
    {
        return stripos(
            $request->getHeaderLine('Content-Type'),
            'application/json'
        ) !== false;
    }

    public function isAcceptHtml(RequestInterface $request): bool
    {
        if ($this->isAcceptJson($request)) {
            return false;
        }

        if (
            stripos(
                $request->getHeaderLine('Accept'),
                'text/html'
            ) !== false
        ) {
            return true;
        }
        return stripos(
            $request->getHeaderLine('Accept'),
            '*/*'
        ) !== false;
    }

    public function isAcceptJson(RequestInterface $request): bool
    {
        return stripos(
            $request->getHeaderLine('Accept'),
            'application/json'
        ) !== false;
    }

    public function isWithBody(RequestInterface $request): bool
    {
        if ((string) $request->getBody() || $request->getParsedBody()) {
            return true;
        }
        return false;
    }
}
