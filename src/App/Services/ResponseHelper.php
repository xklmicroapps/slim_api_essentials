<?php

declare(strict_types=1);

namespace SlimApiEssentials\App\Services;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\RequestInterface;
use SlimApiEssentials\App\Entities\AbstractBaseMessage;
use SlimApiEssentials\App\Entities\RedirectMessage;
use Slim\App;
use RuntimeException;

class ResponseHelper
{
    public function __construct(
        private App $app,
        private RequestHelper $requestHelper
    ) {
    }

    /**
     * get response with location header
     *     uri obtained from $fromRequest
     *
     * if $request is ajax,
     *     then return json response with proper payload
     *     to allow Ajaxify.js redirection
     */
    public function getWithLocationFromRequest(
        RequestInterface $request,
        ResponseInterface $response,
        RequestInterface $fromRequest,
        int $status = 302
    ): ResponseInterface {
        $location = urldecode(
            $fromRequest->getUri()->__toString()
        );

        return $this->getWithLocationUri(
            $request,
            $response,
            $location,
            $status
        );
    }

    /**
     * get response with location header
     *     uri generated from $routeName
     *
     * if $request is ajax,
     *     then return json response with proper payload
     *     to allow Ajaxify.js redirection
     *
     * for $urlMaxLength, if not null
     *     check location url length and throw exception if is longer
     */
    public function getWithLocation(
        RequestInterface $request,
        ResponseInterface $response,
        string $routeName,
        array $data = [],
        array $queryParams = [],
        int $status = 302,
        ?int $urlMaxLength = null
    ): ResponseInterface {

        // join param arrays like comma delimited string
        $dataToPass = array_map(
            function ($val) {
                return is_array($val) ? implode(',', $val) : $val;
            },
            $data
        );

        $location = urldecode(
            $this->app->getRouteCollector()->getRouteParser()->fullUrlFor(
                $request->getUri(),
                $routeName,
                $dataToPass,
                $queryParams
            )
        );

        if ($urlMaxLength !== null && strlen($location) > $urlMaxLength) {
            throw new RuntimeException(
                "Location URL is too long!"
                    . " \n"
                    . " Reduce generated URL max strlen $urlMaxLength chars."
                    . " \n"
                    . " (Actual location URL strlen is " . strlen($location) . ")"
            );
        }

        return $this->getWithLocationUri(
            $request,
            $response,
            $location,
            $status
        );
    }

    /**
     * for basic http request:
     *     return response with Location and $status
     * for "ajax" request:
     *     return JSON response with payload.redirect === true and payload.location === $uri
     */
    public function getWithLocationUri(
        RequestInterface $request,
        ResponseInterface $response,
        string $uri,
        int $status = 302
    ): ResponseInterface {
        if ($this->requestHelper->isAcceptJson($request)) {
            return $this->getJson(
                $response,
                new RedirectMessage(
                    [
                        'location' => $uri
                    ]
                )
            );
        }

        return $response
                ->withHeader(
                    'Location',
                    $uri
                )
                ->withStatus($status);
    }

    /**
     * get html response
     */
    public function getHtml(
        ResponseInterface $response,
        string $payload,
        int $status = 200
    ): ResponseInterface {
        $response->getBody()->write($payload);
        return $response
                ->withHeader('Content-Type', 'text/html')
                ->withStatus($status);
    }

    /**
     * get json response (with use of string payload)
     */
    public function getJsonFromText(
        ResponseInterface $response,
        string $payload,
        int $status = 200
    ): ResponseInterface {
        $response->getBody()->write(
            $payload
        );

        return $this->getWithJson($response, $status);
    }

    /**
     * get json response (with use of Message/jsonseencode`able payload)
     */
    public function getJson(
        ResponseInterface $response,
        AbstractBaseMessage $payload,
        int $status = 200
    ): ResponseInterface {
        $response->getBody()->write(
            json_encode(
                $payload,
                JSON_THROW_ON_ERROR
                | JSON_UNESCAPED_UNICODE
                | JSON_UNESCAPED_SLASHES
            )
        );

        return $this->getWithJson($response, $status);
    }

    /**
     * get response with CORS headers
     */
    public function getWithCors(
        ResponseInterface $response,
        array $addHeaders = []
    ): ResponseInterface {
        $response = $response
                        ->withHeader('Access-Control-Allow-Origin', '*')
                        ->withHeader('Access-Control-Allow-Methods', 'GET, POST, DELETE, PUT, PATCH, OPTIONS')
                        ->withHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization, X-Requested-With');
        foreach ($addHeaders as $key => $value) {
            $response = $response->withHeader($key, $value);
        }
        return $response;
    }

    private function getWithJson(
        ResponseInterface $response,
        int $status = 200
    ): ResponseInterface {
        $response = $this->getWithCors(
            $response,
            [
                'Access-Control-Expose-Headers' => 'Content-Type, Location'
            ]
        );
        return $response
                ->withHeader('Content-Type', 'application/json')
                ->withStatus($status);
    }
}
