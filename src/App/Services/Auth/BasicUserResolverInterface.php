<?php

declare(strict_types=1);

namespace SlimApiEssentials\App\Services\Auth;

interface BasicUserResolverInterface
{
    /**
     * get user entity by its username for basic auth
     */
    public function getUserByUsername(string $username): mixed;

    /**
     * verify password for user returned by $this->getUserByUsername()
     * and return true if ok, or false if not ok
     */
    public function isUserPassOk(mixed $user, string $password): bool;
}
