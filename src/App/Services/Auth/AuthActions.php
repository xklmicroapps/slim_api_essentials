<?php

declare(strict_types=1);

namespace SlimApiEssentials\App\Services\Auth;

use Psr\Http\Message\RequestInterface;
use SlimApiEssentials\App\Exceptions\AuthException;
use SlimApiEssentials\App\Exceptions\AuthBasicException;
use SlimApiEssentials\App\Services\Helpers\Common as CommonHelper;

class AuthActions
{
    public function __construct(private CommonHelper $commonHelper)
    {
    }

    public function getAuthorizationTypeFromRequest(RequestInterface $request): ?string
    {
        $authHeaderContent = $this->getAuthorizationHeaderContent($request);
        if (!$authHeaderContent) {
            return null;
        }

        $matches = null;
        if (
            !preg_match(
                '#^\s*([A-Za-z]+)\s+[^\s]+\s*$#',
                $authHeaderContent,
                $matches
            )
        ) {
            return null;
        }

        return $matches[1];
    }

    public function getBasicCredentialsFromRequest(RequestInterface $request): array
    {
        $matches = null;
        $authHeaderContent = $this->getAuthorizationHeaderContent($request);
        if (
            !preg_match(
                '#^\s*Basic\s+([A-Za-z0-9+/=]+)\s*$#',
                $authHeaderContent,
                $matches
            )
        ) {
            throw new AuthBasicException(
                'Unknown Authorization header! "'
                . $authHeaderContent
                . '"'
            );
        }

        $authCredentialsArr = explode(
            ':',
            $this->commonHelper->base64Decode($matches[1]),
            2
        );
        if (count($authCredentialsArr) !== 2) {
            throw new AuthBasicException(
                'Invalid Auth header Basic format! Basic Auth format must be "base64Encode(id:pass)"'
            );
        }

        return $authCredentialsArr;
    }

    public function getAuthorizationHeaderContent(RequestInterface $request): string
    {
        return $request->getHeaderLine('Authorization');
    }
}
