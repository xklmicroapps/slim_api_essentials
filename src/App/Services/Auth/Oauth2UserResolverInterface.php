<?php

declare(strict_types=1);

namespace SlimApiEssentials\App\Services\Auth;

interface Oauth2UserResolverInterface
{
    /**
     * get client name (any string) by client for oauth2 auth
     * (some king of reverse $this->getClientByClientId())
     */
    public function getClientNameByClient(mixed $client): string;

    /**
     * get client_id (unique client identifier) by client for oauth2 auth
     * (some king of reverse $this->getClientByClientId())
     */
    public function getClientIdByClient(mixed $client): string;

    /**
     * get client entity by its client_id for oauth2 auth
     */
    public function getClientByClientId(string $id): mixed;

    /**
     * verify secret for client
     *     returned by $this->getClientById()
     *     and $this->getClientByClientId()
     * and return true if ok, or false if not ok
     */
    public function isClientSecretOk(mixed $client, string $secret): bool;
}
