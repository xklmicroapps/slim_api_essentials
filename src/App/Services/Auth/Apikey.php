<?php

declare(strict_types=1);

namespace SlimApiEssentials\App\Services\Auth;

use Psr\Http\Message\RequestInterface;
use SlimApiEssentials\App\Exceptions\AuthApikeyException;
use SlimApiEssentials\App\Services\Helpers\Common as CommonHelper;

class Apikey implements AuthInterface
{
    public function __construct(
        private ApikeyUserResolverInterface $userResolver,
        private AuthActions $actions
    ) {
    }

    /**
     * @throws AuthApikeyException 403 if unauthorized
     * @param  RequestInterface $request [description]
     * @return void                    [description]
     */
    public function check(RequestInterface $request): void
    {
        $matches = null;
        $authHeaderContent = $this->actions->getAuthorizationHeaderContent($request);
        if (
            !preg_match(
                '#^\s*ApiKey\s+([^\s]+)\s*$#',
                $authHeaderContent,
                $matches
            )
        ) {
            throw new AuthApikeyException(
                'Unknown Authorization header! "'
                . $authHeaderContent
                . '"'
            );
        }

        $apikey = $matches[1];

        $user = $this->userResolver->getUserByApikey($apikey);
        if (!$user) {
            throw new AuthApikeyException(
                'Unknown api key!'
            );
        }

        if ($this->userResolver->isApikeyExpired($apikey)) {
            throw new AuthApikeyException(
                'Expired api key!'
            );
        }
    }
}
