<?php

declare(strict_types=1);

namespace SlimApiEssentials\App\Services\Auth;

use Psr\Http\Message\RequestInterface;
use SlimApiEssentials\App\Exceptions\AuthBasicException;

class Basic implements AuthInterface
{
    public function __construct(
        private BasicUserResolverInterface $userResolver,
        private AuthActions $actions
    ) {
    }

    /**
     * @throws AuthBasicException 401 if unauthorized
     * @param  RequestInterface $request [description]
     * @return void                    [description]
     */
    public function check(RequestInterface $request): void
    {
        $authCredentialsArr = $this->actions->getBasicCredentialsFromRequest($request);

        $user = $this->userResolver->getUserByUsername($authCredentialsArr[0]);
        if (
            !$user
            || !$this->userResolver->isUserPassOk($user, $authCredentialsArr[1])
        ) {
            throw new AuthBasicException(
                'Incorrect user or password!'
            );
        }
    }
}
