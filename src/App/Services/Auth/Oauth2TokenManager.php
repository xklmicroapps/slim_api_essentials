<?php

declare(strict_types=1);

namespace SlimApiEssentials\App\Services\Auth;

use DateTimeInterface;
use SlimApiEssentials\App\Exceptions\AuthOauth2Exception;
use SlimApiEssentials\App\Services\Helpers\Security as SecurityHelper;
use SlimApiEssentials\App\Services\Helpers\Common as CommonHelper;
use Psr\Http\Message\ServerRequestInterface;
use SlimApiEssentials\App\Entities\Oauth2JwtToken;

class Oauth2TokenManager
{
    const TOKEN_MINUTES_VALID = 60;

    private $encryptedStack = [];

    private $decryptedStack = [];

    public function __construct(
        private SecurityHelper $securityHelper,
        private CommonHelper $commonHelper
    ) {
    }

    public function getTokenHeaderFromAccessToken(string $accessToken): ?array
    {
        return $this->getTokenPartFromAccessToken(0, $accessToken);
    }

    public function getTokenPayloadFromAccessToken(string $accessToken): ?array
    {
        return $this->getTokenPartFromAccessToken(1, $accessToken);
    }

    public function getSignatureFromAccessToken(string $accessToken): ?string
    {
        return $this->getTokenPartFromAccessToken(2, $accessToken);
    }

    public function getValidInFromAccessToken(
        string $accessToken,
        DateTimeInterface $datetimenow
    ): ?int {
        $payload = $this->getTokenPayloadFromAccessToken($accessToken);
        if (isset($payload['nbf'])) {
            return $payload['nbf'] - $datetimenow->getTimestamp();
        }
        return null;
    }

    public function getExpiresInFromAccessToken(
        string $accessToken,
        DateTimeInterface $datetimenow
    ): ?int {
        $payload = $this->getTokenPayloadFromAccessToken($accessToken);
        if (isset($payload['exp'])) {
            return $payload['exp'] - $datetimenow->getTimestamp();
        }
        return null;
    }

    public function getSignatureFromPayload(
        array $tokenHeader,
        array $tokenPayload
    ): string {
        return $this->securityHelper->getSignatureHmac(
            json_encode($tokenHeader, JSON_THROW_ON_ERROR)
            . json_encode($tokenPayload, JSON_THROW_ON_ERROR)
        );
    }

    public function getNewTokenFromData(
        DateTimeInterface $datetimenow,
        string $clientId,
        string $clientName
    ): Oauth2JwtToken {
        $timeNow = $datetimenow->getTimestamp();
        $exp = strtotime('+' . strval(self::TOKEN_MINUTES_VALID) . ' minutes', $timeNow);
        $nbf = $timeNow;
        $jti = uniqid('jti_', true) . '.' . rand(0, 999999);
        $accessToken = $this->getAccessToken(
            [
                'typ' => 'JWT',
                'alg' => 'HS256'
            ],
            [
                'sub' => $clientId,
                'name' => $clientName,
                'iat' => $timeNow,
                'nbf' => $nbf,
                'exp' => $exp,
                'jti' => $jti
            ]
        );
        return new Oauth2JwtToken([
            'access_token' => $accessToken,
            'expires_in' => $this->getExpiresInFromAccessToken(
                $accessToken,
                $datetimenow
            ),
            'valid_in' => $this->getValidInFromAccessToken(
                $accessToken,
                $datetimenow
            )
        ]);
    }

    /**
     * get access token string usable in Auth header Bearer
     */
    private function getAccessToken(
        array $tokenHeader,
        array $tokenPayload
    ): string {
        /**
         * generate access token string
         */
        $accessToken = $this->commonHelper->base64Encode(
            json_encode($tokenHeader, JSON_THROW_ON_ERROR)
        )
        . '.'
        . $this->commonHelper->base64Encode(
            json_encode($tokenPayload, JSON_THROW_ON_ERROR)
        )
        . '.'
        . $this->commonHelper->base64Encode(
            $this->getSignatureFromPayload($tokenHeader, $tokenPayload)
        );

        /**
         * encrypt
         */
        return $this->getEncryptedAccessToken($accessToken);
    }

    private function getTokenPartFromAccessToken(int $part, string $accessToken): array|string|null
    {
        /**
         * decrypt
         */
        $accessToken = $this->getDecryptedAccessToken($accessToken);

        /**
         * decode parts from string
         */
        $tokenPartDecoded = null;
        $tokenParts = explode('.', $accessToken);
        if (isset($tokenParts[$part])) {
            $tokenPartDecoded = $this->commonHelper->base64Decode($tokenParts[$part]);
            if (in_array($part, [0, 1], true)) {
                $tokenPartDecoded = json_decode(
                    $tokenPartDecoded,
                    true,
                    512,
                    JSON_THROW_ON_ERROR
                );
            }
        }
        return $tokenPartDecoded;
    }

    private function getEncryptedAccessToken(string $accessToken): string
    {
        $idx = md5($accessToken);
        if (!isset($this->encryptedStack[$idx])) {
            $this->encryptedStack[$idx] = $this->securityHelper->encrypt($accessToken);
        }
        return $this->encryptedStack[$idx];
    }

    private function getDecryptedAccessToken(string $accessToken): string
    {
        $idx = md5($accessToken);
        if (!isset($this->decryptedStack[$idx])) {
            $this->decryptedStack[$idx] = $this->securityHelper->decrypt($accessToken);
        }
        return $this->decryptedStack[$idx];
    }
}
