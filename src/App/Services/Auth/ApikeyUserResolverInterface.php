<?php

declare(strict_types=1);

namespace SlimApiEssentials\App\Services\Auth;

interface ApikeyUserResolverInterface
{
    /**
     * get user entity by its apikey
     */
    public function getUserByApikey(string $apikey): mixed;

    /**
     * verify api key expiration
     * and return true if expired, or false if not expired and can continue request
     */
    public function isApikeyExpired(string $apikey): bool;
}
