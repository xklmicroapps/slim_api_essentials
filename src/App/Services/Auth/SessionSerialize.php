<?php

declare(strict_types=1);

namespace SlimApiEssentials\App\Services\Auth;

use SlimApiEssentials\App\Services\Helpers\Common as CommonHelper;
use SlimApiEssentials\App\Services\Helpers\Security as SecurityHelper;
use SlimApiEssentials\App\Di\Container;
use Psr\Http\Message\RequestInterface;
use Slim\Routing\RouteContext;

class SessionSerialize
{
    public function __construct(
        private Container $container
    ) {
    }

    public function getSerializedRequest(RequestInterface $request): string
    {
        /**
         * can not serialize sll request
         *     (anonymous classes and closures etc...)
         *
         * serialize only part of request and try to rebuild back when unserializing
         */
        return $this->getEncrypted(
            serialize(
                $request
                    ->withoutAttribute(RouteContext::ROUTE_PARSER)
                    ->withoutAttribute(RouteContext::ROUTING_RESULTS)
                    ->withoutAttribute(RouteContext::ROUTE)
            )
        );
    }

    public function getUnserializedRequest(string $request): RequestInterface
    {
        /**
         * try to rebuild back incomplete serialized request
         */
        return $this->container
            ->getRoutingMiddleware()
            ->performRouting(
                unserialize(
                    $this->getDecrypted($request)
                )
            );
    }

    public function getEncoded(string $value): string
    {
        return $this->container->get(CommonHelper::class)->base64Encode($value);
    }

    public function getDecoded(string $value): string
    {
        return $this->container->get(CommonHelper::class)->base64Decode($value);
    }

    public function getEncrypted(string $value): string
    {
        return $this->container->get(SecurityHelper::class)->encrypt($value);
    }

    public function getDecrypted(string $value): string
    {
        return $this->container->get(SecurityHelper::class)->decrypt($value);
    }
}
