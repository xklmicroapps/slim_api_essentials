<?php

declare(strict_types=1);

namespace SlimApiEssentials\App\Services\Auth;

use Psr\Http\Message\RequestInterface;

interface SessionUserResolverInterface
{
    /**
     * get user entity by its username for Session auth
     */
    public function getUserByUsername(string $username): mixed;

    /**
     * verify password for user returned by $this->getUserByUsername()
     * and return true if ok, or false if not ok
     */
    public function isUserPassOk(mixed $user, string $password): bool;

    /**
     * get user Session
     *     return NULL for empty or expired session
     */
    public function getUserSession(RequestInterface $request): mixed;

    /**
     * get flash messages interface
     *     return NULL for not provided
     */
    public function getFlashMessages(): ?SessionFlashMessagesInterface;

    /**
     * set user Session
     *     set user to session
     *
     * @param mixed $user the same $user as from $this->getUserByUsername
     */
    public function setUserSession(mixed $user): void;

    /**
     * logout
     */
    public function unsetUserSession(RequestInterface $request): void;
}
