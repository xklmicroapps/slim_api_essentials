<?php

declare(strict_types=1);

namespace SlimApiEssentials\App\Services\Auth;

use Psr\Http\Message\RequestInterface;
use SlimApiEssentials\App\Exceptions\AuthRemoteException;
use GuzzleHttp\Exception\BadResponseException as GuzzleBadResponseException;
use GuzzleHttp\Client as GuzzleClient;

class Remote implements AuthInterface
{
    public function __construct(
        private array $urls,
        private bool $certVerify,
        private AuthActions $actions,
        private GuzzleClient $httpClient
    ) {
    }

    /**
     * @throws AuthRemoteException (401, 403, ...) if unauthorized
     * @param  RequestInterface $request [description]
     * @return void                    [description]
     */
    public function check(RequestInterface $request): void
    {
        try {
            $options = [
                'verify' => $this->certVerify
            ];

            /**
             * Authorization header should be used everywhere for authorization/ed requests
             *     - so authorization header is a Must for auth check
             */
            $options['headers']['Authorization'] = $this->actions->getAuthorizationHeaderContent(
                $request
            );
            $authType = $this->actions->getAuthorizationTypeFromRequest(
                $request
            );
            if ($authType === null) {
                throw new AuthRemoteException(
                    'Authorization header type not found!'
                );
            }
            if (!isset($this->urls[$authType])) {
                throw new AuthRemoteException(
                    'Authorization header type has no url endpoint set!'
                        . ' (mssing url[' . $authType . '] in config)'
                );
            }

            $this->httpClient->request(
                'GET',
                $this->urls[$authType],
                $options
            );
        } catch (GuzzleBadResponseException $e) {
            $excp = new AuthRemoteException(
                $e->getMessage(),
                $e->getCode(),
                $e
            );
            $response = $e->getResponse();
            if ($response->hasHeader('WWW-Authenticate')) {
                $excp->setWwwAuthenticate(
                    $response->getHeaderLine('WWW-Authenticate')
                );
            }
            if ($response->hasHeader('Xklmicroapps-Authenticate-Location')) {
                $excp->setAuthenticateLocation(
                    $response->getHeaderLine('Xklmicroapps-Authenticate-Location')
                );
            }

            throw $excp;
        }
    }
}
