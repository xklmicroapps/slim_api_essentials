<?php

declare(strict_types=1);

namespace SlimApiEssentials\App\Services\Auth;

use Throwable;
use DateTime;
use Psr\Http\Message\RequestInterface;
use SlimApiEssentials\App\Exceptions\AuthBasicException;
use SlimApiEssentials\App\Exceptions\AuthOauth2Exception;
use SlimApiEssentials\App\Entities\Oauth2JwtToken;
use SlimApiEssentials\App\Services\Auth\Oauth2TokenManager;

class Oauth2 implements AuthInterface
{
    public function __construct(
        private Oauth2UserResolverInterface $userResolver,
        private AuthActions $actions,
        private Oauth2TokenManager $tokenManger
    ) {
    }

    /**
     * @throws AuthOauth2Exception 401 if unauthorized
     * @param  RequestInterface $request [description]
     * @return void                    [description]
     */
    public function check(RequestInterface $request): void
    {
        $matches = null;
        $authHeaderContent = $this->actions->getAuthorizationHeaderContent($request);
        if (
            !preg_match(
                '#^\s*Bearer\s+([A-Za-z0-9+/=.]+)\s*$#',
                $authHeaderContent,
                $matches
            )
        ) {
            throw (new AuthOauth2Exception(
                'Unknown Authorization header! "'
                . $authHeaderContent
                . '"'
            ))->setError('invalid_client');
        }

        $accessToken = $matches[1];

        try {
            $signature = $this->tokenManger->getSignatureFromAccessToken(
                $accessToken
            );
            $signatureFromPayload = $this->tokenManger->getSignatureFromPayload(
                $this->tokenManger->getTokenHeaderFromAccessToken(
                    $accessToken
                ),
                $this->tokenManger->getTokenPayloadFromAccessToken(
                    $accessToken
                )
            );
        } catch (Throwable $e) {
            throw (new AuthOauth2Exception(
                'Invalid Token!',
                401,
                $e
            ))->setError('invalid_client');
        }

        if ($signature !== $signatureFromPayload) {
            throw (new AuthOauth2Exception(
                'Invalid access token (signature)!'
            ))->setError('invalid_grant');
        }

        $datetimenow = new DateTime('now');
        $validIn = $this->tokenManger->getValidInFromAccessToken(
            $accessToken,
            $datetimenow
        );
        if ($validIn === null || $validIn > 0) {
            throw (new AuthOauth2Exception(
                'Access token is not yet valid! Seconds to wait for validity: ' . strval($validIn)
            ))->setError('invalid_grant');
        }

        $expiresIn = $this->tokenManger->getExpiresInFromAccessToken(
            $accessToken,
            $datetimenow
        );
        if ($expiresIn === null || $expiresIn < 1) {
            throw (new AuthOauth2Exception(
                'Expired access token!'
            ))->setError('invalid_grant');
        }
    }

    public function getNewTokenFromRequest(RequestInterface $request): Oauth2JwtToken
    {
        $body = $request->getParsedBody();
        if (
            !isset($body['grant_type'])
            || $body['grant_type'] !== 'client_credentials'
        ) {
            throw (new AuthOauth2Exception(
                'Missing or invalid body content parameter "grant_type=client_credentials"!',
            ))->setError('unsupported_grant_type');
        }

        try {
            $authCredentialsArr = $this->actions->getBasicCredentialsFromRequest(
                $request
            );
        } catch (AuthBasicException $e) {
            throw (new AuthOauth2Exception(
                $e->getMessage(),
                $e->getCode(),
                $e
            ))->setError('invalid_client');
        }

        /**
         * check client credentials (mabye some Auth service?), if not ok, go 401,
         */
        $client = $this->userResolver->getClientByClientId($authCredentialsArr[0]);
        if (!$client) {
            throw (new AuthOauth2Exception(
                'Client not found!',
            ))->setError('invalid_client');
        }
        if (!$this->userResolver->isClientSecretOk($client, $authCredentialsArr[1])) {
            throw (new AuthOauth2Exception(
                'Invalid client secret!'
            ))->setError('invalid_grant');
        }

        /**
         * if credentials are ok return new token
         */
        try {
            return $this->tokenManger->getNewTokenFromData(
                new DateTime('now'),
                $this->userResolver->getClientIdByClient($client),
                $this->userResolver->getClientNameByClient($client)
            );
        } catch (Throwable $e) {
            if ($e instanceof AuthOauth2Exception) {
                throw $e;
            }

            throw (new AuthOauth2Exception(
                'Token by client credentials error! ' . $e->getMessage(),
                401,
                $e
            ));
        }
    }
}
