<?php

declare(strict_types=1);

namespace SlimApiEssentials\App\Services\Auth;

interface SessionFlashMessagesInterface
{
    /**
     * add flashmessage to stack
     *
     * @param string      $message  main message / title
     * @param string      $type     type (probably some css class part)
     * @param string      $body     sub message / body
     * @param int|integer $autohide miliseconds after message is auto-hidden
     * @param string      $bodyHtml sub message html body (will not be html escaped on output)
     */
    public function add(
        string $message = '',
        string $type = 'info',
        string $body = '',
        int $autohide = 1,
        string $bodyHtml = ''
    ): void;

    /**
     * get all falshmessages from stack
     */
    public function get(): array;

    /**
     * get all falshmessages from stack and empty the stack
     */
    public function getAndClean(): array;
}
