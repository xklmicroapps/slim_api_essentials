<?php

declare(strict_types=1);

namespace SlimApiEssentials\App\Services\Auth;

use Throwable;
use SlimApiEssentials\App\Services\RequestHelper;
use SlimApiEssentials\App\Services\ResponseHelper;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Slim\Routing\RouteContext;
use Slim\Routing\Route;
use Slim\Interfaces\RouteInterface;
use Slim\Psr7\Factory\ResponseFactory;

class SessionActions
{
    public function __construct(
        private RequestHelper $requestHelper,
        private ResponseHelper $responseHelper,
        private SessionUserResolverInterface $userResolver,
        private ResponseFactory $responseFactory
    ) {
    }

    public function isRouteName(RequestInterface $request, string $routenameCheck): bool
    {
        $route = $this->getRouteFromRequest($request);
        $routeName = $route ? $route->getName() : null;
        if (!$routeName) {
            return false;
        }
        if ($routeName === $routenameCheck) {
            return true;
        }

        return false;
    }

    public function getRequestParamsToUse(RequestInterface $request): array
    {
        if ($this->requestHelper->isWithBody($request)) {
            return $request->getParsedBody();
        }

        return $request->getQueryParams();
    }

    public function getRouteFromRequest(RequestInterface $request): RouteInterface
    {
        return RouteContext::fromRequest($request)->getRoute();
    }

    public function getResponseRedirectLoggedSaved(
        array $expiredRedirectPost,
        RequestInterface $request,
        RequestInterface $requestSaved
    ): ResponseInterface {
        if (strtoupper($requestSaved->getMethod()) === 'POST') {
            /**
             * Slim\Routing\Route::handle() is the real handler here
             *     due to debugging handle process of MiddlewareDispatcher
             */
            $routeSaved = $requestSaved->getAttribute(RouteContext::ROUTE);
            if ($routeSaved) {
                return $this->getResponseRedirectLoggedSavedPost(
                    $expiredRedirectPost,
                    $routeSaved,
                    $request,
                    $requestSaved
                );
            }
        }

        return $this->responseHelper->getWithLocationFromRequest(
            $request,
            $this->responseFactory->createResponse(),
            $requestSaved
        );
    }

    private function getResponseRedirectLoggedSavedPost(
        array $expiredRedirectPost,
        Route $routeSaved,
        RequestInterface $request,
        RequestInterface $requestSaved
    ): ResponseInterface {
        //
        //  !!! if post action returns redirect response itself, this config array is NOT used !!!
        //  !!! if post action returns response with some file transfer (download), post action is NOT repeated, only redirected !!!
        //  !!! the first found regexps wins !!!
        //
        $response = null;
        try {
            $handled = $routeSaved->handle($requestSaved);
            if ($this->isHandledResponseRedirection($handled)) {
                return $handled;
            }

            $response = $this->getExpiredRedirectPostResponse(
                $expiredRedirectPost,
                $requestSaved,
                $request
            );
        } catch (Throwable $e) {
            if ($this->userResolver->getFlashMessages()) {
                $this->userResolver->getFlashMessages()->add(
                    'Repeated request error!',
                    'danger',
                    $e->getMessage(),
                    0
                );
            }
            $response = $this->getExpiredRedirectPostResponse(
                $expiredRedirectPost,
                $requestSaved,
                $request
            );
        }

        if ($response !== null) {
            return $response;
        }

        if ($this->userResolver->getFlashMessages()) {
            $this->userResolver->getFlashMessages()->add(
                'Repeated request warning!',
                'warning',
                'Redirection is not configured for this POST url ('
                    . $this->getPathQuerySaved($requestSaved)
                    . '). Contact support, please!',
                0
            );
        }

        return $this->getResponseRedirectLoggedFirst($request);
    }

    private function getExpiredRedirectPostResponse(
        array $expiredRedirectPost,
        RequestInterface $requestSaved,
        RequestInterface $requestActual
    ): ?ResponseInterface {
        $uriSaved = $requestSaved->getUri();
        $pathQuerySaved = $this->getPathQuerySaved($requestSaved);
        foreach ($expiredRedirectPost as $regexp => $replace) {
            if (!preg_match($regexp, $pathQuerySaved)) {
                continue;
            }

            return $this->responseHelper->getWithLocationUri(
                $requestActual,
                $this->responseFactory->createResponse(),
                $uriSaved->getScheme()
                    . '://'
                    . $uriSaved->getAuthority()
                    . preg_replace($regexp, $replace, $pathQuerySaved)
            );
        }

        return null;
    }

    private function getPathQuerySaved(RequestInterface $requestSaved): string
    {
        $uriSaved = $requestSaved->getUri();
        $pathSaved = $uriSaved->getPath();
        $querySaved = $uriSaved->getQuery();
        if ($querySaved) {
            return $pathSaved . '?' . $querySaved;
        }
        return $pathSaved;
    }

    private function isHandledResponseRedirection(ResponseInterface $handled): bool
    {
        /**
         * common http redirection
         */
        if (
            $handled->getStatusCode() >= 300
            && $handled->getStatusCode() < 400
            && $handled->getHeaderLine('Location')
        ) {
            return true;
        }

        /**
         * cusom javascript/ajax redirection
         */
        if ($handled->getStatusCode() !== 200) {
            return false;
        }
        if ($handled->getHeaderLine('Content-Type') !== 'application/json') {
            return false;
        }
        $bodyDec = json_decode($handled->getBody()->__toString(), true);
        if (!$bodyDec) {
            return false;
        }
        if (($bodyDec['redirect'] ?? null) && ($bodyDec['location'] ?? null)) {
            return true;
        }

        return false;
    }
}
