<?php

declare(strict_types=1);

namespace SlimApiEssentials\App\Services\Auth;

use Psr\Http\Message\RequestInterface;

interface AuthInterface
{
    /**
     * @throws \Throwable if check failed
     */
    public function check(RequestInterface $request): void;
}
