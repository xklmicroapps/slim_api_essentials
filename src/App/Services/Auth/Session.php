<?php

declare(strict_types=1);

namespace SlimApiEssentials\App\Services\Auth;

use Throwable;
use LogicException;
use SlimApiEssentials\App\Exceptions\AuthSessionException;
use SlimApiEssentials\App\Services\ResponseHelper;
use Slim\Routing\RouteParser;
use Slim\Psr7\Factory\ResponseFactory;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class Session implements AuthInterface
{
    const PARAM_NAME_REQUESTSAVEDFILEPATH = '_svdrqfp';

    const FILEPREFIX_REQUESTSAVED = 'xkl.request';

    private $onLoginSuccess = null;

    private $onLoginError = null;

    private $config = [
        'keyUsername' => null,
        'keyPassword' => null,
        'routeNameGet' => null,
        'routeNameSet' => null,
        'routeNameLoggedFirst' => null,
        'routeArgsPersistent' => null,
        'expiredRedirectPost' => null
    ];

    public function __construct(
        array $config,
        private SessionActions $actions,
        private SessionUserResolverInterface $userResolver,
        private RouteParser $routeParser,
        private ResponseFactory $responseFactory,
        private ResponseHelper $responseHelper,
        private SessionSerialize $sessionSerialize
    ) {
        foreach (array_keys($this->config) as $key) {
            $this->config[$key] = $config[$key];
        }
        foreach (array_keys($this->getExpiredRedirectPost()) as $regexp) {
            /**
             * test if it is a valid regexp
             */
            preg_match($regexp, '');
        }
    }

    public function getKeyPassword(): string
    {
        return $this->config['keyPassword'];
    }

    public function getKeyUsername(): string
    {
        return $this->config['keyUsername'];
    }

    public function getRouteNameGet(): string
    {
        return $this->config['routeNameGet'];
    }

    public function getRouteNameSet(): string
    {
        return $this->config['routeNameSet'];
    }

    public function getRouteNameLoggedFirst(): string
    {
        return $this->config['routeNameLoggedFirst'];
    }

    public function getRouteArgsPersistent(): array
    {
        return $this->config['routeArgsPersistent'] ?: [];
    }

    public function getExpiredRedirectPost(): array
    {
        return $this->config['expiredRedirectPost'] ?: [];
    }

    public function onLoginSuccess(callable $callable)
    {
        $this->onLoginSuccess = $callable;
    }

    public function onLoginError(callable $callable)
    {
        $this->onLoginError = $callable;
    }

    public function getOnLoginSuccess()
    {
        return $this->onLoginSuccess;
    }

    public function getOnLoginError()
    {
        return $this->onLoginError;
    }

    public function getResponseRedirectNotLogged(
        RequestInterface $request
    ): ResponseInterface {
        $redirQueryParams = $request->getQueryParams();
        if (!$this->isRouteNameSet($request)) {
            $redirQueryParams[self::PARAM_NAME_REQUESTSAVEDFILEPATH] = $this->saveRequestAndGetFilepathEnc(
                $request
            );
        }

        $responseEmpty = $this->responseFactory->createResponse();
        return $this->responseHelper->getWithLocation(
            $request,
            $responseEmpty,
            $this->getRouteNameGet(),
            $this->getRouteArgsPersistentFromRequest($request),
            $redirQueryParams
        );
    }

    public function getResponseRedirectLogged(
        RequestInterface $request
    ): ResponseInterface {
        $params = $request->getQueryParams();
        if (!isset($params[self::PARAM_NAME_REQUESTSAVEDFILEPATH])) {
            throw new LogicException(
                "Missing required query parameter"
                    . " \"" . self::PARAM_NAME_REQUESTSAVEDFILEPATH . "\"!"
                    . " Login action should be created with "
                    . self::class . "::getLoginActionUri()"
            );
        }
        $reqPathParamValue = $params[self::PARAM_NAME_REQUESTSAVEDFILEPATH];
        if (!$reqPathParamValue) {
            return $this->getResponseRedirectLoggedFirst($request);
        }

        try {
            $filepath = $this->sessionSerialize->getDecoded(
                $reqPathParamValue
            );
        } catch (Throwable $e) {
            return $this->getResponseRedirectLoggedFirst($request);
        }

        $requestSaved = $this->getRequestSavedFromFilepath($filepath);

        if (!$requestSaved) {
            return $this->getResponseRedirectLoggedFirst($request);
        }

        /**
         * unlink file only if saved request is available
         */
        unlink($filepath);

        return $this->actions->getResponseRedirectLoggedSaved(
            $this->getExpiredRedirectPost(),
            $request,
            $requestSaved
        );
    }

    public function getLoginActionUri(RequestInterface $request): string
    {
        $paramValue = '';
        $queryParams = $request->getQueryParams();
        if (isset($queryParams[self::PARAM_NAME_REQUESTSAVEDFILEPATH])) {
            $paramValue = $queryParams[self::PARAM_NAME_REQUESTSAVEDFILEPATH];
        }

        return $this->routeParser->fullUrlFor(
            $request->getUri(),
            $this->getRouteNameSet(),
            $this->getRouteArgsPersistentFromRequest($request),
            [
                self::PARAM_NAME_REQUESTSAVEDFILEPATH => $paramValue
            ]
        );
    }

    public function login(RequestInterface $request)
    {
        $authCredentialsArr = $this->getSessionCredentialsFromRequest($request);

        $user = $this->userResolver->getUserByUsername(
            $authCredentialsArr[$this->getKeyUsername()]
        );
        if (
            !$user
            || !$this->userResolver->isUserPassOk(
                $user,
                $authCredentialsArr[$this->getKeyPassword()]
            )
        ) {
            throw (new AuthSessionException(
                'Incorrect user or password!'
            ))->setArgs(
                $this->getRouteArgsPersistentFromRequest($request)
            );
        }

        $this->userResolver->setUserSession($user);
    }

    public function logout(RequestInterface $request)
    {
        return $this->userResolver->unsetUserSession($request);
    }

    public function getUserSession(RequestInterface $request)
    {
        return $this->userResolver->getUserSession($request);
    }

    public function check(RequestInterface $request): void
    {
        $this->checkSession($request);
    }

    public function isLoggedIn(RequestInterface $request)
    {
        if ($this->getUserSession($request) === null) {
            return false;
        }
        return true;
    }

    public function isRouteNameSet(RequestInterface $request): bool
    {
        return $this->actions->isRouteName(
            $request,
            $this->getRouteNameSet()
        );
    }

    public function isRouteNameGet(RequestInterface $request): bool
    {
        return $this->actions->isRouteName(
            $request,
            $this->getRouteNameGet()
        );
    }

    private function checkSession(RequestInterface $request)
    {
        if (!$this->isLoggedIn($request)) {
            throw (new AuthSessionException(
                'User session expired!'
            ))->setArgs(
                $this->getRouteArgsPersistentFromRequest($request)
            );
        }
    }

    private function getSessionCredentialsFromRequest(RequestInterface $request): array
    {
        $authCredentialsArr = array_filter(
            $this->actions->getRequestParamsToUse($request),
            fn ($key) => in_array($key, [$this->getKeyUsername(), $this->getKeyPassword()]),
            ARRAY_FILTER_USE_KEY
        );

        $errs = [];
        if (!isset($authCredentialsArr[$this->getKeyUsername()])) {
            $errs[] = 'Missing parameter named "' . $this->getKeyUsername() . '".';
        }
        if (!isset($authCredentialsArr[$this->getKeyPassword()])) {
            $errs[] = 'Missing parameter named "' . $this->getKeyPassword() . '".';
        }

        if ($errs) {
            throw (new AuthSessionException(
                'Invalid Auth (POST/GET) params passed!'
                    . '('
                    . implode(' ', $errs)
                    . ')'
            ))->setArgs(
                $this->getRouteArgsPersistentFromRequest($request)
            );
        }

        return $authCredentialsArr;
    }

    private function getResponseRedirectLoggedFirst(RequestInterface $request)
    {
        $responseEmpty = $this->responseFactory->createResponse();
        return $this->responseHelper->getWithLocation(
            $request,
            $responseEmpty,
            $this->getRouteNameLoggedFirst(),
            $this->getRouteArgsPersistentFromRequest($request)
        );
    }

    private function saveRequestAndGetFilepathEnc(RequestInterface $request)
    {
        $filepath = tempnam(sys_get_temp_dir(), self::FILEPREFIX_REQUESTSAVED);
        file_put_contents(
            $filepath,
            $this->sessionSerialize->getSerializedRequest($request)
        );
        return $this->sessionSerialize->getEncoded(
            $filepath
        );
    }

    private function getRequestSavedFromFilepath(string $filepath)
    {
        if (!is_file($filepath)) {
            return null;
        }

        if (!is_readable($filepath)) {
            return null;
        }

        $content = file_get_contents($filepath);
        if (!$content) {
            return null;
        }

        return $this->sessionSerialize->getUnserializedRequest($content);
    }

    public function getRouteArgsPersistentFromRequest(RequestInterface $request)
    {
        $route = $this->actions->getRouteFromRequest($request);

        $args = [];
        foreach ($this->getRouteArgsPersistent() as $value) {
            $args[$value] = $route->getArgument($value);
        }
        return $args;
    }
}
