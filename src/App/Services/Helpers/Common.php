<?php

declare(strict_types=1);

namespace SlimApiEssentials\App\Services\Helpers;

use RuntimeException;

class Common
{
    const BASE64_REGEXP = '^(?:[A-Za-z0-9+/]{4})*(?:[A-Za-z0-9+/]{2}==|[A-Za-z0-9+/]{3}=)?$';

    /**
     * base64 decode string
     *
     * @param  string $string [description]
     * @return string         [description]
     * @throws RuntimeException [<description>]
     */
    public function base64Decode(string $string): string
    {
        // if (!preg_match('#' . self::BASE64_REGEXP . '#', $string)) {
        //     throw new RuntimeException('Looks like NOT base64 encoded string! "' . $string . '"');
        // }
        $decoded = base64_decode($string, true);
        if ($decoded === false) {
            throw new RuntimeException('Error with base64_decode string "' . $string . '"');
        }
        return $decoded;
    }

    public function base64Encode(string $string): string
    {
        return base64_encode($string);
    }

    public function getFormattedSizebytes(int $bytes): string
    {
        $bytesFmt = '0 bytes';
        if ($bytes >= 1073741824) {
            $bytesFmt = number_format($bytes / 1073741824, 2, '.', ' ') . ' GB';
        } elseif ($bytes >= 1048576) {
            $bytesFmt = number_format($bytes / 1048576, 2, '.', ' ') . ' MB';
        } elseif ($bytes >= 1024) {
            $bytesFmt = number_format($bytes / 1024, 2, '.', ' ') . ' KB';
        } elseif ($bytes > 1) {
            $bytesFmt = $bytes . ' bytes';
        } elseif ($bytes === 1) {
            $bytesFmt = $bytes . ' byte';
        }
        return $bytesFmt;
    }
}
