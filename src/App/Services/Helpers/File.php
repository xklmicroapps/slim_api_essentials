<?php

declare(strict_types=1);

namespace SlimApiEssentials\App\Services\Helpers;

use RuntimeException;
use Throwable;

class File
{
    private $filePathFromStrings = [];

    public function __construct(
        private Common $commonHelper
    ) {
    }

    /**
     * get file path
     * from the string $file that can be real file path, or url or base64 encoded string
     * @param  string $file [description]
     * @return string       realpath if $file is the path or system temporary real file path in case of base64 or url
     * @throws RuntimeException [<description>]
     */
    public function getFilePathFromString(string $file): string
    {
        $idx = md5($file);
        if (isset($this->filePathFromStrings[$idx])) {
            return $this->filePathFromStrings[$idx];
        }

        try {
            if (filter_var($file, FILTER_VALIDATE_URL)) {
                $this->filePathFromStrings[$idx] = $this->getFileFromUrl(
                    $this->getTmpFilepath($file),
                    $file
                );
                return $this->filePathFromStrings[$idx];
            }
            if ($this->isStringFilepath($file)) {
                $this->filePathFromStrings[$idx] = realpath($file);
                return $this->filePathFromStrings[$idx];
            }
            $this->filePathFromStrings[$idx] = $this->getFileFromBase64(
                $this->getTmpFilepath($file),
                $file
            );
            return $this->filePathFromStrings[$idx];
        } catch (Throwable $e) {
            throw new RuntimeException("Bad file format or not found!", 500, $e);
        }
    }

    /**
     * [isStringFilepath description]
     * @param  string  $file [description]
     * @return boolean       [description]
     */
    public function isStringFilepath(string $file): bool
    {
        try {
            if (file_exists($file) && is_file($file)) {
                return true;
            }
        } catch (Error $e) {
            return false;
        }
        return false;
    }

    /**
     * get temp filepath of a $file (path, url or base64 string)
     * @param  string $file path, url or base64 string
     * @return string       temp filepath
     */
    private function getTmpFilepath(string $file): string
    {
        return sys_get_temp_dir() . '/php_api_exports_' . md5($file);
    }

    /**
     * [getFileFromBase64 description]
     * @param  string $filepath [description]
     * @param  string $base64content      [description]
     * @return string           $filepath
     */
    private function getFileFromBase64(string $filepath, string $base64content): string
    {
        file_put_contents(
            $filepath,
            $this->commonHelper->base64Decode($base64content)
        );
        return $filepath;
    }

    /**
     * [getFileFromUrl description]
     * @param  string $filepath [description]
     * @param  string $url      valid url of the file
     * @return string           $filepath
     */
    private function getFileFromUrl(string $filepath, string $url): string
    {
        file_put_contents(
            $filepath,
            file_get_contents($url)
        );
        return $filepath;
    }
}
