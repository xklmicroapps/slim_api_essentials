<?php

declare(strict_types=1);

namespace SlimApiEssentials\App\Services\Helpers;

use RuntimeException;

class Security
{
    const ENCRYPT_METHOD = 'AES-256-CBC';

    public function __construct(
        private Common $commonHelper,
        private string $secretKey,
        private string $secretIv
    ) {
    }

    public function getSignatureHmac(string $data, string $algo = 'sha256')
    {
        return hash_hmac(
            $algo,
            $data,
            $this->getHmacKey(),
            false
        );
    }

    /**
     * decrypt string encrypted by Helper::encrypt()
     *
     * @param  string $string [description]
     * @return string         [description]
     */
    public function decrypt(string $string): string
    {
        if (!$string) {
            return '';
        }

        $decrypted = openssl_decrypt(
            $this->commonHelper->base64Decode($string),
            self::ENCRYPT_METHOD,
            $this->getEncryptKey(),
            0,
            $this->getEncryptIv()
        );
        if ($decrypted === false) {
            throw new RuntimeException(openssl_error_string());
        }
        return $decrypted;
    }

    /**
     * encrypt string decriptable by Helper::decrypt()
     * "base64 wrapped"
     *
     * @param  string $string [description]
     * @return string         [description]
     */
    public function encrypt(string $string): string
    {
        if (!$string) {
            return '';
        }

        return $this->commonHelper->base64Encode(
            openssl_encrypt(
                $string,
                self::ENCRYPT_METHOD,
                $this->getEncryptKey(),
                0,
                $this->getEncryptIv()
            )
        );
    }

    private function getEncryptKey(): string
    {
        return hash(
            'sha256',
            $this->secretKey . $this->secretIv . $this->secretKey
        );
    }

    private function getEncryptIv(): string
    {
        // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
        return substr(
            hash(
                'sha1',
                $this->secretIv . $this->secretKey
            ),
            0,
            16
        );
    }

    private function getHmacKey(): string
    {
        return hash(
            'sha256',
            $this->getEncryptKey() . $this->getEncryptIv()
        );
    }
}
